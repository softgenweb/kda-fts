<style>
.dropdown-submenu{
	position:relative;
	display:block;
}
.dropdown-submenu>.dropdown-menu {
    top:1px;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block !important;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}


.navbar-nav a{ color:#FFF !important;
/*background-color:#30a5ff !important;*/
}
<!--.Active{ background-color:#30a5ff !important; }-->

.dropdown-menu {
  background-color: #125C95 !important;
}


.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
  background-color: #080808 !important;
  color: #262626;
  text-decoration: none;
}
 a{ /*color:#FFF !important;*/} 
 .Active{background-color:#30a5ff !important;}
 
 
 </style>


 



<?php 
$master = array('hrm_department','regional_hierarchy','hrm_post','hrm_registration');

$hrm = array('hrm_post','hrm_registration','hrm_leave','hrm_salary','hrm_post');

//$emp_task = array('show_emp');

$lgm = array('customer','customer_caller');

$report = array('hrm_attendence','lgm_report');

$target = array('master_target','master_caller_target');

$emi= array('master_emi');

$outbound = array('hrm_attendence','outbound_report');
?>


 <!------------------------------------------Admin Menu-Start---------------------------------->
 <?php  
if($_SESSION['utype']=='Employee'){ 

/****************For Management*******************/
if($_SESSION['department_id']=='1'){?>



<ul class="nav navbar-nav" >
          
                  <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                   <li class="<?php if($_REQUEST['control']=='customer') {echo "panel-red";} ?>"><a href="index.php?control=customer&task=update_contact_detail">Update Customer Details</a></li> 
              <li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                         <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
<li><a href="index.php?control=fts&task=reject">Reject File</a></li>
<li><a href="index.php?control=fts&task=close">close File</a></li>
                </ul>
              
              </li>
              
              <li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                </ul>
              
              </li>
              
              
            </ul>
  
  <?php } 
  
  
  /****************For Project*******************/
if($_SESSION['department_id']=='2'){?>



<ul class="nav navbar-nav" >
          
              <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
             
              
               <!--<li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" ><a href="index.php?control=user_feedback">Feedback/Complain</a></li>-->
               <li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Feedback/Complain<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=user_feedback">Underprocess Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=close">Close Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=forwardtome_complain">Forward To Me Feedback/Complain</a></li>
                </ul>              
              </li>
               
              <li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                         <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
                         <li><a href="index.php?control=fts&task=reject">Reject File</a></li>
                         <li><a href="index.php?control=fts&task=close">close File</a></li>
                         
                </ul>
              
              </li>
              
              <li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                </ul>
              
              </li>
              
            </ul>
  
  <?php }
  
  
  /****************For Marketing*******************/
  if($_SESSION['department_id']=='3'){?>
  
  <ul class="nav navbar-nav" >
          
              <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
              
               <li class="<?php if($_REQUEST['control']=='customer' && $_REQUEST['task']!='payment_status') {echo "panel-red";} ?>"><a href="index.php?control=customer">Customer</a></li>  
               
               <li><a href="index.php?control=customer&task=meter_reading"  class="<?php if($_REQUEST['control']=='customer' && $_REQUEST['task']=='meter_reading') {echo "panel-red";} ?>">Customer Meter Reading</a></li>              
                 <li><a href="index.php?control=customer&task=update_contact_detail" class="<?php if($_REQUEST['control']=='customer' && $_REQUEST['task']=='update_contact_detail') {echo "panel-red";} ?>">Update Customer Details</a></li>              
               <li class="dropdown <?php if($_REQUEST['control']=='user_applynewconnection') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Connection<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=user_applynewconnection">Success Connection</a></li>
                        <li><a href="index.php?control=user_applynewconnection&task=pending_connection">Pending Connection</a></li>
                </ul>              
              </li>
                          
              
           
               <!--<li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" ><a href="index.php?control=user_feedback">Feedback/Complain</a></li>-->
               <li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Feedback/Complain<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=user_feedback">Underprocess Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=close">Close Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=forwardtome_complain">Forward To Me Feedback/Complain</a></li>
                </ul>              
              </li>
              
           
           <li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                        <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
                        <li><a href="index.php?control=fts&task=reject">Reject File</a></li>
                        <li><a href="index.php?control=fts&task=close">Close File</a></li>
                </ul>
              
              </li>
           
           <li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                       <li><a href="index.php?control=customer&task=meter_reading">Customer Meter Reading</a></li>
                </ul>
              
              </li>
           
            </ul>
  <?php }
  /****************For Finance*******************/
  if($_SESSION['department_id']=='4'){ ?>   
  <ul class="nav navbar-nav" >
          
              <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
              
                       
               <li class="dropdown <?php if($_REQUEST['control']=='customer' && $_REQUEST['task']=='payment_status') {echo "panel-red";} ?>" ><a href="index.php?control=customer&task=payment_status">Customer Payment</a></li>                
                             
               <li class="dropdown <?php if($_REQUEST['control']=='user_applynewconnection') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Connection<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=user_applynewconnection">Success Connection</a></li>
                        <li><a href="index.php?control=user_applynewconnection&task=pending_connection">Pending Connection</a></li>
                </ul>              
              </li>            
              
               <!--<li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" ><a href="index.php?control=user_feedback">Feedback/Complain</a></li>-->
               <li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Feedback/Complain<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=user_feedback">Underprocess Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=close">Close Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=forwardtome_complain">Forward To Me Feedback/Complain</a></li>
                </ul>              
              </li>
           <li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                        <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
                        <li><a href="index.php?control=fts&task=reject">Reject File</a></li>
                        <li><a href="index.php?control=fts&task=close">close File</a></li>
                </ul>
              
              </li>
           
           
           <li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                </ul>
              
              </li>
           
           
            </ul>
            
  
    <?php }
  /****************For HR*******************/
  if($_SESSION['department_id']=='5'){ ?>   
  <ul class="nav navbar-nav" >
          
              <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
           <li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                           <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
<li><a href="index.php?control=fts&task=reject">Reject File</a></li>
<li><a href="index.php?control=fts&task=close">close File</a></li>
                </ul>
              
              </li>
           
           
           <li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                </ul>
              
              </li>
           
           
            </ul>
            
  
    <?php }
  /****************For IT Department*******************/
  if($_SESSION['department_id']=='6'){ ?> 
  
  <ul class="nav navbar-nav" >
          
              <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
              
                <li class="<?php if($_REQUEST['control']=='customer') {echo "panel-red";} ?>"><a href="index.php?control=customer&task=update_contact_detail">Update Customer Details</a></li>   
               <li class="dropdown <?php if($_REQUEST['control']=='tender' && $_REQUEST['task']!='download_tender') {echo "panel-red";} ?>" ><a href="index.php?control=tender">Tender</a></li>
                  <li class="dropdown <?php if($_REQUEST['control']=='tender' && $_REQUEST['task']=='download_tender') {echo "panel-red";} ?>" ><a href="index.php?control=tender&task=download_tender">Download Tender</a></li>       
             <li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                      <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
<li><a href="index.php?control=fts&task=reject">Reject File</a></li>
<li><a href="index.php?control=fts&task=close">close File</a></li>
                </ul>
              
              </li>
              
              
              <li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                </ul>
              
              </li>
              
           
            </ul>
  <?php } 
  /****************For C & P*******************/
  if($_SESSION['department_id']=='7'){ ?> 
  
  <ul class="nav navbar-nav" >
          
              <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
              
                   
              <!-- <li class="dropdown <?php if($_REQUEST['control']=='tender' && $_REQUEST['task']!='download_tender') {echo "panel-red";} ?>" ><a href="index.php?control=tender">Tender</a></li>
                  <li class="dropdown <?php if($_REQUEST['control']=='tender' && $_REQUEST['task']=='download_tender') {echo "panel-red";} ?>" ><a href="index.php?control=tender&task=download_tender">Download Tender</a></li>   -->    
                                     <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='show') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=show">Initiate New files</a></li> 
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='pending_desk') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='pending') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=pending">Pending File</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='reject') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=reject">Reject File</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='close') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=close">close File</a></li>
             
           <!--<li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                       <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
<li><a href="index.php?control=fts&task=reject">Reject File</a></li>
<li><a href="index.php?control=fts&task=close">close File</a></li>
                </ul>
              
              </li>-->
              
              <!--<li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                </ul>              
              </li>-->
              
            </ul>
  <?php }  
  /****************For Planning & MIS*******************/
  if($_SESSION['department_id']=='8'){ ?> 
  
  <ul class="nav navbar-nav" >
          
              <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                              <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='show') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=show">Initiate New files</a></li> 
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='pending_desk') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='pending') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=pending">Pending File</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='reject') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=reject">Reject File</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='close') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=close">close File</a></li>        
             
           <!--<li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                       <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
<li><a href="index.php?control=fts&task=reject">Reject File</a></li>
<li><a href="index.php?control=fts&task=close">close File</a></li>
                </ul>
              
              </li>-->
              
              <!--<li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                </ul>
              
              </li>-->
              
            </ul>
  <?php }  
  /****************For Customer Service*******************/
  if($_SESSION['department_id']=='9'){ ?> 
  
  <ul class="nav navbar-nav" >
          
              <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            
                               <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='show') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=show">Initiate New files</a></li> 
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='pending_desk') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='pending') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=pending">Pending File</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='reject') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=reject">Reject File</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='close') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=close">close File</a></li>       
             
           <!--<li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                       <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
<li><a href="index.php?control=fts&task=reject">Reject File</a></li>
<li><a href="index.php?control=fts&task=close">close File</a></li>
                </ul>
              
              </li>-->
              
              
               <!--<li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" ><a href="index.php?control=user_feedback">Feedback/Complain</a></li>-->
               <!--<li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Feedback/Complain<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=user_feedback">Underprocess Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=close">Close Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=forwardtome_complain">Forward To Me Feedback/Complain</a></li>
                </ul>              
              </li>-->
              
              <!--<li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                </ul>
              
              </li>-->
              
            </ul>
  <?php }  
  /****************For Fire & safety*******************/
  if($_SESSION['department_id']=='10'){ ?> 
  
  <ul class="nav navbar-nav" >
          
              <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            
                               <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='show') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=show">Initiate New files</a></li> 
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='pending_desk') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='pending') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=pending">Pending File</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='reject') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=reject">Reject File</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='close') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=close">close File</a></li>       
             
           <!--<li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                        <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
                        <li><a href="index.php?control=fts&task=reject">Reject File</a></li>
                        <li><a href="index.php?control=fts&task=close">close File</a></li>
                </ul>
              
              </li>-->
              
              
               <!--<li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" ><a href="index.php?control=user_feedback">Feedback/Complain</a></li>-->
              <!-- <li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Feedback/Complain<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=user_feedback">Underprocess Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=close">Close Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=forwardtome_complain">Forward To Me Feedback/Complain</a></li>
                </ul>              
              </li>
            
            <li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                </ul>
              
              </li>  -->
              
              
            </ul>
  <?php } ?>
   
  <?php } ?>          
 <!------------------------------------------Admin Menu-END------------------------------------>
 
 <!------------------------------------------Admin Menu-Start---------------------------------->
 <?php  
if($_SESSION['utype']=='Admin'){ ?>

<ul class="nav navbar-nav" >
          
              <li class="<?php if($_REQUEST['control']=='') {echo "panel-red";} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
             <!--   <li class="dropdown <?php if(in_array($_REQUEST['control'],$master) ||  $_REQUEST['task']=='show_event') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Masters<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li class="dropdown-submenu"><a href="#">Regional Hierarchy</a>
                        <ul class="dropdown-menu">
                        <li><a href="index.php?control=regional_hierarchy&task=show_country">Country</a></li>
                        <li><a href="index.php?control=regional_hierarchy&task=show_state">State</a></li>
                        <li><a href="index.php?control=regional_hierarchy&task=show_city">City</a></li>
                        <li><a href="index.php?control=regional_hierarchy&task=show_area">Area</a></li> 
                      <li><a href="index.php?control=regional_hierarchy&task=show_society">Society</a></li>
                      <li><a href="index.php?control=regional_hierarchy&task=show_scheme">Scheme</a></li>
                      <li><a href="index.php?control=regional_hierarchy&task=show_new_connection_scheme">New Connection Scheme Setup</a></li>
                        </ul>
                        </li>
                
                        <li><a href="index.php?control=hrm_department">Department Master</a></li>
                        <li><a href="index.php?control=hrm_post">Employee Designation</a></li>
                        <li><a href="index.php?control=complain_type">Complain Type</a></li>
                        <li><a href="index.php?control=hrm_registration">Employee Registration</a></li>
                </ul>
              
              </li>-->
            <!--   <li class="<?php if($_REQUEST['control']=='customer' && $_REQUEST['task']!='payment_status') {echo "panel-red";} ?>"><a href="index.php?control=customer">Customer</a></li> -->

               <!--<li class="dropdown <?php if($_REQUEST['control']=='customer') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Customer<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=customer">Customer List</a></li>
                        <li><a href="index.php?control=customer&task=meter_reading">Customer Meter Reading</a></li>
                        <li><a href="index.php?control=customer&task=update_contact_detail">Update Customer Details</a></li>
                </ul>
              
              </li>--> 
               
               <!--<li class="dropdown <?php if($_REQUEST['control']=='customer' && $_REQUEST['task']=='payment_status') {echo "panel-red";} ?>" ><a href="index.php?control=customer&task=payment_status">Customer Payment</a></li>--> 
               
               <!--<li class="dropdown <?php if($_REQUEST['control']=='user_applynewconnection') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Connection<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=user_applynewconnection">Success Connection</a></li>
                        <li><a href="index.php?control=user_applynewconnection&task=pending_connection">Pending Connection</a></li>
                </ul>
              
              </li>-->
               
             <!--  <li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" ><a href="index.php?control=user_feedback">Feedback/Complain</a></li>-->
               
               <!--<li class="dropdown <?php if($_REQUEST['control']=='user_feedback') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Feedback/Complain<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=user_feedback">Underprocess Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=close">Close Feedback/Complain</a></li>
                        <li><a href="index.php?control=user_feedback&task=forwardtome_complain">Forward To Me Feedback/Complain</a></li>
                </ul>              
              </li>
              
              
               <li class="dropdown <?php if($_REQUEST['control']=='tender' && $_REQUEST['task']!='download_tender') {echo "panel-red";} ?>" ><a href="index.php?control=tender">Tender</a></li>
                  <li class="dropdown <?php if($_REQUEST['control']=='tender' && $_REQUEST['task']=='download_tender') {echo "panel-red";} ?>" ><a href="index.php?control=tender&task=download_tender">Download Tender</a></li>-->
                  
                   <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='show') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=show">Initiate New files</a></li> 
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='pending_desk') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='pending') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=pending">Pending File</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='reject') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=reject">Reject File</a></li>
                        <li class="<?php if($_REQUEST['control']=='fts' && $_REQUEST['task']=='close') {echo "panel-red";} ?>"><a href="index.php?control=fts&task=close">close File</a></li>
                 
                 <!--<li class="dropdown <?php if($_REQUEST['control']=='fts') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FTS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=fts">Initiate New files</a></li>
                        <li><a href="index.php?control=fts&task=pending_desk">Pending File On Desk</a></li>
                        <li><a href="index.php?control=fts&task=pending">Pending File</a></li>
                        <li><a href="index.php?control=fts&task=reject">Reject File</a></li>
                        <li><a href="index.php?control=fts&task=close">close File</a></li>
                </ul>
              
              </li>-->
              
              
              <!--<li class="dropdown <?php if($_REQUEST['control']=='bms') {echo "panel-red";} ?>" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">BMS<b class="caret"></b></a>
                <ul class="dropdown-menu">
                        <li><a href="index.php?control=bms">Initiate New Bill</a></li>
                        <li><a href="index.php?control=bms&task=pending_desk">Pending Bill On Desk</a></li>
                        <li><a href="index.php?control=bms&task=pending">Pending Bill</a></li>
                        <li><a href="index.php?control=bms&task=reject">Reject Bill</a></li>
                        <li><a href="index.php?control=bms&task=close">Close Bill</a></li>
                </ul>
              
              </li>-->
          
            </ul>
  
  <?php } ?>          
 <!------------------------------------------Admin Menu-END------------------------------------>
 

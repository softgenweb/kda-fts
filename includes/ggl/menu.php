<style>
   .dropdown-submenu{
   position:relative;
   display:block;
   }
   .dropdown-submenu>.dropdown-menu {
   top:1px;
   left: 100%;
   margin-top: -6px;
   margin-left: -1px;
   -webkit-border-radius: 0 6px 6px 6px;
   -moz-border-radius: 0 6px 6px;
   border-radius: 0 6px 6px 6px;
   }
   .dropdown-submenu:hover>.dropdown-menu {
   display: block !important;
   }
   .dropdown-submenu>a:after {
   display: block;
   content: " ";
   float: right;
   width: 0;
   height: 0;
   border-color: transparent;
   border-style: solid;
   border-width: 5px 0 5px 5px;
   border-left-color: #ccc;
   margin-top: 5px;
   margin-right: -10px;
   }
   .dropdown-submenu:hover>a:after {
   border-left-color: #fff;
   }
   .dropdown-submenu.pull-left {
   float: none;
   }
   .dropdown-submenu.pull-left>.dropdown-menu {
   left: -100%;
   margin-left: 10px;
   -webkit-border-radius: 6px 0 6px 6px;
   -moz-border-radius: 6px 0 6px 6px;
   border-radius: 6px 0 6px 6px;
   }
   .navbar-nav a{ color:#FFF !important;
   /*background-color:#30a5ff !important;*/
   }
   <!--.Active{ background-color:#30a5ff !important; }-->
   .dropdown-menu {
   background-color: #125C95 !important;
   }
   .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
   background-color: #080808 !important;
   color: #262626;
   text-decoration: none;
   }
   a{ /*color:#FFF !important;*/} 
   .Active{background-color:#30a5ff !important;}
</style>
<?php 
   $master = array('hrm_department','regional_hierarchy','hrm_post','hrm_registration');
   $hrm = array('hrm_post','hrm_registration','hrm_leave','hrm_salary','hrm_post');
   //$emp_task = array('show_emp');
   $lgm = array('customer','customer_caller');
   $report = array('hrm_attendence','lgm_report');
   $target = array('master_target','master_caller_target');
   $emi= array('master_emi');
   $outbound = array('hrm_attendence','outbound_report');
   ?>
<!-- ======================Super Admin Menu==================== -->
<?php  
   if($_SESSION['utype']=='Super-Admin'){ ?>
<ul class="nav navbar-nav" >
      <li class=" <?php if($_REQUEST['control']==''){ echo('panel-red');} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>

   <li class="dropdown <?php if($_REQUEST['control']=='hrm_department' || $_REQUEST['control']=='hrm_registration'){ echo('panel-red');} ?>" >
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Masters<b class="caret"></b></a>
      <ul class="dropdown-menu">
         <li><a href="index.php?control=hrm_department">Department Master</a></li>
         <!-- <li><a href="index.php?control=hrm_post">Designation</a></li> -->
         <li><a href="index.php?control=hrm_registration">Employees</a></li>
      </ul>
   </li>

   <li class="<?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='search') { echo 'panel-red';} ?>"><a href="index.php?control=report&task=search">Search Files</a></li>
   <!-- <li class="<?php if($_REQUEST['control']=='file_transfer' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='file_detail'|| $_REQUEST['task']=='addnew' )) { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=addnew"> New files</a></li> -->
   <!-- <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='received' ) { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=received">All Files</a></li> -->
   <li class="<?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='report'){ echo 'panel-red';} ?>"><a href="index.php?control=report&task=report">Report</a></li>
   <li class="<?php if($_REQUEST['control']=='file_transfer' && ( $_REQUEST['task']=='daily_report' )) { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=daily_report"> Daily Report </a></li>
 <li class="dropdown <?php if($_REQUEST['control']=='report' && ($_REQUEST['task']=='server_log' || $_REQUEST['task']=='file_log')){ echo('panel-red');} ?>" >
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Activity Logs<b class="caret"></b></a>
      <ul class="dropdown-menu">
         <li><a href="index.php?control=report&task=server_log">Server Logs</a></li>
         <!-- <li><a href="index.php?control=hrm_post">Designation</a></li> -->
         <li><a href="index.php?control=report&task=file_log">File Transfer Logs</a></li>
      </ul>
   </li>
</ul>
<?php } ?> <!-- ====================== Admin Menu==================== -->
<?php  
   if($_SESSION['utype']=='Admin'){ ?>
<ul class="nav navbar-nav" >
   <li class=" <?php if($_REQUEST['control']==''){ echo('panel-red');} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
   <li class="dropdown <?php if($_REQUEST['control']=='hrm_department' || $_REQUEST['control']=='hrm_registration'){ echo('panel-red');} ?>" >
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Masters<b class="caret"></b></a>
      <ul class="dropdown-menu">
         <li><a href="index.php?control=hrm_department">Department Master</a></li>
         <!-- <li><a href="index.php?control=hrm_post">Designation</a></li> -->
         <li><a href="index.php?control=hrm_registration">Employees</a></li>
      </ul>
   </li>

   <!-- <li class="<?php if($_REQUEST['control']=='file_transfer' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='file_detail'|| $_REQUEST['task']=='addnew' )) { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=addnew"> New files</a></li> -->

   <li class="<?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='search') { echo 'panel-red';} ?>"><a href="index.php?control=report&task=search">Search Files</a></li>
   <li class="<?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='report'){ echo 'panel-red';} ?>"><a href="index.php?control=report&task=report">Report</a></li>

</ul>
<?php } ?>          
<!-- =========================Record Room Menu=========================== -->
<?php  
   if($_SESSION['department_id']=='2'){  
   // if($_SESSION['utype']=='Record Room'){  
 
  ?>
<ul class="nav navbar-nav" >
   <li class=" <?php if($_REQUEST['control']==''){ echo('panel-red');} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
   
   <li class="<?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='search') { echo 'panel-red';} ?>"><a href="index.php?control=report&task=search">Search Files</a></li>
   <li class="<?php if($_REQUEST['control']=='file_transfer' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='file_detail'|| $_REQUEST['task']=='addnew' )) { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=addnew">New file</a></li>
      <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='received' ) { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=received&rr_files=1">Record Room Files</a></li>
   <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='assigned') { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=assigned">Moved Files</a></li>
   <li class="<?php if($_REQUEST['control']=='file_transfer' && ( $_REQUEST['task']=='daily_report' )) { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=daily_report"> Daily Report </a></li>
   <!-- <li class="<?php if($_REQUEST['control']=='report' && ( $_REQUEST['task']=='figer_print' )) { echo 'panel-red'; } ?>"><a href="index.php?control=report&task=figer_print"> Check Finger Print </a></li> -->
</ul>

<?php } ?>           
<!-- =========================Department Menu=========================== -->
<?php  
   // if($_SESSION['utype']=='Record Room'){  
   if($_SESSION['utype']=='Employee' && $_SESSION['department_id']!='2'){  
  ?>

 <ul class="nav navbar-nav" >
   <li class="<?php if($_REQUEST['control']==''){ echo 'panel-red';} ?>"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
  
   <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='tobe_receive') { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=tobe_receive">File To be received</a></li>
   <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='received') { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=received">Received Files</a></li>
   <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='assigned') { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=assigned">Assigned Files</a></li>
   <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='dispatched') { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=dispatched">Dispatched Files</a></li>
   <li class="<?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='search') { echo 'panel-red';} ?>"><a href="index.php?control=report&task=search">Search Files</a></li>
    <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='addnew') { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=addnew">Add New files</a></li>
   <!-- <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='closed') { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=closed">Closed Files</a></li> -->
   <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='exceeded') { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=exceeded">Exceeded Files</a></li>
  <!--  <li class="<?php if($_REQUEST['control']=='file_transfer' && $_REQUEST['task']=='stored_record') { echo 'panel-red';} ?>"><a href="index.php?control=file_transfer&task=stored_record">Storage Files</a></li> -->
</ul>
<?php } ?>          


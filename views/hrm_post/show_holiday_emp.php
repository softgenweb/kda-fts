<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:5;?>

    <div class="col-sm-12">		
    
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    	<li class="active"><a href="index.php?control=hrm_post&task=show_holiday_emp">Reports</a></li>
                    <li class="active">Holiday Calendar Report</li>
                </ol>
            </div>
        </div>	
				
            <div class="row" >
    <div class="col-md-12">
    <div class="panel panel-default">
		<div class="panel-body">
			<div class="col-md-3 col-sm-12">
			   <div class="panel-heading">
					<u>
					   <h3>Holiday Calendar Report</h3>
					</u> 
			   </div>
			</div>
			<div class="col-md-9 col-sm-12" style="margin-top:1%">
				<div class="col-md-8 col-sm-7 col-xs-12">
					<?php foreach($results as $result) { }  ?>
						<span class="col-md-6" style="top: 8px; color:<?php if($result[0]['id']!=''){ echo 'green';} else { echo 'red';}?>">
						   <?php  
								if($_REQUEST['search']!=''){
									if($result[0]['id']!=''){ 
										echo "Welcome! " .$result['fname'];
									}
									else{ 
										echo "Sorry! This Name Does Not Exists.";
									}
								}				
							?>
						</span>
					<form class="col-md-6" name="customer_search" action="index.php" method="post">
                    <div class="input-group">
						<!--<input type="text" id="btn-input" class="form-control input-md" name="search" value="<?php echo $_REQUEST['search']?$_REQUEST['search']:$_REQUEST['id'];?>" placeholder="Type your search here..." />
						<span class="input-group-btn">
							<button class="btn btn-primary btn-md" id="btn-chat">Search</button>
						</span>-->
						<input type="hidden" name="control" value="hrm_post"/>
						<input type="hidden" name="view" value="show_holiday"/>
						<input type="hidden" name="task" value="show_holiday"/>
                        </div>
					</form>
				</div>
			
		
			<div class="col-md-3 col-sm-4 col-xs-9 addd_button">			
			
			</div>
			
                <div class="col-md-1 col-sm-1 col-xs-3 for_paging">
                            
                </div>
            </div>
            
            
			<div class="container">
				<div class="col-md-12"></div>
			</div>
			<br>
			<table style="width:100%" data-toggle="table" id="table-style"  data-row-style="rowStyle">
				<thead>
					<tr>
                        <th><div align="center">S.No.</div></th>
                        <th><div align="center">Holiday Name</div></th>
                      
                        <th><div align="center">Holiday Date</div></th>
                         <th><div align="center">Day</div></th>
                        <!--<th><div align="center">Action</div></th>-->
					  </tr>
				</thead>
				<?php
					if($results) {
						$countno = ($page-1)*$tpages;
						$i=0;
						foreach($results as $result){
						$i++;
						$countno++;
						
					
						($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
						
					?>
	
						<tr>
							<td class="<?php echo $class; ?>"><div align="center"><?php echo $countno; ?></div></td>
							<td class="<?php echo $class; ?>"><div align="center"><?php echo $result['name'] ; ?></div></td>
                          
							<td class="<?php echo $class; ?>"><div align="center"><?php echo date(('d-M-Y'),strtotime($result['leaveto'])); ?></div></td>
                            <td class="<?php echo $class; ?>"><div align="center"><?php echo $result['day'] ; ?></div></td>
							<!--<td class="<?php echo $class; ?>"><div align="center">
								<a onclick="customer_edit(<?php echo $result['id']; ?>)" style="cursor:pointer;" title="Edit"><img src="images/edit.png" alt="edit" title="Edit" /></a>
								<?php if($result['status']){  ?>
								<a onclick="customer_status(<?php echo $result['id']; ?>,0)" style="cursor:pointer;" title="Click to Unpublish"><img src="images/backend/check.png" alt="check_de" /></a>
								<?php } else { ?>
								<a  onclick="customer_status(<?php echo $result['id']; ?>,1)" style="cursor:pointer;" title="Click to Publish"><img src="images/backend/check_de.png" alt="check_de" /></a>
								<?php } ?>
								<a onclick="customer_delete(<?php echo $result['id']; ?>)" title="Delete"  ><img src="images/backend/del.png" alt="Delete" title="Delete" /></a>


								</div>
							</td>-->
						</tr>
					<?php }  
					} else{ ?>
				<div>
			   <!-- <strong>Data not found.</strong>-->
				</div>
			   <!-- <tbody><tr class="no-records-found"><td colspan="11">No matching records found</td></tr></tbody>-->
				<?php } ?>
			
			</table>
		</div>
			
			</div>
            </div>
            </div>
            </div>			
			
			
<div id="myModal" class="reveal-modal">
    <div id="view_popup"></div>
	<a class="close-reveal-modal"><!--<img src="assets/popup/images/close.png" width="23" height="23" />--></a> 
</div>

              
              
<script type="text/javascript">
	function customer_edit(id)
	{
		//alert(id);
		document.getElementById("edit").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "addnew_holiday";
		document.getElementById("id").value = id;
		//document.getElementById("pageNo").value = Number(document.getElementById("page").value)*Number(document.getElementById("filterForm").value);
		/*document.getElementById("searchForm").action="/betcha/index.php/promotion/edit/"+edit;*/
		document.filterForm.submit(); 
	}
	
	function customer_status(id,status)
	{ 
		document.getElementById("id").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "status_holiday";
		document.getElementById("id").value = id;
		document.getElementById("status").value = status;
		document.filterForm.submit(); 
	}
	
	function customer_delete(id)
	{
		document.getElementById("del").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "delete_holiday";
		document.getElementById("id").value = id;
	
		if(confirm('Are you sure you want to delete ?')) {
			document.filterForm.submit(); 
		}
		else
		{
		//alert("No");
		}
	}
	
	function addnew_customer(id)
	{
		document.getElementById("edit").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "addnew_customer";
		document.getElementById("id").value = id;
		//alert(id);
		//document.getElementById("pageNo").value = Number(document.getElementById("page").value)*Number(document.getElementById("filterForm").value);
		/*document.getElementById("searchForm").action="/betcha/index.php/promotion/edit/"+edit;*/
		document.filterForm.submit(); 
	}
	
	function add_test(id)
	{
		document.getElementById("edit").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "add_test";
		document.getElementById("id").value = id;
		//alert(id);
		//document.getElementById("pageNo").value = Number(document.getElementById("page").value)*Number(document.getElementById("filterForm").value);
		/*document.getElementById("searchForm").action="/betcha/index.php/promotion/edit/"+edit;*/
		document.filterForm.submit(); 
	}
	  
	  
	
</script> 

<div class="col-md-10 col-md-offset-1">
   <div class="row">
      <div class="col-lg-12">
         <h2 class="page-header" align="center"><?php if($_SESSION['utype']=="Admin"){ ?>Record Room Dashboard <?Php } ?></h2>
      </div>
   </div>
   <br>
   <?php if($_SESSION['utype']=="Admin"){ ?> 
   <div class="row">
      <div class="container2">
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=show">
               <div class="panel panel-teal panel-widget ">
                  <div class="row no-padding district">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php echo $customer = mysql_num_rows(mysql_query("SELECT * FROM `file_record` WHERE 1"));
                           ?></div>
                        <div class="large">Total Files</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-user" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=pending_desk">
               <div class="panel panel-blue panel-widget ">
                  <div class="row no-padding assembley">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php echo $customer = mysql_num_rows(mysql_query("SELECT * FROM fts_initiate_file where initiate_by_action='Pending'")); ?></div>
                        <div class="large">My Pending Files</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=pending">
               <div class="panel panel-teal panel-widget">
                  <div class="row no-padding mohalla">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php echo $lgm = mysql_num_rows(mysql_query("SELECT * FROM `fts_initiate_file`  where initiate_by_action='Pending'")); ?></div>
                        <div class="large ">Assigned Files</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=reject">
               <div class="panel panel-teal panel-widget ">
                  <div class="row no-padding category">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php echo $lgm = mysql_num_rows(mysql_query("SELECT * FROM `fts_initiate_file`  where initiate_by_action='Not Approved'")); ?></div>
                        <div class="large">Dispatched Files</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-comments" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=close">
               <div class="panel panel-teal panel-widget ">
                  <div class="row no-padding district">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php  echo $lgm = mysql_num_rows(mysql_query("SELECT * FROM `fts_initiate_file`  where initiate_by_action='Approved'"));?></div>
                        <div class="large">Search Files</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-list-alt" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=show">
               <div class="panel panel-blue panel-widget ">
                  <div class="row no-padding assembley">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php echo $customer = mysql_num_rows(mysql_query("SELECT * FROM `fts_initiate_file` ORDER BY `id` DESC"));
                           ?></div>
                        <div class="large">Add New Files</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-users" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=user_applynewconnection">
               <div class="panel panel-teal panel-widget">
                  <div class="row no-padding mohalla">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php //echo $suss = mysql_num_rows(mysql_query("SELECT * FROM `new_connection` where payment_status='SUCCESS'")); ?></div>
                        <div class="large ">Files Exceeded 15 Days</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=report">
               <div class="panel panel-teal panel-widget">
                  <div class="row no-padding category">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php //echo $suss = mysql_num_rows(mysql_query("SELECT * FROM `new_connection` where payment_status='SUCCESS'")); ?></div>
                        <div class="large ">Report</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-file-text-o fa-5x" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
      </div>
   </div>
   <!--/.row-->
   <?Php }else{ ?>  
   <div class="row">
      <div class="container2">
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=show">
               <div class="panel panel-teal panel-widget ">
                  <div class="row no-padding district">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php echo $customer = mysql_num_rows(mysql_query("SELECT * FROM `fts_initiate_file` ORDER BY `id` DESC"));
                           ?></div>
                        <div class="large">Received Files</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-user" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=reject">
               <div class="panel panel-teal panel-widget ">
                  <div class="row no-padding category">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php echo $lgm = mysql_num_rows(mysql_query("SELECT * FROM `fts_initiate_file`  where initiate_by_action='Not Approved'")); ?></div>
                        <div class="large">Dispatched Files</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-comments" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=close">
               <div class="panel panel-teal panel-widget ">
                  <div class="row no-padding district">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php  echo $lgm = mysql_num_rows(mysql_query("SELECT * FROM `fts_initiate_file`  where initiate_by_action='Approved'"));?></div>
                        <div class="large">Search Files</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-list-alt" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="index.php?control=fts&task=show">
               <div class="panel panel-blue panel-widget ">
                  <div class="row no-padding assembley">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                        <div class="digit "><?php echo $customer = mysql_num_rows(mysql_query("SELECT * FROM `fts_initiate_file` ORDER BY `id` DESC"));
                           ?></div>
                        <div class="large">Add New Files</div>
                     </div>
                     <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-users" aria-hidden="true"></i>
                     </div>
                     <div class="clearfix"></div>
                     <div class="block-footer">
                        <strong>
                           <div class="more_info">More info <i class="glyphicon glyphicon-circle-arrow-right"></i></div>
                        </strong>
                     </div>
                  </div>
               </div>
            </a>
         </div>
      </div>
   </div>
   <!--/.row-->
   <?Php } ?>  
</div>


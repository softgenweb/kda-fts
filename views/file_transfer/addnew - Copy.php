<?php session_start(); ?>
<?php foreach($results as $result) { }  ?>
<div class="col-md-12">
   <div class="row">
      <div class="col-lg-12">
         <ol class="breadcrumb">
            <li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
            <!-- <li class="active"><a href="index.php?control=file_transfer">FTS</a></li> -->
            <li class="active"> <?php if($result['id']!='') { ?> Edit <?php } else { ?>Add New<?php } ?>  file </li>
         </ol>
      </div>
   </div>
   <!--/.row-->
</div>
<div class="col-md-12">
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="panel-heading">
            <u>
               <h3><?php if($result['id']!='') { ?> Edit <?php } else { ?>Add New <?php } ?> file</h3>
            </u> 
             <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-primary btn-md" id="btn-chat" style="float:right;">Back</a>
            
         </div>
        
         
         <br>
         <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
            <div class="col-md-8 col-md-offset-1">
               <?php if($_REQUEST['id']!=''){ ?>
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     UID NO :
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" placeholder="Auto Generate" name="uid" id="uid" value="<?php echo $result['uid']; ?>" readonly/>    
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div><?php } ?>

               <div class="clearfix"></div>
               <?php if($_SESSION['utype']=='Admin' || $_SESSION['utype']=='Record Room'){ ?>
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     Department:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <select class="form-control" name="department" id="department">
                        <option value="">Select</option>
                        <?php
                        $depart = mysql_query("SELECT * FROM `department` WHERE `status`=1 AND `id` NOT IN (1,2)");
                         while($row = mysql_fetch_array($depart)){ ?>
                        <option value="<?php echo $row['id']; ?>" <?php echo $result['department_id']==$row['id']?'selected':''; ?>><?php echo $row['name'] ?></option>
                        <?php } ?>                          
                     </select>  
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
                <!--  <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     Assign to:
                  </div>
               </div>
             <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <select class="form-control" name="assigned_to" id="assigned_to">
                        <option value="">Select</option>
                        <?php
                        $value = $result['assigned_to']?$result['assigned_to']:$result['dispatched_to'];
                        $user = mysql_query("SELECT * FROM `users` WHERE `id`='".$value."' AND `status`=1");
                         while($rows = mysql_fetch_array($user)){ ?>
                        <option value="<?php echo $row['id']; ?>" <?php echo $value==$rows['id']?'selected':''; ?>><?php echo $row['name'] ?></option>
                        <?php } ?>                          
                     </select>  
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div> -->
               <div class="clearfix"></div>
            <?php }else{ ?>
               <input type="hidden" name="department" value="<?php echo $_SESSION['department_id']; ?>">
               <input type="hidden" name="assigned_to" value="<?php echo $_SESSION['adminid']; ?>">
            <?php } ?>
               <div class="clearfix"></div>
               <!-- <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     File Name:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="file_name" id="file_name" value="<?php echo $result['file_name']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div> 
               <div class="clearfix"></div>--> 
               <div class="clearfix"></div>
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     Old No:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="old_no" id="old_no" value="<?php echo $result['old_no']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div>
                <!-- <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     New Number:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="new_no" id="new_no" value="<?php echo $result['new_no']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div> -->
               <div class="clearfix"></div>
            <div id="property" style=" display: <?php echo $_SESSION['department_id']!='3'?'none;':'block'; ?>">
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     Zone:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="zone" id="zone" value="<?php echo $result['zone']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                    Category:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="category" id="category" value="<?php echo $result['category']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
         </div>
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     Property No.:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="property_no" id="property_no" value="<?php echo $result['property_no']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     Sceme Name:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="sceme_name" id="sceme_name" value="<?php echo $result['sceme_name']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     Name of Allotee:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="allotee" id="allotee" value="<?php echo $result['allotee']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <!-- <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     Subject:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="subject" id="subject" value="<?php echo $result['subject']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div> -->
              
              <!--  <div class="clearfix"></div>
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     Owner:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="owner" id="owner" value="<?php echo $result['owner']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div> -->
              
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     File Description :
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <textarea class="form-control" name="file_desc" id="file_desc" ><?php echo $result['description']; ?></textarea>  
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     Priority:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                    <select class="form-control add_priority" id="add_priority" onchange="get_priority(this.value,<?php echo $i; ?>)" name="add_priority">
                              <option value="">Select</option>
                              <option value="Urgent" <?php echo $result['priority']=='Urgent'?'selected':''; ?>>Urgent</option>
                              <option value="High" <?php echo $result['priority']=='High'?'selected':''; ?>>High</option>
                              <option value="Normal" <?php echo $result['priority']=='Normal'?'selected':''; ?>>Normal</option>
                           </select>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3 col-md-offset-2">
                  <div class="form-group" >
                     LOT No.:
                  </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
                  <div class="form-group">
                     <input type="text" class="form-control" name="lot_no" id="lot_no" value="<?php echo $result['lot_no']; ?>" />   
                     <span id="msgname" style="color:red;"></span>
                  </div>
               </div>
  
               </br>
               <div class="col-md-12" align="center">
                  <div class="form-group">
                     <button type="submit" class="btn btn-primary"><?php if($result['id']) { echo "Update"; } else { echo "Submit";} ?></button>
                     <!--<button type="submit" class="btn btn-primary">Send</button>-->
                  </div>
               </div>
            </div>
            <!-- <input type="hidden" name="initiate_by_action" id="initiate_by_action" value="Pending" placeholder="Pending"/>  -->
            <input type="hidden" name="control" value="file_transfer"/>
            <input type="hidden" name="edit" value="1"/>
            <input type="hidden" name="task" value="save"/>
            <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
            <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
         </form>
      </div>
   </div>
</div>


<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   
   $('#tender_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'Y/m/d',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });

$('#department').change(function(){
   var id = $(this).val();
   // $("input[type=text]").prop('required',true);
   $.get( "script/popup_scripts/employee.php?id="+id, function( data ) {
  $("#assigned_to").html(data);

   });

   if(id == '3'){
      $('#property').show();

   }else if(id==''){
      $('#property').hide();
   }else{
      $('#property').hide();
   }
});


</script>




<?php session_start(); ?>
<?php foreach($results as $result) { }  ?>
<div class="col-md-12">
   <div class="row">
      <div class="col-lg-12">
         <ol class="breadcrumb">
            <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
            <!-- <li class="active"><a href="index.php?control=file_transfer">FTS</a></li> -->
            <li class="active">File Detail</li>
            <?php if($_SESSION['back_btn']!="1"){ ?>
              <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-primary btn-md" id="btn-chat" style="float:right;">Back</a>
               <?php }else{ ?>
			 
              <a href="#"  class="btn btn-primary btn-md" id="btn-chat" style="float:right;">Back</a>
           <?php } unset($_SESSION['back_btn']); ?>
         </ol>
      </div>
   </div>
   <!--/.row-->
</div>
<div class="col-md-12">
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="panel-heading">
           
               <h3>File Detail
            
            <span class="pull-right"><strong>
                           <?php if($_SESSION['utype']=='Admin' ){ ?>
                           <?php //if($_SESSION['utype']=='Admin' || $_SESSION['utype']=='Record Room' || $result['created_by']==$_SESSION['adminid']){ ?>
                           <a class="btn btn-primary" href="index.php?control=file_transfer&task=addnew&id=<?php echo $result['id']; ?>">Edit</a>
                        <?php } ?></strong>&nbsp;&nbsp;&nbsp;&nbsp;
                         <?php if($result['department_type']!='2'){ ?> 
<a class="btn btn-primary" target="_blank" href="script/print/print_label.php?uid=<?php echo $result['uid']; ?>">Print Label</a><?php } ?>
                        </span></h3>
                        
         </div>

         <br>
         <!-- <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();"> -->
            <div class="col-md-8 col-md-offset-2">
             
            
             <div id="uid" >
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     UID NO (New File No):
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="uid" value="<?php echo $result['uid']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="abcd" >
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                    Owner Department:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  value="<?php echo $this->departmentName($result['department_id']); ?>"  disabled=""/>   
                     
                  </div>
               </div></div>
               <!-- <div class="clearfix"></div> -->
             <?php if($result['department_type']=='2'){ ?> 
            
               <div id="old_file_no" >
               <div class="col-md-3">
                  <div class="form-group pull-right" >File Subject        </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  value="<?php echo $result['old_no']; ?>"  disabled=""/>  
                  </div>
               </div>
            </div>
			 
			 <?php } else{ ?>
             
               <div id="old_file_no" >
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Property Id :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="old_file_no" value="<?php echo $result['old_no']; ?>"  disabled=""/>  
                  </div>
               </div>
            </div>
            
              <div id="property_no">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Property No. :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="property_no"  value="<?php echo $result['property_no']; ?>"  disabled=""/>    
                     
                  </div>
               </div>               
               </div>
            
            
               <div id="zone" >
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Zone :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="zone" value="<?php echo $result['zone']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div  id="category">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Category:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="category" value="<?php echo $result['category']; ?>"  disabled=""/>   
                     
                  </div>
               </div></div>
               <!-- <div class="clearfix"></div> -->
             
          
           
               
               
               
               <div id="sceme_name">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Scheme Name:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="sceme_name" id="" value="<?php echo $result['sceme_name']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
             
             </div>
             <div id="allotee" >
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Allotte :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="allotee" id="" value="<?php echo $result['allotee']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
              
               
              
               
               <!-- <div class="clearfix"></div> -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
          
             <div id="name" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Name :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="name" id="" value="<?php echo $result['name']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="pensioner_name" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Pensioner Name:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="pensioner_name" id="" value="<?php echo $result['pensioner_name']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
               <div id="father_name" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Father's Name :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="father_name" id="" value="<?php echo $result['father_name']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="department_name" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Department:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="department_name" id="" value="<?php echo $result['department_name']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
               <div id="post" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Post :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="post" id="" value="<?php echo $result['post']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="dob" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Date of Birth:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="dob" id="" value="<?php echo $result['dob']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
               <div id="appointment_date" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Date of Appointment :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="appointment_date" id="" value="<?php echo $result['appointment_date']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="retirement_date" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                    Date of Retirement:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="retirement_date" id="" value="<?php echo $result['retirement_date']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
               <div id="relation" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Relation :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="relation" id="" value="<?php echo $result['relation']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="contract_name" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Contractor Name:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="contract_name" id="" value="<?php echo $result['contract_name']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
               <div id="work_name" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Name of Work :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="work_name" id="" value="<?php echo $result['work_name']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="mb_num" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     MB Number:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="mb_num" id="" value="<?php echo $result['mb_num']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
               <div id="year" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Year :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="year" id="" value="<?php echo $result['year']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="section_name" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Section Name:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="section_name" id="" value="<?php echo $result['section_name']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
               <div id="pentition_id" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Petition ID No :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="pentition_id" id="" value="<?php echo $result['pentition_id']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="file_type" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     File Type:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="file_type" id="" value="<?php echo $result['file_type']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
               <div id="court_name" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Court Name :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="court_name" id="" value="<?php echo $result['court_name']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="related_depart" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                    Related Department:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="related_depart" id="" value="<?php echo $result['related_depart']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
               <div id="advocate_name" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Advocate Name :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="advocate_name" id="" value="<?php echo $result['advocate_name']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="property_detail" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Property Detail:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="property_detail" id="" value="<?php echo $result['property_detail']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
               <div id="detail" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Detail :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control"  name="detail" id="" value="<?php echo $result['detail']; ?>"  disabled=""/>    
                     
                  </div>
               </div>
               
               </div>
               <div id="address" style="display:none;">
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Address:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="address" id="" value="<?php echo $result['address']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
              
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->
             <!-- ================================================================== -->               
          </div>
             <div id="" >
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Created By:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <?php $creater = mysql_fetch_array(mysql_query("SELECT * FROM `users` WHERE `id`='".$result['created_by']."'")); ?>
                     <input type="text" class="form-control" name="old_no" id="" value="<?php echo $creater['name']; ?>"  disabled=""/>   
                     
                  </div>
               </div>
              
               
               </div>
               <div id="new_no" >
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     Created Date:
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="new_no" id="" value="<?php echo substr($result['created_date'],0,16); ?>"  disabled=""/>   
                     
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
            </div>
            
            <div class="clearfix"></div> 
        
           
               <form name="form" autocomplete="off" method="post" enctype="multipart/form-data" >
                <div id="lot_no" >
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                       LOT No. :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="lot_no" id="lot_number" value="<?php echo $result['lot_no']; ?>"  disabled=""/>          
                     
                  </div>
               </div>
               <?php } ?>
                <div class="col-md-3" align="left">
               <div class="form-group"> <?php if($_SESSION['department_id'] =='2' ){ ?>
                   <img src="images/edit.png" alt=""  onclick="lotshow_button()" id="lotimg_hide" style="cursor:pointer;"/>
                  <button type="submit" id="lot_btn" class="btn btn-primary" style="display:none;">Update</button>    
                                
                    <input type="hidden" name="control" value="file_transfer"/>
                    <input type="hidden" name="task" value="lot_save"/>
                    <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                    <input type="hidden" name="tpages" id="tpages" value="<?php echo $result['tpages']; ?>"  />
                    <input type="hidden" name="page" id="page" value="<?php echo $result['page'];?>"  />
                    <?php } ?>
               </div>
            </div>
               
               </div>
               </form>
             
                     <div class="clearfix"></div> 
                
              <?php if($_SESSION['department_id'] =='2' ){ ?>
               <form name="form" autocomplete="off" method="post" enctype="multipart/form-data">
               <div id="rack_location" >
               <div class="col-md-3">
                  <div class="form-group pull-right" >
                     File Rack Location :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <input type="text" class="form-control" name="file_rack_location" id="file_rack_location" value="<?php echo $result['file_rack_location']; ?>" disabled="" >  
                     
                     
                  </div>
               </div>
               
                <div class="col-md-3" align="left">
               <div class="form-group">
                   <img src="images/edit.png" alt=""  onclick="show_button()" id="img_hide" style="cursor:pointer;"/>
                  <button type="submit" id="rack_btn" class="btn btn-primary" style="display:none;">Update</button>                  
                    <input type="hidden" name="control" value="file_transfer"/>
                    <input type="hidden" name="task" value="rack_save"/>
                    <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                    <input type="hidden" name="tpages" id="tpages" value="<?php echo $result['tpages']; ?>"  />
                    <input type="hidden" name="page" id="page" value="<?php echo $result['page'];?>"  />
               </div>
            </div>
               
               </div>
               </form>
                <?php } ?>
            
            
               <!-- <div id="abcd" style="display:none;"> -->
               <!-- <div class="col-md-3">
                  <div class="form-group pull-right" >
                     File Description :
                  </div>
               </div>
               <div class="col-md-3 ">
                  <div class="form-group">
                     <textarea class="form-control" name="file_desc" id="file_desc"  disabled=""><?php echo $result['description']; ?></textarea>  
                     
                  </div>
               </div> -->
               <!-- <div class="clearfix"></div> -->
  
               </br>

              
            </div>
 

          <table  id="data_table"  class="table table-bordered">
                  <thead>
            
                     <tr>
                        <th><div align="center">S.No.</div></th>
                        <th><div align="center">Date </div></th>
                        <th><div align="center">Action Taken</div></th>
                        <!-- <th><div align="center">Owner Department</div></th> -->
                        <th><div align="center">Old Assignee </div></th>
                        <th><div align="center">New Assignee </div></th>
                        <!-- <th><div align="center">Assigned Date </div></th> -->
                        <th><div align="center">Age </div></th>
                        <!-- <th><div align="center">Priority </div></th> -->
                        <th><div align="center">Remark </div></th>
                        <!-- <th><div align="center">Action</div></th> -->
                     </tr>
                  </thead>
                  <?php
                     if($results) {                     
                        $countno = ($page-1)*$tpages;                     
                        $i=0;                     
                       // foreach($results as $result){ 
                        $sql= mysql_query("SELECT * FROM `activity_log` WHERE `file_no`='".$result['uid']."'");
                        // $sql= mysql_query("SELECT * FROM `track_file` WHERE `file_no`='".$result['uid']."'");
                     while($track = mysql_fetch_array($sql) ){

                        $i++;                     
                        $countno++;
                     
                       // ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                     //Action taken
                       /* $action = $track['activity_type']=='1'?'Created':
                              ($track['activity_type']=='2'?'File Edited':
                                 ($track['activity_type']=='3'?'Assigned':
                                    ($track['activity_type']=='4'?'Dispatched':
                                       ($track['activity_type']=='5'?'Received Back':
                                          ($track['activity_type']=='6'?'Accepted':
                                             ($track['activity_type']=='7'?'Rejected':'Self Assigned'))))));*/
					$action = $track['activity_type']=='1'?NEWFILE:
					($track['activity_type']=='2'?EDITFILE:
					($track['activity_type']=='3'?ASSIGN:
					($track['activity_type']=='4'?DISPATCH:
					($track['activity_type']=='5'?RECEIVE:
					($track['activity_type']=='6'?ACCEPT:
					($track['activity_type']=='7'?REJECT:
					($track['activity_type']=='8'?CALLBACK:
					($track['activity_type']=='9'?MOVESTORAGE:
					($track['activity_type']=='10'?OUTSTORAGE:SELFASSIGN)))))))));
                

             
                     
                     $OldDate = strtotime($track['move_date']);
                     $NewDate = date('M j, Y', $OldDate);
                     $diff = date_diff(date_create($NewDate),date_create(date("M j, Y")));
                     
                     ?>
                  <tr>
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $countno; ?></div>
                     </td>
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo  date("d-m-Y H:i", strtotime($track['date_created']));//substr($track['date_created'],0,16); ?></div>
                     </td>
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $action; ?></div>
                     </td>
                    <!--  <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $track['from_user']!='0'?$this->userDepartment($track['from_user']):"KDA";//$old['name']; ?></div>
                     </td> -->
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $track['from_user']!='0'?$this->userName($track['from_user']):($track['activity_type']=='1'?"--":"KDA"); 
						echo $this->userDepartment($track['from_user'])?" (".$this->userDepartment($track['from_user']).")":'';?></div>
                     </td><!-- 
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $depart['name']; ?></div>
                     </td> -->
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $this->userName($track['to_user']);//$new['name']; 
						echo $this->userDepartment($track['to_user'])?" (".$this->userDepartment($track['to_user']).")":'';
						?></div>
                     </td>
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $track['age']; ?></div>
                     </td>
                     <!-- <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $track['priority']; ?></div>
                     </td> -->
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $track['remark']; ?></div>
                     </td>
                     <!-- <td class="<?php echo $class; ?>">
                        <div align="center">
                           <input type="checkbox" name="vehicle1" value="Bike">
                        </div>
                     </td> -->
                  </tr>
                  <?php }  
                     } else{ ?>
                  <div>
                     <!-- <strong>Data not found.</strong>-->
                  </div>
                  <!-- <tbody><tr class="no-records-found"><td colspan="11">No matching records found</td></tr></tbody>-->
                  <?php } ?>
               </table>
      </div>
   </div>
</div>


<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   
   $('#tender_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'Y/m/d',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });

  function show_button(val){
	  //$('.file_rack_location').prop("disabled", true);
	  $('#file_rack_location').removeAttr("disabled").prop("required", true);
      $('#img_hide').hide();
      $('#rack_btn').show();
  }

  function lotshow_button(val){
	 // $('.file_rack_location').prop("disabled", true);
	  $('#lot_number').removeAttr("disabled").prop("required", true);
      $('#lotimg_hide').hide();
      $('#lot_btn').show();
  }

$(document).ready(function(){
   department_change(<?php echo $result['department_id']; ?>);
});

function department_change(val){
   
   $('#new_file_no').hide();
   $('#old_file_no').hide();
   $('#sceme_name').hide();
   $('#lot_no').hide();
   $('#allotee').hide();
   $('#zone').hide();
   $('#property_no').hide();
   $('#category').hide();
   $('#name').hide();
   $('#file_name').hide();
   $('#contract').hide();
   $('#work_name').hide();
   $('#mb_num').hide();
   $('#address').hide();
   $('#detail').hide();
   $('#pentition_id').hide();
   $('#court_name').hide();
   $('#related_depart').hide();
   $('#advocate_name').hide();
   $('#pensioner_name').hide();
   $('#property_detail').hide();
   $('#year').hide();
   $('#section_name').hide();
   $('#file_type').hide();
   $('#father_name').hide();
   $('#department_name').hide();
   $('#post').hide();
   $('#dob').hide();
   $('#appoint_date').hide();
   $('#retirement_date').hide();
   $('#pensioner_name').hide();
   $('#relation').hide(); 
   // $('#old_file_no').hide(); 
   
   $('#priority').hide(); 
   /*========BHAVAN======*/
   if(val == '3'){
   
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#allotee').show();
   $('#property_no').show();
   $('#sceme_name').show();
    $('#priority').show();
    $('#old_file_no').show();
   }
   /*========PROPERTY======*/
if(val == '4' || val == '20' || val == '21' || val == '22' || val == '23' || val == '24' || val == '25'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#zone').show();
   $('#allotee').show();
   $('#property_no').show();
   $('#sceme_name').show();
   $('#category').show();
    $('#priority').show();
    $('#old_file_no').show();
   }
   /*========GS AND BA======*/
   if(val == '5'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#old_file_no').show();
   $('#file_type').show();
   $('#name').show();
    $('#priority').show();
    // $('#old_file_no').show();
   }
   /*========BHANDAR======*/
   if(val == '6'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#file_name').show();
    $('#priority').show();
    $('#old_file_no').show();
   }
   /*========ENGINEERING======*/
   if(val == '7'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#section_name').show();
   $('#contract').show();
   $('#zone').show();
   $('#mb_num').show();
   $('#priority').show();
   $('#old_file_no').show();
   }
   /*========MALIN BASTI======*/
   if(val == '8'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#category').show();
   $('#name').show();
   $('#address').show();
   $('#detail').show();
   $('#priority').show();
   $('#old_file_no').show();
   }
   /*========LAW======*/
   if(val == '9'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#file_name').show();
   $('#pentition_id').show();
   $('#court_name').show();
   $('#related_depart').show();
   $('#advocate_name').show();
   $('#pensioner_name').show();
   $('#old_file_no').show();
   $('#property_detail').show();
    $('#priority').show();
     // $('#old_file_no').show();
   }
   /*========CARETAKER======*/
   if(val == '10'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#year').show();
   $('#section_name').show();
   $('#contract').show();
   $('#work_name').show();
   $('#mb_num').show();
    $('#priority').show();
     $('#old_file_no').show();
   }
   /*========COMPUTER======*/
   if(val == '11'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#section_name').show();
   $('#contract').show();
   $('#old_file_no').show();
    $('#priority').show();
     // $('#old_file_no').show();
   }
   /*========KARMIK======*/
   if(val == '12'){
   $('#new_file_no').show();
   $('#lot_no').show();
    $('#priority').show();
     $('#old_file_no').show();
   }
   /*========RENT======*/
   if(val == '13'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#allotee').show();
   $('#property_no').show();
   $('#sceme_name').show();
    $('#priority').show();
     $('#old_file_no').show();
   }
   /*========ENFORCMENT======*/
   if(val == '14'){
   $('#new_file_no').show();
   $('#lot_no').show();
    $('#priority').show();
     $('#old_file_no').show();
   }
   /*========PERSONAL======*/
   if(val == '15'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#name').show();
   $('#father_name').show();
   $('#department_name').show();
   $('#post').show();
   $('#dob').show();
   $('#appoint_date').show();
   $('#retirement_date').show();
   $('#pensioner_name').show();
   $('#relation').show();
    $('#priority').show();
     $('#old_file_no').show();
   }
   /*========PENSION======*/
   if(val == '16'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#name').show();
   $('#father_name').show();
   $('#department_name').show();
   $('#post').show();
   $('#dob').show();
   $('#appoint_date').show();
   $('#retirement_date').show();
   $('#pensioner_name').show();
   $('#relation').show();
    $('#priority').show();
     $('#old_file_no').show();
   }
   /*========DS======*/
   if(val == '17'){
   $('#new_file_no').show();
   $('#lot_no').show();
   $('#allotee').show();
   $('#property_no').show();
   $('#sceme_name').show();
    $('#priority').show();
     $('#old_file_no').show();
   }
   
   $('#search').prop('disabled', false);
   if(val == ''){
   $('#search').prop('disabled', true);
   }
   
   }

</script>




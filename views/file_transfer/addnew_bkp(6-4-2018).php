<?php session_start(); ?>
<script type="text/javascript">
   setTimeout("closeMsg('closeid2')",7000);
   function closeMsg(clss) {
         document.getElementById(clss).className = "clspop";
      }
       
</script>
<style type="text/css">
   .clspop {
   display:none;  
   }
   .darkbase_bg {
   display:block !important;
   }
      .dataTables_info{
      display: none;
   }
</style>
<?php if(isset($_SESSION['error'])){
   ?>
<div id="flashMessage" class="message">
   <div  class='darkbase_bg' id='closeid2'>
      <div class='alert_pink' >
         <a class='pop_close'> <img src="images/close.png" onclick="closeMsg('closeid2')" title="close" /> </a>
         <div class='pop_text <?php echo $_SESSION['errorclass']; ?>'>
            <!--warn_red-->
            <p style='color:#063;'><?php echo $_SESSION['error']; ?></p>
         </div>
      </div>
   </div>
</div>
<?php  
   unset($_SESSION['error']);
   unset($_SESSION['errorclass']);
   
   
   }?>
<?php foreach($results as $result) { } //echo $_SESSION['department_id']; ?>
<div class="col-md-12">
   <div class="row">
      <div class="col-lg-12">
         <ol class="breadcrumb">
            <li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
            <!-- <li class="active"><a href="index.php?control=file_transfer">FTS</a></li> -->
            <li class="active"> <?php if($result['id']!='') { ?> Edit <?php } else { ?>Add New<?php } ?>  file </li>
         </ol>
      </div>
   </div>
   <!--/.row-->
</div>
<div class="col-md-12">
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="panel-heading">
            <u>
               <h3><?php if($result['id']!='') { ?> Edit <?php } else { ?>Add New <?php } ?> file</h3>
            </u>
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-primary btn-md" id="btn-chat" style="float:right;">Back</a>
         </div>
         <br>
         <form name="form" autocomplete="off" method="post" enctype="multipart/form-data" onsubmit="return validation();">
            <div class="col-md-10 col-md-offset-1">
               <div class="col-md-3">
                  <div class="form-group">
                     Department :
                  </div>
               </div>
               <div class="form-group col-md-3">
                  <?php                                   
                     $depart = mysql_query("SELECT * FROM `department` WHERE `id` NOT IN (1,2) AND `status`=1 ORDER BY `name` ASC");

                     $department_id = $result['department_id']?$result['department_id']:$_SESSION['department_id'];
                     ?>
                  <select class="form-control department" onchange="department_change(this.value);" name="department" id="department" <?php echo ($_SESSION['department_id']>='3' && $_SESSION['utype']!='Admin' )?'disabled':'';  ?>>
                     <option value="" >Select</option>
                     <?php 
                        while($row = mysql_fetch_array($depart)){ ?>
                     <option value="<?php echo $row['id']; ?>" <?php echo $row['id']==$department_id?'selected':'';  ?> ><?php echo $row['name']; ?></option>
                     <?php } ?>                          
                  </select>
               </div>
           <?php if($_SESSION['department_id'] <='2' ){ ?>
               <div class="col-md-3">
                  <div class="form-group">
                     User :
                  </div>
               </div>
               <div class="form-group col-md-3">
                  <select class="form-control empl_list" name="empl_list" id="empl_list" required="">     
                     <option value="">Select</option>                   
                  </select>
               </div>
            <?php } ?>
            </div>
            <div class="col-md-10 col-md-offset-1">
               <hr>
               <div id="new_file_no" style="display:none;">
                  <?php if($_REQUEST['id']!=''){ ?>
                  <div class="col-md-3">
                     <div class="form-group" >
                        UID NO (New file No.):
                     </div>
                  </div>
                  <div class="col-md-3 ">
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter  UID No" name="uid"  value="<?php echo $result['uid']; ?>" readonly/>    
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
                  <?php } ?>
               </div>
               <div id="old_file_no" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Property ID:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter File No" name="old_no"  value="<?php echo $result['old_no']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="sceme_name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Scheme Name:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="sceme_name"  value="<?php echo $result['sceme_name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="lot_no" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        LOT No.:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="lot_no"  value="<?php echo $result['lot_no']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <!-- ======================================================= -->
               <!-- ======================================================= -->
               <!-- ======================================================= -->
               <!-- ======================================================= -->
               <div id="allotee" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Allotee Name:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="allotee"  value="<?php echo $result['allotee']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="zone" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Zone:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter Zone " name="zone" value="<?php echo $result['zone']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="property_no" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Property No.:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="property_no" value="<?php echo $result['property_no']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="category" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Category:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="category"  value="<?php echo $result['category']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Name:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="name" value="<?php echo $result['name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="file_name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        File Name.:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="file_name" value="<?php echo $result['file_name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="contract" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Contractor Name:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="contract_name" value="<?php echo $result['contract_name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="work_name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Name of Work:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="work_name"  value="<?php echo $result['work_name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="mb_num" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        MB Number:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="mb_num"  value="<?php echo $result['mb_num']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="address" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Address:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="address"  value="<?php echo $result['address']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="detail" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Detail:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="detail"  value="<?php echo $result['detail']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="pentition_id" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Petition ID No.:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="pentition_id"  value="<?php echo $result['pentition_id']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="court_name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Court Name:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="court_name"  value="<?php echo $result['court_name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="related_depart" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Related Department:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="related_depart"  value="<?php echo $result['related_depart']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="advocate_name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Advocate Name:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="advocate_name" value="<?php echo $result['advocate_name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="pensioner_name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Pensioner Name:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="pensioner_name" value="<?php echo $result['pensioner_name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="property_detail" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Property Detail:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="property_detail"  value="<?php echo $result['property_detail']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="year" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Year:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="year" value="<?php echo $result['year']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="section_name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Section Name:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="section_name" value="<?php echo $result['section_name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="file_type" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        File Type:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="file_type"  value="<?php echo $result['file_type']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="father_name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Father's Name:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="father_name"  value="<?php echo $result['father_name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="department_name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Department:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="department_name"  value="<?php echo $result['department_name']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="post" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Post:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="post"  value="<?php echo $result['post']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="dob" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Date of Birth:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="dob"  value="<?php echo $result['dob']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="appoint_date" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Date of Appointment:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="appointment_date"  value="<?php echo $result['appointment_date']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="retirement_date" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Date of Retirement:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="retirement_date" value="<?php echo $result['retirement_date']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <!-- <div class="clearfix"></div> -->
               <div id="pensioner_name" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Family Pensioner Name:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="family_pensioner"  value="<?php echo $result['family_pensioner']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="relation" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Relation:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <input type="text" class="form-control"  name="relation" value="<?php echo $result['relation']; ?>" />   
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
               </div>
               <div id="priority" style="display:none;">
                  <div class="col-md-3">
                     <div class="form-group" >
                        Priority:
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <select class="form-control add_priority" id="add_priority" onchange="get_priority(this.value,<?php echo $i; ?>)" name="add_priority">
                           <option value="">Select</option>
                           <option value="Urgent" <?php echo $result['priority']=='Urgent'?'selected':''; ?>>Urgent</option>
                           <option value="High" <?php echo $result['priority']=='High'?'selected':''; ?>>High</option>
                           <option value="Normal" <?php echo $result['priority']=='Normal'?'selected':''; ?>>Normal</option>
                        </select>
                     </div>
                  </div>
               </div>
               
                <?php if($_SESSION['department_id'] =='2' ){ ?>
               <div id="rack_location" style="display:none;"> 
               <div class="col-md-3">
                  <div class="form-group">
                     File Rack Location :
                  </div>
               </div>
               <div class="form-group col-md-3">
                  <input type="text" class="form-control" name="file_rack_location" id="file_rack_location" >     
                      </div> 
                 </div>
            <?php } ?>
               <!-- <div class="clearfix"></div> -->
               </br>
            </div>
            <!--   <div class="col-md-3 col-md-offset-2">
               <div class="form-group" >
                  Priority:
               </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
               <div class="form-group">
                 <select class="form-control add_priority" id="add_priority" onchange="get_priority(this.value,<?php echo $i; ?>)" name="add_priority">
                           <option value="">Select</option>
                           <option value="Urgent" <?php echo $result['priority']=='Urgent'?'selected':''; ?>>Urgent</option>
                           <option value="High" <?php echo $result['priority']=='High'?'selected':''; ?>>High</option>
                           <option value="Normal" <?php echo $result['priority']=='Normal'?'selected':''; ?>>Normal</option>
                        </select>
               </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3 col-md-offset-2">
               <div class="form-group" >
                  LOT No.:
               </div>
               </div>
               <div class="col-md-4 col-md-offset-right-4">
               <div class="form-group">
                  <input type="text" class="form-control" name="lot_no" id="lot_no" value="<?php echo $result['lot_no']; ?>" />   
                  <span id="msgname" style="color:red;"></span>
               </div>
               </div>
               -->
            </br>
            <div class="col-md-12" align="center">
               <div class="form-group">
                  <button type="submit" id="btn_sub" class="btn btn-primary" disabled><?php if($result['id']) { echo "Update"; } else { echo "Submit";} ?></button>
               </div>
            </div>
      </div>
      <!-- <input type="hidden" name="initiate_by_action" id="initiate_by_action" value="Pending" placeholder="Pending"/>  -->
      <input type="hidden" name="control" value="file_transfer"/>
      <input type="hidden" name="edit" value="1"/>
      <input type="hidden" name="task" value="save"/>
      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
      <input type="hidden" name="tpages" id="tpages" value="<?php echo $result['tpages']; ?>"  />
      <input type="hidden" name="page" id="page" value="<?php echo $result['page'];?>"  />
      </form>
   </div>
</div>
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   $('#tender_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'Y/m/d',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });
   
 /*  $('#department').change(function(){
   var id = $(this).val();
   // $("input[type=text]").prop('required',true);
   $.get( "script/popup_scripts/employee.php?id="+id, function( data ) {
   $("#assigned_to").html(data);
   
   });*/

   
   
   
   
   /*===============New Changes================*/
   $(document).ready(function(){
      department_change(<?php echo $department_id; ?>);
   <?php  if($department_id!=''){ ?>
      $('#btn_sub').prop('disabled', false);
   <?php }else{ ?>
      $('#btn_sub').prop('disabled', true);
   <?php } ?>
   });
   
   
   function department_change(val){
      $('#new_file_no').hide();
      $('#old_file_no').hide();
      $('#sceme_name').hide();
      $('#lot_no').hide();
      $('#allotee').hide();
      $('#zone').hide();
      $('#property_no').hide();
      $('#category').hide();
      $('#name').hide();
      $('#file_name').hide();
      $('#contract').hide();
      $('#work_name').hide();
      $('#mb_num').hide();
      $('#address').hide();
      $('#detail').hide();
      $('#pentition_id').hide();
      $('#court_name').hide();
      $('#related_depart').hide();
      $('#advocate_name').hide();
      $('#pensioner_name').hide();
      $('#property_detail').hide();
      $('#year').hide();
      $('#section_name').hide();
      $('#file_type').hide();
      $('#father_name').hide();
      $('#department_name').hide();
      $('#post').hide();
      $('#dob').hide();
      $('#appoint_date').hide();
      $('#retirement_date').hide();
      $('#relation').hide(); 
      $('#priority').hide(); 
      $('#rack_location').hide(); 
   
   if(val == ''){
      $('#btn_sub').prop('disabled', true);
   }else{
      $('#btn_sub').prop('disabled', false);
   }
   
   /*========BHAVAN======*/
   if(val == '3'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#allotee').show();
      $('#property_no').show();
      $('#sceme_name').show();
      $('#priority').show();
      $('#old_file_no').show();
	    $('#rack_location').show(); 
   }

   /*========PROPERTY======*/
   if(val == '4' || val == '20' || val == '21' || val == '22' || val == '23' || val == '24' || val == '25' ){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#zone').show();
      $('#allotee').show();
      $('#property_no').show();
      $('#sceme_name').show();
      $('#category').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========GS AND BA======*/
   if(val == '5'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#old_file_no').show();
      $('#file_type').show();
      $('#name').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========BHANDAR======*/
   if(val == '6'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#file_name').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========ENGINEERING======*/
   if(val == '7'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#section_name').show();
      $('#contract').show();
      $('#zone').show();
      $('#mb_num').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========MALIN BASTI======*/
   if(val == '8'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#category').show();
      $('#name').show();
      $('#address').show();
      $('#detail').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========LAW======*/
   if(val == '9'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#file_name').show();
      $('#pentition_id').show();
      $('#court_name').show();
      $('#related_depart').show();
      $('#advocate_name').show();
      $('#pensioner_name').show();
      $('#old_file_no').show();
      $('#property_detail').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========CARETAKER======*/
   if(val == '10'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#year').show();
      $('#section_name').show();
      $('#contract').show();
      $('#work_name').show();
      $('#mb_num').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========COMPUTER======*/
   if(val == '11'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#section_name').show();
      $('#contract').show();
      $('#old_file_no').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========KARMIK======*/
   if(val == '12'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========RENT======*/
   if(val == '13'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#allotee').show();
      $('#property_no').show();
      $('#sceme_name').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========ENFORCMENT======*/
   if(val == '14'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========PERSONAL======*/
   if(val == '15'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#name').show();
      $('#father_name').show();
      $('#department_name').show();
      $('#post').show();
      $('#dob').show();
      $('#appoint_date').show();
      $('#retirement_date').show();
      $('#pensioner_name').show();
      $('#relation').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========PENSION======*/
   if(val == '16'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#name').show();
      $('#father_name').show();
      $('#department_name').show();
      $('#post').show();
      $('#dob').show();
      $('#appoint_date').show();
      $('#retirement_date').show();
      $('#pensioner_name').show();
      $('#relation').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }

   /*========DS======*/
   if(val == '17'){
      $('#new_file_no').show();
      $('#lot_no').show();
      $('#allotee').show();
      $('#property_no').show();
      $('#sceme_name').show();
      $('#priority').show();
      $('#old_file_no').show();
	   $('#rack_location').show(); 
   }
   
   }
   
      $('#department').change(function(){
     val = $(this).val();
      $.get( "script/popup_scripts/empl_list.php?id="+val, function( data ) {
      $("#empl_list").html(data);
      // $("#empl_list").trigger("chosen:updated");
// $("#empl_list").trigger('chosen:updated');
// $("#empl_list").chosen();
      });
       // $("#empl_list").chosen();
   }); 
</script>


<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<!--<link rel="stylesheet" href="styles/alertmessage.css" />-->
<script type="text/javascript">
   setTimeout("closeMsg('closeid2')",5000);
   function closeMsg(clss) {
   		document.getElementById(clss).className = "clspop";
   	}
       
</script>
<!--for alert message start-->
<style type="text/css">
   .clspop {
   display:none;	
   }
   .darkbase_bg {
   display:block !important;
   }
</style>
<!--POPUP MESSAGE-->
<?php if(isset($_SESSION['error'])){?>
<div id="flashMessage" class="message">
   <div  class='darkbase_bg' id='closeid2'>
      <div class='alert_pink' >
         <a class='pop_close'> <img src="images/close.png" onclick="closeMsg('closeid2')" title="close" /> </a>
         <div class='pop_text <?php echo $_SESSION['errorclass']; ?>'>
            <!--warn_red-->
            <p style='color:#063;'><?php echo $_SESSION['error']; ?></p>
         </div>
      </div>
   </div>
</div>
<?php  
   unset($_SESSION['error']);
   unset($_SESSION['errorclass']);
   
   }?>
<!--POPUP MESSAGE CLOSE-->
<!-------------------------------Alert Message-------------------------------->    
<div class="col-sm-12">
   <div class="row">
      <div class="col-lg-12">
         <ol class="breadcrumb">
            <li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active"><a href="index.php?control=hrm_department">Masters</a></li>
            <li class="active">Department Master / <?php if($result['id']!='') { ?> Edit <?php } else { ?>Add<?php } ?> Department</li>
         </ol>
      </div>
   </div>
   <!--/.row-->
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-body">
               <div class="panel-heading">
                  <?php foreach($results as $result) { }  ?>
                  <?php if($result['id']!='') { ?> 
                  <u>
                     <h3>Edit Department</h3>
                  </u>
                  <?php } else { ?>
                  <u>
                     <h3>Add New Department</h3>
                  </u>
                  <?php } ?>
               </div>
               <br>
               <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
               	<div class="col-md-8 col-md-offset-2">
                  <div class="col-md-2">
                     <div class="form-group" >
                        Department Name:
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $result['name']; ?>" required/>
                        <!--<div class="form-control" id="txtHint"></div>-->
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-md-2">
                     <div class="form-group" >
                        Key:
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <input type="text" class="form-control" name="key" id="key" value="<?php echo $result['key']; ?>" required/>
                        <!--<div class="form-control" id="txtHint"></div>-->
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-md-6" align="center">
                     <div class="form-group">
                        <button type="submit" class="btn btn-primary"><?php if($result['id']) { echo "Update"; } else { echo "Submit";} ?></button>
                        <!--<button type="submit" class="btn btn-primary">Send</button>-->
                     </div>
                  </div>
                  <input type="hidden" name="control" value="hrm_department"/>
                  <input type="hidden" name="edit" value="1"/>
                  <input type="hidden" name="task" value="save"/>
                  <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                  <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
                  <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
              </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   function isEmail(text)
   {
   
   var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
   
   //"^[\\w-_\.]*[\\w-_\.]\@[\\w]\.\+[\\w]+[\\w]$";
   var regex = new RegExp( pattern );
   return regex.test(text);
   
   }
   
   function numeric(sText)
   {
   var ValidChars = "0123456789,.";
   var IsNumber=true;	
   for (i = 0; i < sText.length && IsNumber == true; i++) 
       { 
       Char = sText.charAt(i); 
       if (ValidChars.indexOf(Char) == -1) 
          {			 
    IsNumber = false;
          }
       }
   	 return IsNumber;
   }
   
</script>
<script type="text/javascript">
   function validation()
   {
   	/* var chk=1;
   if(document.getElementById('email').value == '') { 
   	document.getElementById('msgemail').innerHTML ="*Required field.";
   	chk=0;
   	}else if(!isEmail(document.getElementById('email').value)){ 
   	document.getElementById('msgemail').innerHTML ="*Must be email-id only.";
   	chk=0;	
       }else {
   	document.getElementById('msgemail').innerHTML = "";
   	}
   	
   	if(document.getElementById('name').value == '') { 
   	document.getElementById('msgname').innerHTML ="*Required field.";
   	chk=0;
   	}
   	else {
   	document.getElementById('msgname').innerHTML = "";
   	}
   	
   	if(document.getElementById('subject').value == '') { 
   	document.getElementById('msgsubject').innerHTML ="*Required field.";
   	chk=0;
   	}
   	else {
   	document.getElementById('msgsubject').innerHTML = "";
   	}
   	
   	if(document.getElementById('message').value == '') { 
   	document.getElementById('msgmessage').innerHTML ="*Required field.";
   	chk=0;
   	}
   	else {
   	document.getElementById('msgmessage').innerHTML = "";
   	}
   	
   	if(document.getElementById('mobile').value == '') { 
   	document.getElementById('msgmobile').innerHTML ="*Required field.";
       chk=0;
   	}else if(!numeric(document.getElementById('mobile').value)){ 
   	document.getElementById('msgmobile').innerHTML ="*Must be numeric only.";
   	chk=0;	
       }
   	else {
   	document.getElementById('msgmobile').innerHTML = "";
   	}
   	
   	
   	if(document.getElementById('filename').value == '') {
   	chk = 0;
   	document.getElementById('msgfilename').innerHTML ="This field is required";	
   	}else {
   	var image = document.getElementById('filename').value;
   	var imagefzipLength = image.length;
   	var imagefzipDot = image.lastIndexOf(".");
   	var imagefzipType = image.substring(imagefzipDot,imagefzipLength);
   	if(image) {
   		
   			if((imagefzipType==".jpg")||(imagefzipType==".jpeg")||(imagefzipType==".gif")||(imagefzipType==".png")||(imagefzipType==".doc")||(imagefzipType==".docx")) {
   				   document.getElementById('msgfilename').innerHTML = "";
   				   
   				      var fsize = $('#filename')[0].files[0].size; //get file size
   				if(fsize>102400) 
   				{   alert("Upload Max 100Kb File");
   					document.getElementById('msgfilename').innerHTML = "Please reduce the size of your file using an image editor."; 
   					chk =0;
   				}
   			}
   			else {	chk = 0;
   				 document.getElementById('msgfilename').innerHTML = "Invalid file format only (jpg,gif,png,doc)"; 
   			}
   		 }
   	} */
   	var chk=1;
   <!--Designation NAME Validation -->
   if(document.getElementById("post_name").value == '') {
   chk = 0;
   document.getElementById('post_name').focus();
   document.getElementById('post_name').style.borderColor="red";} 
   else if (!isletter(document.getElementById("post_name").value)) {
   chk = 0;
   document.getElementById('post_name').focus();
   document.getElementById('post_name').style.borderColor="red";}
   else {
   document.getElementById('post_name').style.border="1px solid #ccc";
   }
   
   <!-- Operation Area Validation -->
   if(document.getElementById("area").value == '') {
   chk = 0;
   document.getElementById('area').focus();
   document.getElementById('area').style.borderColor="red";
   }
   else {
   document.getElementById('area').style.border="1px solid #ccc";
   }
   <!-- Management Lavel Validation -->
   if(document.getElementById("level").value == '') {
   chk = 0;
   document.getElementById('level').focus();
   document.getElementById('level').style.borderColor="red";
   }
   else {
   document.getElementById('level').style.border="1px solid #ccc";
   }
   	if(chk) {	
   	        alert("Doctor Registration Successful ..!!");
   		return true;
   		}
   		else {
   		return false;		
   		}	
   }
   
</script>


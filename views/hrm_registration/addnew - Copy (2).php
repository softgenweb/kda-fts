<div class="col-sm-12">
   <div class="row">
      <div class="col-lg-12">
         <ol class="breadcrumb">
            <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active"><a href="index.php?control=hrm_registration&task=show">HRM</a></li>
            <li class="active">Employee Registration / Add New Employee</li>
           
         </ol>
      </div>
   </div>
   <!--/.row-->
   <div class="panel panel-default">
      <div class="panel-body tabs">
         <script src="biomatric/jquery-1.8.2.js"></script>
         <script src="biomatric/mfs100-9.0.2.6.js"></script>
         <form name="employee" autocomplete="off" action="index.php" method="post" enctype="multipart/form-data" onsubmit="return validation();">
            <?php foreach($results as $result) { }  ?>
            <div class="tab-content">
               <div class="panel-heading">
                  <u>
                     <h3>Add New Employee </h3>
                  </u> <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-primary btn-md" id="btn-chat" style="float:right;">Back</a>
               </div>
               <br>
               <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
                  <?php if(!$result['id']){ ?>          
                  <?php 
                     $response = array();
                     
                     		//$sql="SELECT * FROM user ORDER BY id";
                     		 $sql    = "SELECT * FROM users where base64iostemp!='' ORDER BY id";
                     		  $result = mysql_query($sql);
                     		 $rowcount = mysql_num_rows($result);
                     		 
                     		$rowl = 1;
                     
                     		$fingerdate = "";
                     		$fingerdateid = "";
                     		$i = 1;
                     		 
                     		while($row=mysql_fetch_array($result)){
                     
                     			$fingerdate .= $fingerdate != "" ? ",".$row['base64iostemp'] : $row['base64iostemp'];
                     			$fingerdateid .= $fingerdateid != "" ? ",".$row['id'] : $row['id'];
                     
                     			 
                     			if($rowl == 10){
                     				$tmp = array();
                     				$tmp['id']=$fingerdateid;
                     				$tmp['fdata']=$fingerdate;
                     				array_push($response, $tmp);
                     				$fingerdate = "";
                     				$fingerdateid = "";
                     				$rowl=1;
                     			}else if($i == $rowcount){
                     				$tmp = array();
                     				$tmp['id']=$fingerdateid;
                     				$tmp['fdata']=$fingerdate;
                     				array_push($response, $tmp);
                     				$fingerdate = "";
                     				$fingerdateid = "";
                     				$rowl=1;
                     			}else{						
                     				$rowl++;
                     			}
                     			$i++;
                     
                     			
                     		} 
                     		//print_r($idarr);
                     
                     		?>
                  <div class="col-md-4">
                     <div class="form-group" >
                     </div>
                  </div>
                  <div class="col-md-8" >
                     <div class="form-group">
                        <div id="sec_capture_data">
                           <img id="default_img" width="145px" height="188px" alt=""  src="images/thumb.jpg"/>
                           <img id="imgFinger" width="145px" height="188px" alt="" name="imgFinger" />
                           <input type="hidden" name="hd_quality" id="hd_quality">
                           <input type="hidden" name="hd_base64iostemp" id="hd_base64iostemp">
                           <input type="hidden" name="fingre_id" id="fingre_id" value="">
                        </div>
                        <span id="msgfinger"></span>
                        <div class="clearfix"></div>
                        <!--   <a href="javascript:;" class="btn btn-success" onclick="Capture()">Capture Fingre Print</a>-->
                        <a href="javascript:;" class="btn btn-success" onclick="verify()">Capture Fingre Print</a>
                     </div>
                  </div>
                  <?php } ?>
                  <div class="col-md-4">
                     <div class="form-group" >
                        Employee's Name :
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <input class="form-control" type="text" name="name" id="name" value="<?php echo $result['name']; ?>" placeholder="Type your Name as per our record like Rahul Kashyap"   required>
                        <!-- <input class="form-control" type="text" name="name" id="name" value="<?php echo $result['name']; ?>" placeholder="Type your Name as per our record like Rahul Kashyap"  pattern="[a-zA-Z\s]+" required> -->
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group" >
                        Mobile Number :
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <input class="form-control" type="text" name="mobile" id="mobile" value="<?php echo $result['mobile']; ?>"  placeholder="Enter your 10 digit Mobile Number" pattern="[6789][0-9]{9}" maxlength="10" required >
                        <span id="msgmobile" style="color:red;"></span>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group" >
                        Email-Id :
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <input class="form-control" type="email" name="email" id="email" value="<?php echo $result['email']; ?>">
                        <span id="msgemail" style="color:red;"></span>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group" >
                        Department Name :
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <select name="department_id" id="department_id" class="form-control"  onchange="departmentAjax(this.value);" >
                           <option value="">---Select Department---</option>
                           <?php $seldept = mysql_query("select * from department where status='1'");
                              while($res_dept = mysql_fetch_array($seldept))
                              { ?>
                           <option value="<?php echo $res_dept['id'];?>"<?php if($result['department_id']==$res_dept['id']) { echo "selected"; } ?>><?php echo $res_dept['name'];?></option>
                           <?php } ?>
                        </select>
                        <span id="msgdept" class="msg"></span>
                     </div>
                  </div>
                  
                  <div class="col-md-4">
                     <div class="form-group" >
                        Login Access :
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                  <div class="radio">
                  <label><input type="radio" name="login_access" id="login_yes" required=""  value="1" <?php echo $result['status']=='1'?'checked':''; ?>>Yes</label>

                  <label><input type="radio" name="login_access" id="login_no" required="" value="0" <?php echo $result['status']=='0'?'checked':''; ?>> No</label>
                  </div>
                     </div>
                  </div>
                  <div id="show_pass" >
                  <div class="col-md-4">
                     <div class="form-group" >
                        Password :
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <input class="form-control" type="password" name="password" id="password" value="<?php echo $result['password']; ?>" >
                        <span id="msgpassword" style="color:red;"></span>
                     </div>
                  </div>
                  </div>
               </div>
            </div>
            <div class="col-md-12" align="center" style="margin-bottom:15px;">
               <br>
               <button type="submit" class="btn btn-primary" id="submit_btn"><?php if($results[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>
               <!--<a href="javascript:history.go(-1)" class="btn btn-primary btn-md" id="btn-chat">Back</a>
                  <button type="reset" class="btn btn-default">Reset </button>--> 
            </div>
            <input type="hidden" name="control" value="hrm_registration"/>
            <input type="hidden" name="task" value="save"/>
            <input type="hidden" name="edit" id="edit" value="1"  />
            <input type="hidden" name="id" id="idd" value="<?php echo $result['id']; ?>"  />
            <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
            <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
            <input class="form-control" type="hidden" name="utype" id="utype" value="Employee">
         </form>
      </div>
   </div>
   <!--/.panel-->
</div>
<script type="text/javascript">
   /*$(document).ready(function(){
if($('#login_yes').prop('checked')==true){
   $('#show_pass').show();
   $('#password').prop('required',true);
}else if($('#login_no').prop('checked')==true){
   $('#show_pass').hide();
   $('#password').prop('required',false);
}
});*/




   var jqueryarray = <?php echo json_encode($response); ?>;
   
          var quality = 60; //(1 to 100) (recommanded minimum 55)
          var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )
   
          function Capture() {
          	try {
   
          		//$('#sec_get_info').hide();
          		$('#sec_capture_data').show();
   
   
          		$('#txt_quality').val('');
          		$('#txt_base64iostemp').val('');
   
          		$('#hd_quality').val('');
          		$('#hd_base64iostemp').val('');
   
   
   
          		var res = CaptureFinger(quality, timeout);
          		if (res.httpStaus) {
   
                     // document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;
   
                     if (res.data.ErrorCode == "0") {   	
   			    document.getElementById('default_img').style.display = 'none';
                     	document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;
   
                     	$('#hd_imgFinger').val("data:image/bmp;base64," + res.data.BitmapData);
   
                     	var imageinfo = "Quality: " + res.data.Quality + " Nfiq: " + res.data.Nfiq + " W(in): " + res.data.InWidth + " H(in): " + res.data.InHeight + " area(in): " + res.data.InArea + " Resolution: " + res.data.Resolution + " GrayScale: " + res.data.GrayScale + " Bpp: " + res.data.Bpp + " WSQCompressRatio: " + res.data.WSQCompressRatio + " WSQInfo: " + res.data.WSQInfo;
   
   
   
                     	$('#txt_quality').val(imageinfo);
                     	$('#txt_base64iostemp').val(res.data.IsoTemplate);
   
                     	$('#hd_quality').val(imageinfo);
                     	$('#hd_base64iostemp').val(res.data.IsoTemplate);
   
                   	//$('#btn_save_capture_data').click();
   
   
   
                     }
                 }
                 else {
                 	alert(res.err);
                 }
             }
             catch (e) {
             	alert(e);
             }
             return false;
         }
   
   
   
   
   function verify(){
         	try{   
   	
          		$('#sec_capture_data').show();
   			
   				    document.getElementById("submit_btn").disabled = true; 
   
   
         		var res = CaptureFinger(quality, timeout);
   		document.getElementById('default_img').style.display='none';
   			document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;
               	$('#hd_imgFinger').val("data:image/bmp;base64," + res.data.BitmapData);
   			
   			      	var imageinfo = "Quality: " + res.data.Quality + " Nfiq: " + res.data.Nfiq + " W(in): " + res.data.InWidth + " H(in): " + res.data.InHeight + " area(in): " + res.data.InArea + " Resolution: " + res.data.Resolution + " GrayScale: " + res.data.GrayScale + " Bpp: " + res.data.Bpp + " WSQCompressRatio: " + res.data.WSQCompressRatio + " WSQInfo: " + res.data.WSQInfo;
   
   			
   			$('#hd_quality').val(imageinfo);
                  $('#hd_base64iostemp').val(res.data.IsoTemplate);
   			
   		
         		if (res.httpStaus) {		
         			if (res.data.ErrorCode == "0") {				    	
   
   				
         				var probTemplate= res.data.IsoTemplate; 
         				var logs = 0; 
   					$.each(jqueryarray, function () {
   
   						var galleryTemplate= this.fdata;
   						var fingeridcom = this.id;
   
   						var res = VerifyFinger(probTemplate, galleryTemplate); 
   
   
   						if (res.httpStaus) { 
   							if (res.data.Status) { 
   
   								var arrfig = galleryTemplate.split(',');
   								var arrfigid = fingeridcom.split(',');
   									console.log(arrfig);
   								for(var inneri=0; inneri < 10; inneri++ ){
   									var res = VerifyFinger(probTemplate, arrfig[inneri]); 
   
   									if (res.httpStaus) { 
   										if (res.data.Status) {
   										 document.getElementById('hd_quality').value='';
   										 document.getElementById('hd_base64iostemp').value='';
   										 document.getElementById('msgfinger').innerHTML = 'Already Fingre Registered';
   										 document.getElementById('msgfinger').style.color = '#f00';
   										 document.getElementById('fingre_id').value = arrfigid[inneri];
                                              	//alert("Finger matched");
   											//alert("Already Fingre Registered");
   											
   											//alert(arrfigid[inneri]);
   											logs = 1;	
   								//$('#btn_save_capture_data').click(); 
   						
   											return false;
   										}
   									}
   								} 
   								//logs = 1;
   								//return false;
   								
   							
   
   							} 	
   							
   						
   						} else {   
   							alert(res.err); 
   						}
   					});
   					if(logs == 0){
   						//alert("Finger Captured Successfully");  							
   						document.getElementById('msgfinger').innerHTML = 'Finger Captured Successfully';
   						document.getElementById('msgfinger').style.color = '#175200';
   						document.getElementById("submit_btn").disabled = false; 
   						document.getElementById('fingre_id').value = '';
   					}
   				}
   			}
   			else {
   				alert(res.err);
   			} 
   	    
   
   		} catch(e){
   			alert(e);
   		}
   		return false;
   	}
   
   
   function validation() {	
   var chk=1;
   
   	if(document.getElementById('name').value == '') { 
   		document.getElementById('msgname').innerHTML = "*Required field.";
   		chk=0;
   	}
   	else if(!isletter(document.getElementById('name').value)) { 
   		document.getElementById('msgname').innerHTML = "*Enter Valid Name.";
   		chk=0;
   	}
   	else {
   		document.getElementById('msgname').innerHTML = "";
   	}
   	
   	if(document.getElementById("password").value == '') {
   	document.getElementById('msgpassword').innerHTML = "*Required field.";
   	document.getElementById('password').focus();
   	chk = 0;
   	}
   	 else {
   	document.getElementById('msgpassword').innerHTML = "";
   	}
   	
   	if(document.getElementById("permanent_address").value == '') {
   	document.getElementById('msgpermanent_address').innerHTML = "*Required field.";
   	document.getElementById('permanent_address').focus();
   	chk = 0;
   	}
   	 else {
   	document.getElementById('msgpermanent_address').innerHTML = "";
   	}
   	
   	
   	
   	if(document.getElementById("email").value == '') {
   	document.getElementById('msgemail').innerHTML = "*Required field.";
   	document.getElementById('email').focus();
   	chk = 0;
   	}
   	 else {
   	document.getElementById('msgemail').innerHTML = "";
   	}
   
   
   if(chk){
   
     // $('#form').submit();
   
   alert("Employee Registration Successful ..!!");
   
   return true;}
   
   else{
   
   return false;	}	
   
   }
   
   
   
</script>


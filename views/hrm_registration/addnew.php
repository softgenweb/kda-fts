  <!--<link rel="stylesheet" href="styles/alertmessage.css" />-->
<script type="text/javascript">
setTimeout("closeMsg('closeid2')",3000);
function closeMsg(clss) {
		document.getElementById(clss).className = "clspop";
	}
    </script>

  <!--for alert message start-->
  <style type="text/css">
.clspop {
	display:none;	
}
		.darkbase_bg {
			display:block !important;
			
		}
	.dataTables_filter{
		display: none;
	}	
</style>
  <!--POPUP MESSAGE-->
  <?php if(isset($_SESSION['error'])){?>
  <div id="flashMessage" class="message">
    <div  class='darkbase_bg' id='closeid2'>
      <div class='alert_pink' > <a class='pop_close'> <img src="images/close.png" onclick="closeMsg('closeid2')" title="close" /> </a>
        <div class='pop_text <?php echo $_SESSION['errorclass']; ?>'><!--warn_red-->
          <p style='color:#063;'><?php echo $_SESSION['error']; ?></p>
        </div>
      </div>
    </div>
  </div>
  <?php  
  unset($_SESSION['error']);
  unset($_SESSION['errorclass']);

  }?>
  <!--POPUP MESSAGE CLOSE-->
<!-------------------------------Alert Message--------------------------------> 
<div class="col-sm-12">
   <div class="row">
      <div class="col-lg-12">
         <ol class="breadcrumb">
            <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active"><a href="index.php?control=hrm_registration&task=show">HRM</a></li>
            <li class="active">Employee Registration / Add New Employee</li>
           
         </ol>
      </div>
   </div>
   <!--/.row-->
   <div class="panel panel-default">
      <div class="panel-body tabs">
         <script src="biomatric/jquery-1.8.2.js"></script>
         <script src="biomatric/mfs100-9.0.2.6.js"></script>
         <form name="employee" autocomplete="off" action="index.php" method="post" enctype="multipart/form-data" onsubmit="return validation();">
            <?php foreach($results as $result) { }  ?>
            <div class="tab-content">
               <div class="panel-heading">
                  <u>
                     <h3>Add New Employee </h3>
                  </u> <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-primary btn-md" id="btn-chat" style="float:right;">Back</a>
               </div>
               <br>
               
                  
              
                  
               <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
               
               
                <!--   <?php 
				   /* $idemp = $result['id']?" and id!='".$result['id']."'":'';
				  //if(!$result['id']){ 
                     $response = array();
                     
                     		//$sql="SELECT * FROM user ORDER BY id";
                     		 $sql    = "SELECT * FROM users where base64iostemp!='' $idemp ORDER BY id";
                     		  $datas = mysql_query($sql);
                     		 $rowcount = mysql_num_rows($datas);
                     		 
                     		$rowl = 1;
                     
                     		$fingerdate = "";
                     		$fingerdateid = "";
                     		$i = 1;
                     		 
                     		while($row=mysql_fetch_array($datas)){
                     
                     			$fingerdate .= $fingerdate != "" ? ",".$row['base64iostemp'] : $row['base64iostemp'];
                     			$fingerdateid .= $fingerdateid != "" ? ",".$row['id'] : $row['id'];
                     
                     			 
                     			if($rowl == 10){
                     				$tmp = array();
                     				$tmp['id']=$fingerdateid;
                     				$tmp['fdata']=$fingerdate;
                     				array_push($response, $tmp);
                     				$fingerdate = "";
                     				$fingerdateid = "";
                     				$rowl=1;
                     			}else if($i == $rowcount){
                     				$tmp = array();
                     				$tmp['id']=$fingerdateid;
                     				$tmp['fdata']=$fingerdate;
                     				array_push($response, $tmp);
                     				$fingerdate = "";
                     				$fingerdateid = "";
                     				$rowl=1;
                     			}else{						
                     				$rowl++;
                     			}
                     			$i++;
                     
                     			
                     		} 
                     		//print_r($idarr);
                     */
                     		?>
                  <div class="col-md-4">
                     <div class="form-group" >
                     </div>
                  </div>
                  <div class="col-md-8" >
                     <div class="form-group">
                        <div id="sec_capture_data">
                           <img id="default_img" width="145px" height="188px" alt=""  src="images/thumb.jpg"/>
                           <img id="imgFinger" width="145px" height="188px" alt="" name="imgFinger" />
                           <input type="hidden" name="hd_quality" id="hd_quality">
                           <input type="hidden" name="hd_base64iostemp" id="hd_base64iostemp">
                           <input type="hidden" name="fingre_id" id="fingre_id" value="">
                        </div>
                        <span id="msgfinger"></span>
                        <div class="clearfix"></div>
                        <a href="javascript:;" class="btn btn-success" onclick="Capture()">Capture Fingre Print</a>-
                        <a href="javascript:;" class="btn btn-success" onclick="verify()">Capture Fingre Print</a>
                     </div>
                  </div>
                  <?php //} ?> -->
                    <div class="clearfix"></div>
                   <h4>Employee Detail:</h4>
                 <hr>
                  <div class="col-md-4">
                     <div class="form-group" >
                        Employee Id <span style="color: #f00">*</span>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <input class="form-control" type="text" name="username" id="username" value="<?php echo $result['username']; ?>" placeholder="Enter Employee Id"   required>
                        <!-- <input class="form-control" type="text" name="name" id="name" value="<?php echo $result['name']; ?>" placeholder="Type your Name as per our record like Rahul Kashyap"  pattern="[a-zA-Z\s]+" required> -->
                        <span id="erruserid"></span>

                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group" >
                        Employee's Name <span style="color: #f00">*</span>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <input class="form-control" type="text" name="name" id="name" value="<?php echo $result['name']; ?>" placeholder="Type your Name as per our record like Rahul Kashyap"   required>
                        <!-- <input class="form-control" type="text" name="name" id="name" value="<?php echo $result['name']; ?>" placeholder="Type your Name as per our record like Rahul Kashyap"  pattern="[a-zA-Z\s]+" required> -->
                        <span id="msgname" style="color:red;"></span>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group" >
                        Mobile Number <span style="color: #f00">*</span>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <input class="form-control" type="text" name="mobile" id="mobile" value="<?php echo $result['mobile']; ?>"  placeholder="Enter your 10 digit Mobile Number" pattern="[6789][0-9]{9}" maxlength="10" required >
                           <span id="errmobile"></span>
                        <span id="msgmobile" style="color:red;"></span>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group" >
                        Email-Id 
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <input class="form-control" type="email" name="email" id="email" value="<?php echo $result['email']; ?>">
                        <span id="msgemail" style="color:red;"></span>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group" >
                        Department Name <span style="color: #f00">*</span>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group"> 
					 <?php if(!$result['id']){ ?>
                        <select name="department_id" id="department_id" class="form-control"  onchange="departmentAjax(this.value);" required>
                           <option value="">---Select Department---</option>
                           <?php $seldept = mysql_query("select * from department where status='1'");
                              while($res_dept = mysql_fetch_array($seldept))
                              { ?>
                           <option value="<?php echo $res_dept['id'];?>"<?php if($result['department_id']==$res_dept['id']) { echo "selected"; } ?>><?php echo $res_dept['name'];?></option>
                           <?php } ?>
                        </select>
                    <?php } else{ echo $this->departmentName($result['department_id']);?>
                    <input type="hidden" name="department_id" id="department_id" class="form-control" value="<?php echo $result['department_id']; ?>">
               <a href="index.php?control=hrm_registration&task=deptartment_changes&id=<?php echo $result['id']; ?>" class="btn btn-primary btn-md" style="cursor:pointer; float:right" onclick="return confirm('Are you sure you want to Change Department ?')">Change Department</a>
              <?php } ?>
               
                        
                        <span id="msgdept" class="msg"></span>
                     </div>
                  </div>
                  
                  <?php //if($result['base64iostemp']!=''){ ?>
                      <div class="col-md-4" id="finger_print_id">
                     <div class="form-group" >
                        Finger Print 
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group"><b id="thumbvalue"> <?php echo ($result['base64iostemp']!='')?'Available':'N/A'; ?></b><button type="button" onclick="finger_print_update();" class="btn btn-primary btn-md" id="btn-chat" style="float:right;">Finger Print Update</button>
                  <!--   <input type="hidden" name="finger_print" id="finger_print" value="<?php echo $result['finger_print']; ?>">-->
                <!--  <select class="form-control" name="finger_print" id="finger_print">
                  <option value="" >Select</option>
                  <option value="1" <?php if($result['finger_print']=='1'){ echo 'selected';} ?>> Available</option>
                  <option value="0" <?php if($result['finger_print']=='0'){ echo 'selected';} ?>> N/A</option>
                  </select>-->
                     </div>
                  </div>
                  
                  
               <?php //} ?>
                  
                 <div class="clearfix"></div> 
                 <h4>Login Detail:</h4>
                 <hr>
                  <div class="col-md-4">
                     <div class="form-group" >
                        Allow Login <span style="color: #f00">*</span>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                  <div class="radio">
                  <label><input type="radio" name="login_access" class="login_access" id="login_yes" required=""  value="1" <?php echo $result['login']=='1'?'checked':''; ?>>Yes</label>

                  <label><input type="radio" name="login_access" class="login_access" id="login_no" required="" value="0" <?php echo $result['login']=='0'?'checked':''; ?>> No</label>
                  </div>
                     </div>
                  </div>
                  
                  
                  <div id="show_pass" style="display:<?php if($result['login']=='1'){echo 'block';}else{echo 'none';}?>" >
                  <div class="col-md-4">
                     <div class="form-group" >
                        Password <span style="color: #f00">*</span>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <input class="form-control" type="password" name="password" id="password" value="<?php echo $result['password'];?>">
                        <span id="msgpassword" style="color:red;"></span>
                     </div>
                  </div>
                  
                  
                  
                  
                  <div class="col-md-4">
                     <div class="form-group" >
                        Allow Login WIthout Finger Print <span style="color: #f00">*</span>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
  <div class="radio">
    <label><input type="radio" name="finger_print" class="login_access" id="finger_print"  value="0" <?php echo $result['finger_print']=='0'?'checked':''; ?>> Yes</label>

                  <label><input type="radio" name="finger_print" class="login_access" id="finger_print"  value="1" <?php echo $result['finger_print']=='1'?'checked':''; ?>>No</label>

                  
                  </div>


                  <!--<div class="radio">
                  <select class="form-control" name="finger_print" id="finger_print">
                  <option value="" >Select</option>
                  <option value="1" <?php if($result['finger_print']=='1'){ echo 'selected';} ?>> No</option>
                  <option value="0" <?php if($result['finger_print']=='0'){ echo 'selected';} ?>>Yes</option>
                  </select>
                  </div>-->
                     </div>
                  </div>
                  </div>
                  
               </div>
            </div>
            <div class="col-md-12" align="center" style="margin-bottom:15px;">
               <br>
               <button type="submit" class="btn btn-primary" id="submit_btn"><?php if($results[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>
               <!--<a href="javascript:history.go(-1)" class="btn btn-primary btn-md" id="btn-chat">Back</a>
                  <button type="reset" class="btn btn-default">Reset </button>--> 
            </div>
            <input type="hidden" name="control" value="hrm_registration"/>
            <input type="hidden" name="task" value="save"/>
            <input type="hidden" name="edit" id="edit" value="1"  />
            <input type="hidden" name="id" id="idd" value="<?php echo $result['id']; ?>"  />
            <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
            <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
            <input class="form-control" type="hidden" name="utype" id="utype" value="Employee">
<!-- ================================= -->
<!-- ================================= -->
<!-- ================================= -->
<!-- ================================= -->
<!-- ================================= -->
<!-- ================================= -->
<!-- ================================= -->
<!-- Trigger the modal with a button -->
<button type="button" data-toggle="modal" id="done" data-target="#myModal" style="display: none;"></button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document" style="margin-top: 125px;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="opacity: 1;"><span class="font-awesome" aria-hidden="true"><i class="fa fa-times-circle fa-2x" aria-hidden="true"></i></span></button>
            <h4 class="modal-title" id="myModalLabel">
               <center>Place Your Finger on Device</center>
            </h4>
         </div>
         <div class="modal-body" style="text-align:center; height: auto;">
            <?php 
               $idemp = $result['id']?" and id!='".$result['id']."'":'';
               //if(!$result['id']){ 
                    $response = array();
                    
                          //$sql="SELECT * FROM user ORDER BY id";
                           $sql    = "SELECT * FROM users where base64iostemp!='' $idemp ORDER BY id";
                            $datas = mysql_query($sql);
                           $rowcount = mysql_num_rows($datas);
                           
                          $rowl = 1;
                    
                          $fingerdate = "";
                          $fingerdateid = "";
                          $i = 1;
                           
                          while($row=mysql_fetch_array($datas)){
                    
                             $fingerdate .= $fingerdate != "" ? ",".$row['base64iostemp'] : $row['base64iostemp'];
                             $fingerdateid .= $fingerdateid != "" ? ",".$row['id'] : $row['id'];
                    
                              
                             if($rowl == 10){
                                $tmp = array();
                                $tmp['id']=$fingerdateid;
                                $tmp['fdata']=$fingerdate;
                                array_push($response, $tmp);
                                $fingerdate = "";
                                $fingerdateid = "";
                                $rowl=1;
                             }else if($i == $rowcount){
                                $tmp = array();
                                $tmp['id']=$fingerdateid;
                                $tmp['fdata']=$fingerdate;
                                array_push($response, $tmp);
                                $fingerdate = "";
                                $fingerdateid = "";
                                $rowl=1;
                             }else{                  
                                $rowl++;
                             }
                             $i++;
                    
                             
                          } 
                          //print_r($idarr);
                    
                          ?>
            <!-- <div class="col-md-4">
               <div class="form-group" >
               </div>
               </div> -->
            <!-- <div class="col-md-12" > -->
               <!-- <div class="form-group"> -->
                  <div id="sec_capture_data">
                     <img id="default_img" width="145px" height="188px" alt="&nbsp;"  src="images/thumb.gif" style="display:none"/>
                     <img id="imgFinger" width="145px" height="188px" alt="&nbsp;" name="imgFinger"  style="display:none" />
                     <input type="hidden" name="hd_quality" id="hd_quality">
                     <input type="hidden" name="hd_base64iostemp" id="hd_base64iostemp">
                     <input type="hidden" name="fingre_id" id="fingre_id" value="">
                  </div>
                  <span id="msgfinger"></span>
                  <div class="clearfix"></div>
                  <!--   <a href="javascript:;" class="btn btn-success" onclick="Capture()">Capture Fingre Print</a>-->
                  <a href="javascript:;" class="btn btn-primary" onclick="login_thumb()" id="loginButton">Capture Fingre Print</a>
               <!-- </div> -->
            <!-- </div> -->
            <?php //} ?>
         </div>
         <!-- <div class="modal-footer" style="border-top: 0px;"> -->
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
         <!-- </div> -->
      </div>
   </div>
</div>


<!-- ================================= -->
<!-- ================================= -->
<!-- ================================= -->
<!-- ================================= -->
<!-- ================================= -->
<!-- ================================= -->
<!-- ================================= -->
         </form>
      </div>
   </div>
   <!--/.panel-->
</div>

<script type="text/javascript">
   /*$(document).ready(function(){
if($('#login_yes').prop('checked')==true){
   $('#show_pass').show();
   $('#password').prop('required',true);
}else if($('#login_no').prop('checked')==true){
   $('#show_pass').hide();
   $('#password').prop('required',false);
}
});*/


/*========================*/
 $('input[name="login_access"]').change(function() { 

val =$(this).val();

if(val == '1') {
        
   $('#show_pass').show();
   //$('#password').prop('required',true);
   $("#password").attr('required', ''); 
   $("#finger_print").attr('required', ''); 

   }
else if((val == '0') || (val=='')) {
        
   $('#show_pass').hide();
   $('#password').prop('required',false).val('<?php echo $result['password'];?>');
   $("#finger_print").attr('required', false); 

   }
});

/*$('#finger_print').change(function() {
// $('input[name="login_access"]').change(function() {
   if($(this).val() == '1') {
         // $('#myModal').modal('show');
         $('#done').click();

   }
});*/

function finger_print_update(){
	/* var hd_base64iostemp = $('#hd_base64iostemp').val();
	  if(hd_base64iostemp){
		  $('#imgFinger').hide(); 
		  $('#msgfinger').hide(); 
	  }*/
	
	//alert("Hello");
	 $('#done').click();
	 
}



  function login_thumb(){
	           
			   $('#msgfinger').hide();
			   $('#default_img').show();
			    $('#imgFinger').hide();
			 
			   setTimeout(verify, 1000);
			   
		   }

   var jqueryarray = <?php echo json_encode($response); ?>;
   
          var quality = 60; //(1 to 100) (recommanded minimum 55)
          var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )
   
          function Capture() {
          	try {
   
          		//$('#sec_get_info').hide();
          		$('#sec_capture_data').show();
   
   
          		$('#txt_quality').val('');
          		$('#txt_base64iostemp').val('');
   
          		$('#hd_quality').val('');
          		$('#hd_base64iostemp').val('');
   
   
   
          		var res = CaptureFinger(quality, timeout);
          		if (res.httpStaus) {
   
                     // document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;
   
                     if (res.data.ErrorCode == "0") { 

   			              document.getElementById('default_img').style.display = 'none';
                     	document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;
   
                     	$('#hd_imgFinger').val("data:image/bmp;base64," + res.data.BitmapData);
   
                     	var imageinfo = "Quality: " + res.data.Quality + " Nfiq: " + res.data.Nfiq + " W(in): " + res.data.InWidth + " H(in): " + res.data.InHeight + " area(in): " + res.data.InArea + " Resolution: " + res.data.Resolution + " GrayScale: " + res.data.GrayScale + " Bpp: " + res.data.Bpp + " WSQCompressRatio: " + res.data.WSQCompressRatio + " WSQInfo: " + res.data.WSQInfo;
   
   
   
                     	$('#txt_quality').val(imageinfo);
                     	$('#txt_base64iostemp').val(res.data.IsoTemplate);
   
                     	$('#hd_quality').val(imageinfo);
                     	$('#hd_base64iostemp').val(res.data.IsoTemplate);
   
                   	//$('#btn_save_capture_data').click();
   
   
   
                     }
                 }
                 else {
                 	// alert(res.err);
                 }
             }
             catch (e) {
             	alert(e);
             }
             return false;
         }
   
   
   
   
   function verify(){
         	try{   
   	
          		$('#sec_capture_data').show();
   			
   				    document.getElementById("submit_btn").disabled = true; 
   
   
         		/*var res = CaptureFinger(quality, timeout);
   		document.getElementById('default_img').style.display='none';
   			document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;
               	$('#hd_imgFinger').val("data:image/bmp;base64," + res.data.BitmapData);
   			                     */
                                 var res = CaptureFinger(quality, timeout);
                                 if(res.data.BitmapData == undefined || res.data.BitmapData == null){
									 document.getElementById('default_img').style.display='none';
                                   alert('No finger print found, Please Try Again !!!');
                                 }else{
                    
                                    document.getElementById('default_img').style.display='none';
  $('#imgFinger').show();

                                    // document.getElementById('imgFinger').style.display = 'block';
                                    document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;
                                    $('#hd_imgFinger').val("data:image/bmp;base64," + res.data.BitmapData); 
                                 }

   			      	var imageinfo = "Quality: " + res.data.Quality + " Nfiq: " + res.data.Nfiq + " W(in): " + res.data.InWidth + " H(in): " + res.data.InHeight + " area(in): " + res.data.InArea + " Resolution: " + res.data.Resolution + " GrayScale: " + res.data.GrayScale + " Bpp: " + res.data.Bpp + " WSQCompressRatio: " + res.data.WSQCompressRatio + " WSQInfo: " + res.data.WSQInfo;
   
   			
   			$('#hd_quality').val(imageinfo);
                  $('#hd_base64iostemp').val(res.data.IsoTemplate);
   			
   		
         		if (res.httpStaus) {		
         			if (res.data.ErrorCode == "0") {				    	
   
   				
         				var probTemplate= res.data.IsoTemplate; 
         				var logs = 0; 
   					$.each(jqueryarray, function () {
   
   						var galleryTemplate= this.fdata;
   						var fingeridcom = this.id;
   
   						var res = VerifyFinger(probTemplate, galleryTemplate); 
   
   
   						if (res.httpStaus) { 
   							if (res.data.Status) { 
   
   								var arrfig = galleryTemplate.split(',');
   								var arrfigid = fingeridcom.split(',');
   									console.log(arrfig);
   								for(var inneri=0; inneri < 10; inneri++ ){
   									var res = VerifyFinger(probTemplate, arrfig[inneri]); 
   
   									if (res.httpStaus) { 
   										if (res.data.Status) {
											//  $('#imgFinger').show();
										// document.getElementById('finger_print').value = '1';
										  $('#imgFinger').hide();
										  $('#msgfinger').show();
   										 document.getElementById('hd_quality').value='';
   										 document.getElementById('hd_base64iostemp').value='';
   										 document.getElementById('msgfinger').innerHTML = 'This Finger is Already Registered With Other Username';
   										 document.getElementById('loginButton').innerHTML = 'Try Again';//'Already Finger Registered';
   										 document.getElementById('msgfinger').style.color = '#f00';
   										 document.getElementById('fingre_id').value = arrfigid[inneri];
										 document.getElementById('thumbvalue').innerHTML = 'N/A';
                                              	//alert("Finger matched");
   											//alert("Already Fingre Registered");
   											
   											//alert(arrfigid[inneri]);
   											logs = 1;	
   								//$('#btn_save_capture_data').click(); 
   						
   											return false;
   										}
   									}
   								} 
   								//logs = 1;
   								//return false;
   								
   							
   
   							} 	
   							
   						
   						} else {   
   							alert(res.err); 
   						}
   					});
   					if(logs == 0){
   						//alert("Finger Captured Successfully");  
						// document.getElementById('loginButton').innerHTML = 'Capture Fingre Print';	
						  $('#msgfinger').show();
						 document.getElementById('loginButton').style.display = 'none';							
   						document.getElementById('msgfinger').innerHTML = 'Finger Captured Successfully';
   						document.getElementById('msgfinger').style.color = '#175200';
   						document.getElementById("submit_btn").disabled = false; 
   						document.getElementById('fingre_id').value = '';
   						document.getElementById('thumbvalue').innerHTML = 'Available';

                setTimeout(function hide(){
						document.getElementById('myModal').style.display = 'none';
            $( "#myModal" ).removeClass( "in" ).attr("aria-hidden","true");
            // $("button").attr("aria-expanded","true");
            $('.modal-backdrop').css('position','unset').remove();
						$('.modal-backdrop.fade.in').remove();
           
            $( "body" ).removeClass( "modal-open" ).removeAttr("style");},3000);
						
						//timeIntval();
						/*setInterval(function() {
						document.getElementById('myModal').style.display = 'none';
						$('.modal-backdrop').css('position','unset');
						}, 3000);*/
   					}
   				}
   			}
   			else {
   				alert(res.err);
   			} 
   	    
   
   		} catch(e){
   			alert(e);
   		}
   		return false;
   	}
	
	
/*	function timeIntval() {
		setInterval(function() {
						document.getElementById('myModal').style.display = 'none';
						$('.modal-backdrop').css('position','unset');
						}, 3000);
}*/
   
   
   
   function validation() {	
   var chk=1;
   
   	if(document.getElementById('name').value == '') { 
   		document.getElementById('msgname').innerHTML = "*Required field.";
   		chk=0;
   	}
   	else if(!isletter(document.getElementById('name').value)) { 
   		document.getElementById('msgname').innerHTML = "*Enter Valid Name.";
   		chk=0;
   	}
   	else {
   		document.getElementById('msgname').innerHTML = "";
   	}
   	
   	if(document.getElementById("password").value == '') {
   	document.getElementById('msgpassword').innerHTML = "*Required field.";
   	document.getElementById('password').focus();
   	chk = 0;
   	}
   	 else {
   	document.getElementById('msgpassword').innerHTML = "";
   	}
   	
   	if(document.getElementById("permanent_address").value == '') {
   	document.getElementById('msgpermanent_address').innerHTML = "*Required field.";
   	document.getElementById('permanent_address').focus();
   	chk = 0;
   	}
   	 else {
   	document.getElementById('msgpermanent_address').innerHTML = "";
   	}
   	
   	
   	
   	if(document.getElementById("email").value == '') {
   	document.getElementById('msgemail').innerHTML = "*Required field.";
   	document.getElementById('email').focus();
   	chk = 0;
   	}
   	 else {
   	document.getElementById('msgemail').innerHTML = "";
   	}
   
   
   if(chk){
   
     // $('#form').submit();
   
   alert("Employee Registration Successful ..!!");
   
   return true;}
   
   else{
   
   return false;	}	
   
   }
   

   

$ = jQuery.noConflict();
$(document).ready(function($) { 
$("#username").on('change',function (e) { 

  //removes spaces from username
  $(this).val($(this).val().replace(/\s/g, ''));
  
  var user_id = $(this).val();
  if(user_id.length < 2){$("#erruserid").html('');return;}
  
  if(user_id.length >= 2){
    $("#erruserid").html('<img src="images/ajax-loader.gif" />');
    $.post('script/popup_scripts/check_userid.php', {'username':user_id}, function(data) {
      $("#erruserid").html(data);

if (data.match(/Diffrent.*/)) {
$('#submit_btn').prop('disabled',true);
}else{
  $('#submit_btn').prop('disabled',false);
}
    });
  }
});    
});


$(document).ready(function($) { 
$("#mobile").on('change',function (e) {

  //removes spaces from username
  $(this).val($(this).val().replace(/\s/g, ''));
  
  var mobile = $(this).val();
  var id = document.getElementById("idd").value; 
  if(mobile.length == 9){$("#errmobile").html('');return;}
   //alert(mobile);
  if(mobile.length == 10){
    $("#errmobile").html('<img src="images/ajax-loader.gif" />');
      $.post('script/popup_scripts/check_mobile.php', {'mobile':mobile,'id':id}, function(data) {
		 // alert(data);
      $("#errmobile").html(data);

if (data.match(/Diffrent.*/)) {
$('#submit_btn').prop('disabled',true);
}else{
  $('#submit_btn').prop('disabled',false);
}
    });
  }
});    
});

</script>

<style>
/*===================Department id-1 CSS Start==============*/
.round-rect{
	text-align:center;
}

.round-rect .table.table-striped thead th {
  height: 20px;
  padding-bottom: 5px!important;
  padding-top: 5px !important;
  padding-left:10px;
  padding-right:10px;
}
.round-rect .table > thead > tr > th {
  border-bottom: 1px solid #b8cde8 !important;
  vertical-align: bottom;
}

.table.table-striped.LGM-table {
  margin: 0 auto;
  width: 97%;
}

/*===================Department id-1 CSS End==============*/

/*===================Department id-2 CSS Start==============*/
.table.table-striped.file-table {
  margin: 0 auto;
  width: 90%;
}
.table.table-striped.lenders-table {
  margin: 0 auto;
  width: 90%;
}
.events li {
  color: #f00;
}
.col-md-4.flash_news .round-rect p {
  padding:0px;
}
.col-md-4.flash_news .round-rect {
  background: rgba(0, 0, 0, 0) radial-gradient(ellipse at center center , rgba(245, 245, 245, 1) 0%, rgba(205, 206, 207, 1) 23%, rgba(94, 96, 100, 1) 88%, rgba(76, 78, 82, 1) 99%) repeat scroll 0 0;
  height: 160px;
}
.flash_news {
  background: rgba(0, 0, 0, 0) radial-gradient(ellipse at center center , rgba(245, 245, 245, 1) 0%, rgba(205, 206, 207, 1) 23%, rgba(94, 96, 100, 1) 88%, rgba(76, 78, 82, 1) 99%) repeat scroll 0 0;
  border: 2px solid #597eb2;
  border-radius: 26px;
}
.flash_news p {
    color: #000;
    font-size: 22px;
    padding: 22px;
    position: relative;
    font-weight: 600;

}
.add_lead {
  background: #c8dafe none repeat scroll 0 0;
  border: 2px solid #597eb2;
  border-radius: 5px;
  box-shadow: 0 7px 8px #888888;
  color: #000;
  font-weight: bold;
  margin: 0 auto;
  padding: 4px 15px;
  text-align: center;
}
.active-alert {
  color: #f00;
  font-size: 19px;
  font-weight: bold;
  padding-top: 11px;
}
/*===================Department id-2 CSS End==============*/
</style>

<div class="col-md-10 col-md-offset-1">
			<div class="row">
           

			<!--<div class="col-lg-12">
				<h3 class="page-header">Welcome : <?php echo $_SESSION['admin_name']; ?></h3>
			</div>-->
		</div>
        <br>
		<!---------------------------------Admin Dashboard Start-------------------->
		<?php $year = date('Y'); $year1 = date('y',strtotime("$year +1 year") );
		 if($_SESSION['department_id']=="1" && $_SESSION['level']=="1"){ ?>
		<div class="row">
        <div class="container2">
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
            	<div class="round-rect one">
                <h1 align="center">Achievement <?php echo $year.'-'.$year1;?></h1>
                   <div class="col-md-4 col-sm-4 speed_meter">
            			<div id="jqRadialGauge" style="width: 200px; height: 200px;"></div>
        			</div>
                   <div class="col-md-4 col-sm-4 speed_meter">
        				<div id="jqRadialGauge1" style="width: 200px; height: 200px;"></div>
        			</div> 
                    <div class="col-md-4 col-sm-4 speed_meter">
                    	<div id="jqRadialGauge2" style="width: 200px; height: 200px;"></div>
                    </div>
                 </div>
            </div>
        </div>
        <br />
       <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
            	<div class="round-rect">
                	<h4 class="retail">Retail <?php echo $year.'-'.$year1; 
					$retail = mysql_query("select * from hrm_registration where department_id = '3' and status = '1'");
					
					 ?></h4>
                    
                    <table class="table table-striped retail-table">
                        <thead>
                        <tr>
                        <th>Name</th>
                        <th>Achieved</th>
                        <th>Target</th>
                        </tr>
                        </thead>
                         <?php while($retail_person = mysql_fetch_array($retail))  { ?>
                        <tbody>
                        <tr>
                        <td style="text-align:left"><?php echo $retail_person['name']; ?></td>
                        <td>2</td>
                        <td>3</td>
                        </tr>
                        <tr>
                       <?php } ?>
                        </tbody>
                        </table>
                        <br />
                        <br />
                    <h4>Outbound <?php  $year = date('Y'); $year1 = date('y',strtotime("$year +1 year") ); echo $year.'-'.$year1; $outbound = mysql_query("select * from hrm_registration where department_id = '4' and status = '1'");
					?></h4>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                        <th>Name</th>
                        <th>Achieved</th>
                        <th>Target</th>
                        </tr>
                        </thead>
                         <?php while($outbound_person = mysql_fetch_array($outbound))  { ?>
                        <tbody>
                        <tr>
                        <td style="text-align:left"><?php echo $outbound_person['name']; ?></td>
                        <td>2</td>
                        <td>3</td>
                        </tr>
                          <?php } ?>
                        </tbody>
                        </table>
                        <br />
                </div>
            
            </div>
          <div class="col-md-8 col-sm-8 col-xs-12 lgm_sum">
            	<div class="round-rect"> 
                	<h1 align="center">LGM Summary <?php echo $year.'-'.$year1; ?></h1>
                    <div style="overflow-x:auto;">
        <table class="table table-striped LGM-table">
        <thead>
        <tr>
        <th>Leads</th>
        <th>W.I.P</th>
        <th>Hold</th>
        <th>Login</th>
        <th>Reject</th>
        <th>Approve</th>
        <th>Spill-Over</th>
        <th>Sanction</th>
        <th>Disbursement</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td><a href="index.php?control=lgm_report&task=new_report" style="text-decoration:underline; color:#30a5ff !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='0'")); ?></a></td>
        
        <td> <a href="index.php?control=lgm_report&task=wip_report" style="text-decoration:underline; color:#33F !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='1'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=hold_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='2'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=verify_report" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='3'")); ?></a></td>
        
       <td><a href="index.php?control=lgm_report&task=rejected_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='4'")); ?></a></td>
       
        <td><a href="index.php?control=lgm_report&task=lender_verify_lgm" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from lender_loan_assign where lander_status='1'")); ?></a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">6</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">7</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">8</a></td>
        </tr>
        </tbody>
        </table>
        </div>
        <br />

                </div>
                <br />
                <div class="col-md-8 graph">
                	<div class="round-rect" style="border:none;">
                    	<div id="jqChart"></div>
                    </div>
                </div>
                <div class="col-md-4 flash_news" style="height:250px;">
                <p align="center" style="top:60px;">Performer News flash</p>
                	<!--<div class="round-rect">
                    
                    </div>-->
                </div>
            </div>
        </div>

		</div>
        </div>
        <?php } ?>
     	<!---------------------------------Admin Dashboard End-------------------->  
      
       	<!---------------------------------Operational Dashboard Start-------------------->  
      <?php  if($_SESSION['department_id']=="2" && $_SESSION['level']=="2")  { ?>
			<div class="row">
			<div class="container2">
            	<div class="row">
                    	<div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="round-rect">
                    <p align="center" class="active-alert"><sup>**</sup> <u>You have active alerts</u></p>
                    </div>
                    <br />
            				<div class="round-rect">
                				<h1 align="center">File Summary</h1>
                                <div style="overflow-x:auto">
                                
                                    <table class="table table-striped file-table">
                                    <thead>
                                    <tr>
                                    <th>Leads</th>
                                    <th>W.I.P</th>
                                    <th>Hold</th>
                                    <th>Login</th>
                                    <th>Reject</th>
                                    <th>Approve</th>
                                    <th>Spill-Over</th>
                                    <th>Sanction</th>
                                    <th>Disbursement</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                    <td><a href="index.php?control=lgm_report&task=new_report" style="text-decoration:underline; color:#30a5ff !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='0'")); ?></a></td>
                                    
                                    <td> <a href="index.php?control=lgm_report&task=wip_report" style="text-decoration:underline; color:#33F !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='1'")); ?></a></td>
                                    
                                    <td><a href="index.php?control=lgm_report&task=hold_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='2'")); ?></a></td>
                                    
                                    <td><a href="index.php?control=lgm_report&task=verify_report" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='3'")); ?></a></td>
                                    
                                    <td><a href="index.php?control=lgm_report&task=rejected_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='4'")); ?></a></td>
                                    
                                    <td><a href="index.php?control=lgm_report&task=lender_verify_lgm" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from lender_loan_assign where lander_status='1'")); ?></a></td>
                                    <td><a href="#" style="text-decoration:underline; color:#C00 !important">6</a></td>
                                    <td><a href="#" style="text-decoration:underline; color:#C00 !important">7</a></td>
                                    <td><a href="#" style="text-decoration:underline; color:#C00 !important">8</a></td>
                                    </tr>
                                   
                                    </tbody>
                                    </table>
                                    </div>
                                    <br />
                			</div>
            			</div> 
                       
                        <div class="clearfix"></div>
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:15px;">
            				<div class="round-rect">
                				<h1 align="center">Lenders Summary</h1>
                                <div style="overflow-x:auto">
                                    <table class="table table-striped lenders-table">
                        <thead>
                        <tr>
                        <th>Name</th>
                        <th>Leads</th>
                        <th>Login ID</th>
                        <th>Reject</th>
                        <th>App Id</th>
						<th>Approval</th>
                        <th>Spill Over</th>
                        <th>Sanction</th>
        				<th>Disbursement</th>
                        
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td>NG</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        </tr>
                        <tr>
                        <td>CF</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        </tr>
                        <tr>
                        <td>BF</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        </tr>
                        <tr>
                        <td>ABFL</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        </tr>
                        </tbody>
                        </table>
                        </div>
                                    <br />
                			</div>
            			</div>   
                        <div class="clearfix"></div>
                        <br />
                        <div class="col-md-8 col-sm-8 col-xs-12">
            				<div class="round-rect">
                				<h1 align="center">Upcoming Events</h1>
                                    <marquee direction="up" height="100px" scrollamount="3" onmouseover="stop">
                                    <ul class="events">
                                    <li> Events 1</li>
                                    <li> Events 2</li>
                                    <li> Events 3</li>
                                    <li> Events 4</li>
                                    <li> Events 5</li>
                                    <li> Events 6</li>
                                    </ul></marquee>
                                    <br />
                			</div>
            			</div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="flash_news" style="height:140px;">
                   		<p align="center" style="top:30px;">Performer News flash</p>
                   </div>
                   </div>
                        
                        <!--<div class="col-md-4 flash_news" style="height:150px; margin-top:15px;">
                <p align="center" style="top:60px;">Performer News flash</p>

                </div>-->
                        
                </div>
			</div>
			</div>
			<?php }?>
        
       	<!---------------------------------Operational Dashboard End-------------------->  
      
        
       	<!---------------------------------Retails Regional Sales Manager Dashboard Start-------------------->  
      <?php  if($_SESSION['department_id']=="3" && $_SESSION['level']=="3")  { ?>
			<div class="row">
			<div class="container2">	
            	<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="round-rect">
                    <p align="center" class="active-alert"><sup>**</sup> <u>You have active alerts</u></p>
                    </div>
                    <br />
                        <div class="round-rect one">
                        <h1 align="center">Achievement <?php echo $year.'-'.$year1; ?></h1>
                           <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge" style="width: 200px; height: 200px;"></div>
                            </div>
                           <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge1" style="width: 200px; height: 200px;"></div>
                            </div> 
                            <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge2" style="width: 200px; height: 200px;"></div>
                            </div>
                         </div>
                    </div>
       			 </div>
              <br />
              <div class="row">
            
            <div class="col-md-8 col-sm-8 col-xs-12">
            	<div class="round-rect">
                	<h1 align="center">LGM Summary <?php echo $year.'-'.$year1; ?></h1>
                    <div style="overflow-x:auto">
                    
        <table class="table table-striped LGM-table">
        <thead>
        <tr>
       <tr>
        <th>Leads</th>
        <th>W.I.P</th>
        <th>Hold</th>
        <th>Login</th>
        <th>Reject</th>
        <th>Approve</th>
        <th>Spill-Over</th>
        <th>Sanction</th>
        <th>Disbursement</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td><a href="index.php?control=lgm_report&task=new_report" style="text-decoration:underline; color:#30a5ff !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='0'")); ?></a></td>
        
        <td> <a href="index.php?control=lgm_report&task=wip_report" style="text-decoration:underline; color:#33F !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='1'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=hold_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='2'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=verify_report" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='3'")); ?></a></td>
        
       <td><a href="index.php?control=lgm_report&task=rejected_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='4'")); ?></a></td>
       
        <td><a href="index.php?control=lgm_report&task=lender_verify_lgm" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from lender_loan_assign where lander_status='1'")); ?></a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">6</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">7</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">8</a></td>
        </tr>
        </tbody>
        </table>
        </div>
        <br />

                </div>
                </br>
                <div class="round-rect">
                	<h1 align="center">Target <?php echo $year.'-'.$year1; ?> (Lakhs)</h1>
                    <div style="overflow-x:auto">
        <table class="table table-striped LGM-table">
        <thead>
        <tr>
        <th>Week</th>
        <th>Month</th>
        <th>Quarter</th>
        <th>Half-Year</th>
        <th>Annual</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        </tr>
        </tbody>
        </table>    </div><br />
    

                </div>
 
                
                
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
            	<div class="round-rect" style=" border:none;">
                <input type="button" value="Add Lead" class="add_lead" />
                	<h4 class="retail">Team Report <?php echo $year.'-'.$year1; ?></h4>
                    
                    <table class="table table-striped retail-table">
                        <thead>
                        <tr>
                        <th>Name</th>
                        <th>Achieved</th>
                        <th>Target</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td>Asif</td>
                        <td>2</td>
                        <td>5</td>
                        </tr> 
                        </tbody>
                        </table>
                      </div>
                      <br />
                   <div class="flash_news" style="height:140px;">
                   		<p align="center" style="top:30px;">Performer News flash</p>
                   </div>
                   
                        

            
            </div>
        </div>
	
			</div>
			</div>
			<?php }?>
        
       	<!---------------------------------Retails Regional Sales Manager Dashboard End-------------------->  
       
         
        
       	<!---------------------------------Retails Area Sales Manager Dashboard Start-------------------->  
      <?php  if($_SESSION['department_id']=="3" && $_SESSION['level']=="4")  { ?>
			<div class="row">
			<div class="container2">	
            	<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="round-rect">
                    <p align="center" class="active-alert"><sup>**</sup> <u>You have active alerts</u></p>
                    </div>
                    <br />
                        <div class="round-rect one">
                        <h1 align="center">Achievement <?php echo $year.'-'.$year1; ?></h1>
                           <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge" style="width: 200px; height: 200px;"></div>
                            </div>
                           <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge1" style="width: 200px; height: 200px;"></div>
                            </div> 
                            <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge2" style="width: 200px; height: 200px;"></div>
                            </div>
                         </div>
                    </div>
       			 </div>
              <br />
              <div class="row">
            
            <div class="col-md-8 col-sm-8 col-xs-12">
            	<div class="round-rect">
                	<h1 align="center">LGM Summary <?php echo $year.'-'.$year1; ?></h1>
                    <div style="overflow-x:auto;">
        <table class="table table-striped LGM-table">
        <thead>
        <tr>
      <tr>
        <th>Leads</th>
        <th>W.I.P</th>
        <th>Hold</th>
        <th>Login</th>
        <th>Reject</th>
        <th>Approve</th>
        <th>Spill-Over</th>
        <th>Sanction</th>
        <th>Disbursement</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td><a href="index.php?control=lgm_report&task=new_report" style="text-decoration:underline; color:#30a5ff !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='0'")); ?></a></td>
        
        <td> <a href="index.php?control=lgm_report&task=wip_report" style="text-decoration:underline; color:#33F !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='1'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=hold_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='2'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=verify_report" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='3'")); ?></a></td>
        
       <td><a href="index.php?control=lgm_report&task=rejected_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='4'")); ?></a></td>
       
        <td><a href="index.php?control=lgm_report&task=lender_verify_lgm" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from lender_loan_assign where lander_status='1'")); ?></a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">6</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">7</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">8</a></td>
        </tr>
        </tbody>
        </table>
        </div><br />
        

                </div>
                </br>
                <div class="round-rect">
                	<h1 align="center">Target <?php echo $year.'-'.$year1; ?></h1>
                    <div style="overflow-x:auto;">
        <table class="table table-striped LGM-table">
        <thead>
        <tr>
        <th>Week</th>
        <th>Month</th>
        <th>Quarter</th>
        <th>Half-Year</th>
        <th>Annual</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        </tr>
        </tbody>
        </table>
        </div>
        <br />

                </div>
 
                
                
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
            	<div class="round-rect" style=" border:none;">
                <input type="button" value="Add Lead" class="add_lead" />
                	<h4 class="retail">Team Report <?php echo $year.'-'.$year1; ?></h4>
                    
                    <table class="table table-striped retail-table">
                        <thead>
                        <tr>
                        <th>Name</th>
                        <th>Achieved</th>
                        <th>Target</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td>Asif</td>
                        <td>2</td>
                        <td>5</td>
                        </tr> 
                        </tbody>
                        </table>
                      </div>
                      <br />
                   <div class="flash_news" style="height:140px;">
                   		<p align="center" style="top:30px;">Performer News flash</p>
                   </div>
                   
                        

            
            </div>
        </div>
	
			</div>
			</div>
			<?php }?>
        
       	<!---------------------------------Retails Area Sales Manager Dashboard End-------------------->  
        
     
        <!---------------------------------Retails Sales Dashboard Start-------------------->  	
		  <?php 	if($_SESSION['department_id']=="3" && $_SESSION['level']=="5")  { ?>
			<div class="row">
			<div class="container2">	
            	<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="round-rect">
                    <p align="center" class="active-alert"><sup>**</sup> <u>You have active alerts</u></p>
                    </div>
                    <br />
                        <div class="round-rect one">
                        <h1 align="center">Achievement <?php echo $year.'-'.$year1; ?></h1>
                           <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge" style="width: 200px; height: 200px;"></div>
                            </div>
                           <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge1" style="width: 200px; height: 200px;"></div>
                            </div> 
                            <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge2" style="width: 200px; height: 200px;"></div>
                            </div>
                         </div>
                    </div>
       			 </div>
              <br />
              <div class="col-md-12 col-sm-12 col-xs-12">
            	<div class="round-rect" style=" border:none;">
                <input type="button" value="Add Lead" class="add_lead" />
                	
                    
                    
                      </div>
                      <br />
                      <div class="round-rect">
                	<h1 align="center">LGM Summary <?php echo $year.'-'.$year1; ?></h1>
                    <div style="overflow-x:auto;">
        <table class="table table-striped LGM-table">
        <thead>
       <tr>
        <th>Leads</th>
        <th>W.I.P</th>
        <th>Hold</th>
        <th>Login</th>
        <th>Reject</th>
        <th>Approve</th>
        <th>Spill-Over</th>
        <th>Sanction</th>
        <th>Disbursement</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td><a href="index.php?control=lgm_report&task=new_report" style="text-decoration:underline; color:#30a5ff !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='0'")); ?></a></td>
        
        <td> <a href="index.php?control=lgm_report&task=wip_report" style="text-decoration:underline; color:#33F !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='1'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=hold_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='2'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=verify_report" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='3'")); ?></a></td>
        
       <td><a href="index.php?control=lgm_report&task=rejected_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='4'")); ?></a></td>
       
        <td><a href="index.php?control=lgm_report&task=lender_verify_lgm" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from lender_loan_assign where lander_status='1'")); ?></a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">6</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">7</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">8</a></td>
        </tr>
        </tbody>
        </table>
        </div>
        <br />

                </div>
                   
            </div>
              <div class="row">
            
            <div class="col-md-8 col-sm-8 col-xs-12">
            	
                </br>
                <div class="round-rect">
                	<h1 align="center">Target <?php echo $year.'-'.$year1; ?> </h1>
                    <div style="overflow-x:auto;">
        <table class="table table-striped LGM-table">
        <thead>
        <tr>
        <th>Week</th>
        <th>Month</th>
        <th>Quarter</th>
        <th>Half-Year</th>
        <th>Annual</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td><?php $date = date('Y-m-d'); echo $week =mysql_query("SELECT mts.mca_amount FROM `master_target_specify` as mts join master_target as mt where mts.target_id = mt.id and mts.emp_id = '".$_SESSION['adminid']."' and tenure_type = 'week' and $date between from_date and to_date and   ORDER BY `id` DESC"); ?></td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        </tr>
        </tbody>
        </table>
        </div><br />

                </div>
 
                
                
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
            
                      <br />
                   <div class="flash_news" style="height:140px;">
                   		<p align="center" style="top:30px;">Performer News flash</p>
                   </div>
                   
                        

            
            </div>
        </div>
        </div>
			</div><!--/.row-->
			<?php }?>
		
         	<!---------------------------------Retails Sales  Dashboard End-------------------->  	
			
			<!---------------------------------Outbound Caller Team Leader  Dashboard Start-------------------->  
		 <?php 	if($_SESSION['department_id']=="4" && $_SESSION['level']=="4")  { ?>
			<div class="row">
			<div class="container2">	
            	<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="round-rect">
                    <p align="center" class="active-alert"><sup>**</sup> <u>You have active alerts</u></p>
                    </div>
                    <br />
                        <div class="round-rect one">
                        <h1 align="center">Achievement <?php echo $year.'-'.$year1; ?></h1>
                           <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge" style="width: 200px; height: 200px;"></div>
                            </div>
                           <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge1" style="width: 200px; height: 200px;"></div>
                            </div> 
                            <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge2" style="width: 200px; height: 200px;"></div>
                            </div>
                         </div>
                    </div>
       			 </div>
              <br />
              <div class="row">
            
            <div class="col-md-8 col-sm-8 col-xs-12">
            	<div class="round-rect">
                	<h1 align="center">LGM Summary <?php echo $year.'-'.$year1; ?></h1>
                    <div style="overflow-x:auto;">
        <table class="table table-striped LGM-table">
        <thead>
        <tr>
        <th>Leads</th>
        <th>W.I.P</th>
        <th>Hold</th>
        <th>Login</th>
        <th>Reject</th>
        <th>Approve</th>
        <th>Spill-Over</th>
        <th>Sanction</th>
        <th>Disbursement</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td><a href="index.php?control=lgm_report&task=new_report" style="text-decoration:underline; color:#30a5ff !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='0'")); ?></a></td>
        
        <td> <a href="index.php?control=lgm_report&task=wip_report" style="text-decoration:underline; color:#33F !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='1'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=hold_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='2'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=verify_report" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='3'")); ?></a></td>
        
       <td><a href="index.php?control=lgm_report&task=rejected_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='4'")); ?></a></td>
       
        <td><a href="index.php?control=lgm_report&task=lender_verify_lgm" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from lender_loan_assign where lander_status='1'")); ?></a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">6</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">7</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">8</a></td>
        </tr>
        </tbody>
        </table>
        </div><br />

                </div>
                </br>
                <div class="round-rect">
                	<h1 align="center">Target <?php echo $year.'-'.$year1; ?></h1>
                    <div style="overflow-x:auto;">
        <table class="table table-striped LGM-table">
        <thead>
        <tr>
        <th>Week</th>
        <th>Month</th>
        <th>Quarter</th>
        <th>Half-Year</th>
        <th>Annual</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        </tr>
        </tbody>
        </table>
        </div><br />

                </div>
 
                
                
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
            	<div class="round-rect" style=" border:none;">
                <input type="button" value="Add Lead" class="add_lead" />
                	<h4 class="retail">Team Report <?php echo $year.'-'.$year1; ?></h4>
                    
                    <table class="table table-striped retail-table">
                        <thead>
                        <tr>
                        <th>Name</th>
                        <th>Achieved</th>
                        <th>Target</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td>Asif</td>
                        <td>2</td>
                        <td>5</td>
                        </tr> 
                        </tbody>
                        </table>
                      </div>
                      <br />
                   <div class="flash_news" style="height:140px;">
                   		<p align="center" style="top:30px;">Performer News flash</p>
                   </div>
                   
                        

            
            </div>
        </div>
            </div>
            </div><!--/.row-->
			<?php } ?>
            <!---------------------------------Outbound Caller Team Leader Dashboard End-------------------->  
            
			<!---------------------------------Outbound Caller   Dashboard Start-------------------->  
		 <?php 	if($_SESSION['department_id']=="4" && $_SESSION['level']=="5")  { ?>
			<div class="row">
			<div class="container2">	
            	<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="round-rect">
                    <p align="center" class="active-alert"><sup>**</sup> <u>You have active alerts</u></p>
                    </div>
                    <br />
                        <div class="round-rect one">
                        <h1 align="center">Achievement<?php echo $year.'-'.$year1; ?></h1>
                           <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge" style="width: 200px; height: 200px;"></div>
                            </div>
                           <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge1" style="width: 200px; height: 200px;"></div>
                            </div> 
                            <div class="col-md-4 col-sm-4 speed_meter">
                                <div id="jqRadialGauge2" style="width: 200px; height: 200px;"></div>
                            </div>
                         </div>
                    </div>
       			 </div>
              <br />
              <div class="col-md-12 col-sm-12 col-xs-12">
            	<div class="round-rect" style=" border:none;">
               <a href="index.php?control=customer&task=addnew"><input type="button" value="Add Lead" class="add_lead" /></a>
                	
                    
                    
                      </div>
                      <br />
                      <div class="round-rect">
                	<h1 align="center">LGM Summary <?php echo $year.'-'.$year1; ?></h1>
                    <div style="overflow-x:auto">
        <table class="table table-striped LGM-table">
        <thead>
        <tr>
        <th>Leads</th>
        <th>W.I.P</th>
        <th>Hold</th>
        <th>Login</th>
        <th>Reject</th>
        <th>Approve</th>
        <th>Spill-Over</th>
        <th>Sanction</th>
        <th>Disbursement</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td><a href="index.php?control=lgm_report&task=new_report" style="text-decoration:underline; color:#30a5ff !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='0'")); ?></a></td>
        
        <td> <a href="index.php?control=lgm_report&task=wip_report" style="text-decoration:underline; color:#33F !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='1'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=hold_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='2'")); ?></a></td>
        
        <td><a href="index.php?control=lgm_report&task=verify_report" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='3'")); ?></a></td>
        
       <td><a href="index.php?control=lgm_report&task=rejected_report" style="text-decoration:underline; color:#C00 !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from customer where verify_status='4'")); ?></a></td>
       
        <td><a href="index.php?control=lgm_report&task=lender_verify_lgm" style="text-decoration:underline; color:green !important"><?php echo $lgm = mysql_num_rows(mysql_query("select * from lender_loan_assign where lander_status='1'")); ?></a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">6</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">7</a></td>
        <td><a href="#" style="text-decoration:underline; color:#C00 !important">8</a></td>
        </tr>
        </tbody>
        </table>
        </div><br />

                </div>
                   
            </div>
              <div class="row">
            
            <div class="col-md-8 col-sm-8 col-xs-12">
            	
                </br>
                <div class="round-rect">
                	<h1 align="center">Target <?php echo $year.'-'.$year1; ?> </h1>
                    <div style="overflow-x:auto">
        <table class="table table-striped LGM-table">
        <thead>
        <tr>
        <th>Week</th>
        <th>Month</th>
        <th>Quarter</th>
        <th>Half-Year</th>
        <th>Annual</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        </tr>
        </tbody>
        </table>
        </div><br />

                </div>
 
                
                
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
            
                      <br />
                   <div class="flash_news" style="height:140px;">
                   		<p align="center" style="top:30px;">Performer News flash</p>
                   </div>
                   
                        

            
            </div>
        </div>
        </div>
        </div><!--/.row-->
			<?php } ?>
            <!---------------------------------Outbound Caller Dashboard End-------------------->  
            
           
            
            
            
            <!---------------------------------Lender  Dashboard Start--------------------> 
			<?php	if($_SESSION['utype']=='lender' && $_SESSION['post_id']=='lender'){ ?>
        <div class="row">
        <div class="container2">
        <div class="col-md-4 col-sm-6 col-xs-12">
				<a href="index.php?control=lgm_report&task=lender_lgm">
                <div class="panel panel-teal panel-widget ">
					<div class="row no-padding district">
                    <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                    <div class="digit "><?php echo
					$lender = mysql_num_rows(mysql_query("select * from lender_loan_assign where lender_id = '".$_SESSION['adminid']."' and lander_status='0' order by id DESC"));
					?></div>
							<div class="large">Pending LGM</div>
						</div>
						<div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
						<i class="fa fa-user-plus" aria-hidden="true"></i>

						</div>
                        <div class="clearfix"></div>
						<div class="block-footer">
                       <strong> <div class="more_info">More info <i class="fa fa-arrow-circle-right"></i></div></strong>
                        </div>
					</div>
				</div></a>
			</div>
            
        <div class="col-md-4 col-sm-6 col-xs-12"> 
           <a href="index.php?control=lgm_report&task=verify_lgm">
             <div class="panel panel-teal panel-widget ">
                  <div class="row no-padding category">
                     <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                     <div class="digit "><?php echo $lgm = mysql_num_rows(mysql_query("select * from lender_loan_assign where lender_id = '".$_SESSION['adminid']."' and lander_status='1' order by id DESC")); ?></div>
                        <div class="large">Total Verify LGM</div>
                    </div>
                   <div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>

                     </div>
                    <div class="clearfix"></div>
                    <div class="block-footer">
                    <strong><div class="more_info">More info <i class="fa fa-arrow-circle-right"></i></div></strong>
                    </div>
                    </div>
            </div></a>
            </div>
            <?php $lender = mysql_query("select * from `lender_available` where lender_id = '".$_SESSION['adminid']."' and check_status='1' and status='1'");
						while($lender_product = mysql_fetch_array($lender))
			{
			?>
            <div class="col-md-4 col-sm-6 col-xs-12">
				<a href="index.php?control=lgm_report&task=verify_lgm">
                <div class="panel panel-teal panel-widget ">
                		<?php
				        /* if($lender_product['target_percentage_id']=='1')
				 		 {  ?>
					     <div class="row no-padding district">
                       <?php }
					    if($lender_product['target_percentage_id']=='2')
						{ ?>
						<div class="row no-padding category">
					    <?php	}
						if($lender_product['target_percentage_id']=='3')
						{ ?>
						<div class="row no-padding mohalla">
						<?php 	}
						if($lender_product['target_percentage_id']=='4')
						{ ?>
						<div class="row no-padding assembley">
						<?php 	}
						if($lender_product['target_percentage_id']=='5')
						{ ?>
                       
						<div class="row no-padding district">
						 <?php }
					    if($lender_product['target_percentage_id']=='6')
						{ ?>
						<div class="row no-padding category">
					    <?php	}
						if($lender_product['target_percentage_id']=='7')
						{ ?>
						<div class="row no-padding mohalla">
						<?php 	}
						if($lender_product['target_percentage_id']=='8')
						{ ?>
						<div class="row no-padding assembley">
						<?php 	}
                      	if($lender_product['target_percentage_id']=='9')
						{ ?>
						<div class="row no-padding district">
						<?php 	}
                        if($lender_product['target_percentage_id']=='10')
						{ ?>
						<div class="row no-padding category">
						<?php  } */?>
                        
                        <div class="row no-padding <?php if($lender_product['target_percentage_id']=='1'){echo "district";}  if($lender_product['target_percentage_id']=='2'){echo "category";} if($lender_product['target_percentage_id']=='3'){echo "mohalla";} if($lender_product['target_percentage_id']=='4'){echo "assembley";} if($lender_product['target_percentage_id']=='5'){echo "district";} if($lender_product['target_percentage_id']=='6'){echo "category";} if($lender_product['target_percentage_id']=='7'){echo "mohalla";} if($lender_product['target_percentage_id']=='8'){echo "assembley";} if($lender_product['target_percentage_id']=='9'){echo "district";} if($lender_product['target_percentage_id']=='10'){echo "category";}?>">
							
                    <div class="col-md-7 col-xs-6 col-sm-9 col-lg-7 widget-right">
                  <div class="digit "><?php echo $lgm = mysql_num_rows(mysql_query("select * from lender_loan_assign where lender_id = '".$_SESSION['adminid']."' and loan_product='".$lender_product['target_percentage_id']."'  and lander_status='1' order by id DESC")); ?></div>
							<div class="large"><?php echo $lender_product['loan_code'];?></div>
						</div>
						<div class="col-md-5 col-xs-6 col-sm-3 col-lg-5 widget-left">
						<i class="fa fa-user-plus" aria-hidden="true"></i>

						</div>
                        <div class="clearfix"></div>
						<div class="block-footer">
                       <strong> <div class="more_info">More info <i class="fa fa-arrow-circle-right"></i></div></strong>
                        </div>
					</div>
				</div></a>
			</div>
            
            <?php } ?>
        
			
			
		</div>
        </div>
        <?php } ?>
<br><br>
  
        <br><br>
        
        </div>
     
   <!-- <../../template/bank/speed-meter/js/jquery.jqRangeSlider.min.jsscript src="speed-meter/js/jquery-1.11.1.min.js" type="text/javascript"></script>-->
  
         <script lang="javascript" type="text/javascript">
        $(document).ready(function () {

            var gradient1 = {
                type: 'linearGradient',
                x0: 0,
                y0: 0.5,
                x1: 1,
                y1: 0.5,
				colorStops: [{ offset: 0, color: '#da3611' },
                             { offset: 1, color: '#da370e' }]
				
                /*colorStops: [{ offset: 0, color: '#C5F80B' },
                             { offset: 1, color: '#6B8901' }]*/
            };

            var gradient2 = {
                type: 'linearGradient',
                x0: 0.5,
                y0: 0,
                x1: 0.5,
                y1: 1,
                colorStops: [{ offset: 0, color: '#fe9a04' },
                             { offset: 1, color: '#f9a526' }]
            };
			
			var gradient3 = {
                type: 'linearGradient',
                x0: 0.5,
                y0: 0,
                x1: 0.5,
                y1: 1,
                colorStops: [{ offset: 0, color: '#099915' },
                             { offset: 1, color: '#2f8b37' }]
            };

            var anchorGradient = {
                type: 'radialGradient',
                x0: 0.35,
                y0: 0.35,
                r0: 0.0,
                x1: 0.35,
                y1: 0.35,
                r1: 1,
                colorStops: [{ offset: 0, color: '#4F6169' },
                             { offset: 1, color: '#252E32' }]
            };

            $('#jqRadialGauge').jqRadialGauge({
                background: '#F7F7F7',
                border: {
                    lineWidth: 5,
                    strokeStyle: '#76786A',
                    padding: 0
                },
                shadows: {
                    enabled: true
                },
                anchor: {
                    visible: true,
                    fillStyle: anchorGradient,
                    radius: 0.10
                },
                tooltips: {
                    disabled: true,
                    highlighting: true
                },
                animation: {
                    duration: 1
                },
                annotations: [
                                {
                                    text: 'Month',
                                    font: '18px sans-serif',
                                    horizontalOffset: 0.5,
                                    verticalOffset: 0.80
                                }
                ],
                scales: [
                         {
                             minimum: 0,
                             maximum: 150,
                             startAngle:40,
                             endAngle: 400,
                             majorTickMarks: {
								 visible: false,             /*Here major tick Mark Number*/
                                 length: 12,
                                 lineWidth: 2,
                                 interval: 10,
                                 offset: 0.84
                             },
                             minorTickMarks: {              /*Here Show minor tick mark*/
                                 visible: false,
                                 length: 8,
                                 lineWidth: 2,
                                 interval: 2,
                                 offset: 0.84
                             },
                             labels: {                    /*Here Show Number*/
								 visible: false,
                                 orientation: 'horizontal',
                                 interval: 10,
                                 offset: 1.00
                             },
                             needles: [
                                        {
                                            value: 50,
                                            type: 'pointer',
                                            outerOffset: 0.8,
                                            mediumOffset: 0.7,
                                            width: 10,
                                            fillStyle: '#252E32'
                                        }
                             ],
                             ranges: [
                                        {
                                            outerOffset: 0.91,
                                            innerStartOffset: 0.67,
                                            innerEndOffset: 0.67,
                                            startValue: 40,
                                            endValue: 76,
                                            fillStyle: gradient1
                                        },
                                        {
                                            outerOffset: 0.91,
                                            innerStartOffset: 0.67,
                                            innerEndOffset: 0.67,
                                            startValue: 76,
                                            endValue: 113,
                                            fillStyle: gradient2
                                        },
										{
                                            outerOffset: 0.91,
                                            innerStartOffset: 0.67,
                                            innerEndOffset: 0.67,
                                            startValue: 113,
                                            endValue: 150,
                                            fillStyle: gradient3
                                        }
										
                             ]
                         }
                ]
            });

            /*$('#jqRadialGauge').bind('tooltipFormat', function (e, data) {

                var tooltip = '<b>Element: ' + data.elementType + '</b> ' + '<br />';

                switch (data.elementType) {

                    case 'needle':
                        tooltip += 'Value: ' + data.value;
                        break;
                    case 'range':
                        tooltip += 'Start Value: ' + data.startValue + '<br/>End Value: ' + data.endValue;
                }

                return tooltip;
            });*/
        });
    </script>
<script lang="javascript" type="text/javascript">
        $(document).ready(function () {

            var gradient1 = {
                type: 'linearGradient',
                x0: 0,
                y0: 0.5,
                x1: 1,
                y1: 0.5,
				colorStops: [{ offset: 0, color: '#da3611' },
                             { offset: 1, color: '#da370e' }]
				
                /*colorStops: [{ offset: 0, color: '#C5F80B' },
                             { offset: 1, color: '#6B8901' }]*/
            };

            var gradient2 = {
                type: 'linearGradient',
                x0: 0.5,
                y0: 0,
                x1: 0.5,
                y1: 1,
                colorStops: [{ offset: 0, color: '#fe9a04' },
                             { offset: 1, color: '#f9a526' }]
            };
			
			var gradient3 = {
                type: 'linearGradient',
                x0: 0.5,
                y0: 0,
                x1: 0.5,
                y1: 1,
                colorStops: [{ offset: 0, color: '#099915' },
                             { offset: 1, color: '#2f8b37' }]
            };

            var anchorGradient = {
                type: 'radialGradient',
                x0: 0.35,
                y0: 0.35,
                r0: 0.0,
                x1: 0.35,
                y1: 0.35,
                r1: 1,
                colorStops: [{ offset: 0, color: '#4F6169' },
                             { offset: 1, color: '#252E32' }]
            };

            $('#jqRadialGauge1').jqRadialGauge({
                background: '#F7F7F7',
                border: {
                    lineWidth: 5,
                    strokeStyle: '#76786A',
                    padding: 0
                },
                shadows: {
                    enabled: true
                },
                anchor: {
                    visible: true,
                    fillStyle: anchorGradient,
                    radius: 0.10
                },
                tooltips: {
                    disabled: true,
                    highlighting: true
                },
                animation: {
                    duration: 1
                },
                annotations: [
                                {
                                    text: 'Quarter',
                                    font: '18px sans-serif',
                                    horizontalOffset: 0.5,
                                    verticalOffset: 0.80
                                }
                ],
                scales: [
                         {
                             minimum: 0,
                             maximum: 150,
                             startAngle:40,
                             endAngle: 400,
                             majorTickMarks: {
								 visible: false,             /*Here major tick Mark Number*/
                                 length: 12,
                                 lineWidth: 2,
                                 interval: 10,
                                 offset: 0.84
                             },
                             minorTickMarks: {              /*Here Show minor tick mark*/
                                 visible: false,
                                 length: 8,
                                 lineWidth: 2,
                                 interval: 2,
                                 offset: 0.84
                             },
                             labels: {                    /*Here Show Number*/
								 visible: false,
                                 orientation: 'horizontal',
                                 interval: 10,
                                 offset: 1.00
                             },
                             needles: [
                                        {
                                            value: 100,
                                            type: 'pointer',
                                            outerOffset: 0.8,
                                            mediumOffset: 0.7,
                                            width: 10,
                                            fillStyle: '#252E32'
                                        }
                             ],
                             ranges: [
                                        {
                                            outerOffset: 0.91,
                                            innerStartOffset: 0.67,
                                            innerEndOffset: 0.67,
                                            startValue: 40,
                                            endValue: 76,
                                            fillStyle: gradient1
                                        },
                                        {
                                            outerOffset: 0.91,
                                            innerStartOffset: 0.67,
                                            innerEndOffset: 0.67,
                                            startValue: 76,
                                            endValue: 113,
                                            fillStyle: gradient2
                                        },
										{
                                            outerOffset: 0.91,
                                            innerStartOffset: 0.67,
                                            innerEndOffset: 0.67,
                                            startValue: 113,
                                            endValue: 150,
                                            fillStyle: gradient3
                                        }
										
                             ]
                         }
                ]
            });

            /*$('#jqRadialGauge').bind('tooltipFormat', function (e, data) {

                var tooltip = '<b>Element: ' + data.elementType + '</b> ' + '<br />';

                switch (data.elementType) {

                    case 'needle':
                        tooltip += 'Value: ' + data.value;
                        break;
                    case 'range':
                        tooltip += 'Start Value: ' + data.startValue + '<br/>End Value: ' + data.endValue;
                }

                return tooltip;
            });*/
        });
    </script>
    <script lang="javascript" type="text/javascript">
        $(document).ready(function () {

            var gradient1 = {
                type: 'linearGradient',
                x0: 0,
                y0: 0.5,
                x1: 1,
                y1: 0.5,
				colorStops: [{ offset: 0, color: '#da3611' },
                             { offset: 1, color: '#da370e' }]
				
                /*colorStops: [{ offset: 0, color: '#C5F80B' },
                             { offset: 1, color: '#6B8901' }]*/
            };

            var gradient2 = {
                type: 'linearGradient',
                x0: 0.5,
                y0: 0,
                x1: 0.5,
                y1: 1,
                colorStops: [{ offset: 0, color: '#fe9a04' },
                             { offset: 1, color: '#f9a526' }]
            };
			
			var gradient3 = {
                type: 'linearGradient',
                x0: 0.5,
                y0: 0,
                x1: 0.5,
                y1: 1,
                colorStops: [{ offset: 0, color: '#099915' },
                             { offset: 1, color: '#2f8b37' }]
            };

            var anchorGradient = {
                type: 'radialGradient',
                x0: 0.35,
                y0: 0.35,
                r0: 0.0,
                x1: 0.35,
                y1: 0.35,
                r1: 1,
                colorStops: [{ offset: 0, color: '#4F6169' },
                             { offset: 1, color: '#252E32' }]
            };

            $('#jqRadialGauge2').jqRadialGauge({
                background: '#F7F7F7',
                border: {
                    lineWidth: 5,
                    strokeStyle: '#76786A',
                    padding: 0
                },
                shadows: {
                    enabled: true
                },
                anchor: {
                    visible: true,
                    fillStyle: anchorGradient,
                    radius: 0.10
                },
                tooltips: {
                    disabled: true,
                    highlighting: true
                },
                animation: {
                    duration: 1
                },
                annotations: [
                                {
                                    text: 'Year',
                                    font: '18px sans-serif',
                                    horizontalOffset: 0.5,
                                    verticalOffset: 0.80
                                }
                ],
                scales: [
                         {
                             minimum: 0,
                             maximum: 150,
                             startAngle:40,
                             endAngle: 400,
                             majorTickMarks: {
								 visible: false,             /*Here major tick Mark Number*/
                                 length: 12,
                                 lineWidth: 2,
                                 interval: 10,
                                 offset: 0.84
                             },
                             minorTickMarks: {              /*Here Show minor tick mark*/
                                 visible: false,
                                 length: 8,
                                 lineWidth: 2,
                                 interval: 2,
                                 offset: 0.84
                             },
                             labels: {                    /*Here Show Number*/
								 visible: false,
                                 orientation: 'horizontal',
                                 interval: 10,
                                 offset: 1.00
                             },
                             needles: [
                                        {
                                            value: 130,
                                            type: 'pointer',
                                            outerOffset: 0.8,
                                            mediumOffset: 0.7,
                                            width: 10,
                                            fillStyle: '#252E32'
                                        }
                             ],
                             ranges: [
                                        {
                                            outerOffset: 0.91,
                                            innerStartOffset: 0.67,
                                            innerEndOffset: 0.67,
                                            startValue: 40,
                                            endValue: 76,
                                            fillStyle: gradient1
                                        },
                                        {
                                            outerOffset: 0.91,
                                            innerStartOffset: 0.67,
                                            innerEndOffset: 0.67,
                                            startValue: 76,
                                            endValue: 113,
                                            fillStyle: gradient2
                                        },
										{
                                            outerOffset: 0.91,
                                            innerStartOffset: 0.67,
                                            innerEndOffset: 0.67,
                                            startValue: 113,
                                            endValue: 150,
                                            fillStyle: gradient3
                                        }
										
                             ]
                         }
                ]
            });

            /*$('#jqRadialGauge').bind('tooltipFormat', function (e, data) {

                var tooltip = '<b>Element: ' + data.elementType + '</b> ' + '<br />';

                switch (data.elementType) {

                    case 'needle':
                        tooltip += 'Value: ' + data.value;
                        break;
                    case 'range':
                        tooltip += 'Start Value: ' + data.startValue + '<br/>End Value: ' + data.endValue;
                }

                return tooltip;
            });*/
        });
    </script>
    
        <script lang="javascript" type="text/javascript">

        $(document).ready(function () {

            var fillStyle1 = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 1,
                y1: 0,
                colorStops: [{ offset: 0, color: '#557ebc' },
                             { offset: 0.49, color: '#4b84b9' },
                             { offset: 0.5, color: '#4b84b9' },
                             { offset: 1, color: '#4b84b9' }]
            };

            var fillStyle2 = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 1,
                y1: 0,
                colorStops: [{ offset: 0, color: '#ba534a' },
                             { offset: 0.49, color: '#ba534a' },
                             { offset: 0.5, color: '#ba534a' },
                             { offset: 1, color: '#ba534a' }]
            };

            $('#jqChart').jqChart({
                title: { text: '' },
                legend: {
                    location: 'bottom',
                    visible: false
                },
                animation: { duration: 1 },
               series: [
                    {
                        type: 'column',
                        data: [['', 3], ['', 0], ['', 15], ['', ]],
                        fillStyle: fillStyle1
                    },
                    {
                        type: 'column',
                        data: [['', 5], ['', 0], ['', 20], ['', ]],
                        fillStyle: fillStyle2
                    }
                ]
            });
        });
    </script>
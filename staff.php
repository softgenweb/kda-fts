<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Super Speciality Cancer Institute 
& Hospital, C G City, Lucknow</title>

    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!--Favicon-->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">

    <!-- Stylesheets -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- Color Style -->
    <link rel="stylesheet" href="css/switcher.css">
    <link rel="stylesheet" href="#" id="colors">

</head>

<body>
<div class="page-wrapper">
<?php include("header.php"); ?>


    <!--Page Title-->
    <section class="page-title" style="background: url(images/background/6.jpg);">
        <div class="container">
            <div class="text text-center">
                <h2>Award letters(NOA)</h2>
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li>/</li>
                    <li>Award letters(NOA)</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!--About Section-->
    <section class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <table border="1" cellspacing="0" cellpadding="0" width="1178" id="customers">
  <tr>
    <th width="199" valign="top"><strong>S.No</strong></th>
    <th width="226" valign="top"><strong>Type of Tender</strong></th>
    <th width="271" valign="top">Title of award letter</th>
    <th width="244" valign="top">Date of award</th>
  </tr>
  
  

  <tr><td valign="top"></td>
    <td valign="top"><p></p></td>
    <td valign="top"><p></p></td>
    <td valign="top"><p></p></td>
  </tr>
  
  
  
  </table>
                </div>
                
          </div>
        </div>
    </section>
    <!--End About Section-->





    <!--Footer Section-->
    <?php include("footer.php"); ?>
    <!--End Footer Section-->

</div>
<!--End pagewrapper-->


<!--Scroll to top-->
<?php include("bottom.php"); ?>



<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/isotope.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/validate.js"></script>
<script src="js/wow.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/switcher.js"></script>
<script src="js/script.js"></script>
</body>
</html>

<?php

require_once('dbaccess.php');
require_once('textconfig/config.php');


if (file_exists('configuration.php')) {
    
    require_once('configuration.php');
}


class reportClass extends DbAccess
{
    public $view = '';
    public $name = 'report';
    
    
    
    /***************************************************** POST START **********************************************************/
    
    
    
    function show()
    {
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        $dateTo   = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        //$emp = $_SESSION['adminid']?" and transfer_to='".$_SESSION['adminid']."'":" and transfer_by='".$_SESSION['adminid']."'";        
        
        $emp = $_SESSION['adminid'] ? " and (transfer_to='" . $_SESSION['adminid'] . "' ||  transfer_by='" . $_SESSION['adminid'] . "')" : '';
        
        //$uquery ="select * from (select * from fts_move_file order by id DESC ) fts_move_file where 1 $emp";    
        
        if ($_SESSION['utype'] == 'Admin') {
            $uquery = "select * from (select * from fts_move_file where 1 $date ORDER BY id DESC) AS x GROUP BY file_no";
            // $uquery = "select * from file_record GROUP BY file_no";
        } else {
            $uquery = "SELECT * FROM `file_record` WHERE `created_by`='".$_SESSION['adminid']."'";
            // $uquery = "select * from file_record GROUP BY file_no";
        }
        
        //$uquery ="select * from tender where 1";
        
        $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/show.php");
    }
    

    
    
    function addnew()
    {
        if ($_REQUEST['id']) {
            $query_com = "SELECT * FROM  file_record WHERE id=" . $_REQUEST['id'];
            // $query_com ="SELECT * FROM  fts_initiate_file WHERE id=".$_REQUEST['id'];
            $this->Query($query_com);
            $results = $this->fetchArray();
            require_once("views/" . $this->name . "/" . $this->task . ".php");
        } else {
            
            require_once("views/" . $this->name . "/" . $this->task . ".php");
        }
    }
    
    
    
    

    
    
    
function search(){

    $uid = $_REQUEST['uid'] ? " AND `uid` like '%" .$_REQUEST['uid'] . "%'" : '';
    $old_no = $_REQUEST['lot_no'] ? " AND `old_no` like '" .$_REQUEST['lot_no'] . "%'" : ''; // it's use for Property Id 
    // $old_no = $_REQUEST['old_no'] ? " AND `old_no` like '%" .$_REQUEST['old_no'] . "%'" : '';
    $sceme_name = $_REQUEST['sceme_name'] ? " AND `sceme_name` like '%" .$_REQUEST['sceme_name'] . "%'" : '';
    // $assigned_to = $_REQUEST['assigned_to'] ? " AND `move_to` ='" .$_REQUEST['assigned_to'] . "'" : '';
    $allotee = $_REQUEST['allotee'] ? " AND `allotee` LIKE '%" .trim($_REQUEST['allotee']) . "%'" : '';
    $new_no = $_REQUEST['new_no'] ? " AND `new_no` LIKE '%" .$_REQUEST['new_no'] . "%'" : '';
    // $department = $_REQUEST['department'] ? " AND `file_location` ='" .$_REQUEST['department'] . "'" : '';
    $department = $_REQUEST['department'] ? " AND `department_id` ='" .$_REQUEST['department'] . "'" : '';
    // $lot_no = $_REQUEST['lot_no'] ? " AND `lot_no` ='" .$_REQUEST['lot_no'] . "'" : '';
    $property_no = $_REQUEST['property_no'] ? " AND `property_no` ='" .$_REQUEST['property_no'] . "'" : '';
    $zone = $_REQUEST['zone'] ? " AND `zone` ='" .$_REQUEST['zone'] . "'" : '';
    $category = $_REQUEST['category'] ? " AND `category` LIKE '%" .$_REQUEST['category'] . "%'" : '';
    
    $assigned_to = $_REQUEST['assign_id']?' AND `id` IN (SELECT `record_id` FROM `track_file` WHERE `move_to`="'.$_REQUEST['assign_id'].'" GROUP BY `file_no` ORDER BY `id` DESC )':'';
     // $table = $assigned_to?' `track_file`':' `file_record` ';

     // $where = $assigned_to? ($assigned_to):($uid." ". $sceme_name." ". $old_no." ". $property_no." ". $category." ". $zone." ". $allotee." ". $new_no." ". $lot_no);
     $where =  ($uid." ". $sceme_name." ". $old_no." ". $property_no." ". $category." ". $zone." ". $allotee." ". $new_no." ". $lot_no);

    if($department || $uid){
        if($_SESSION['utype']=='Admin'){
        
          $uquery = "SELECT `id`,`uid`,`old_no`,`allotee`,`file_location` FROM `file_record` WHERE  1 $assigned_to $where $department ORDER BY `id` DESC"; 
        // $uquery = "SELECT * FROM $table WHERE 1 $where $department ORDER BY `id` DESC"; 
    }else {

         $uquery = "SELECT `id`,`uid`,`old_no`,`allotee`,`file_location` FROM `file_record` WHERE  1 $assigned_to $where $department ORDER BY `id` DESC"; 
        // $uquery = "SELECT * FROM $table WHERE 1 $where $department ORDER BY `id` DESC"; 
        // $uquery = "SELECT * FROM `file_record` WHERE `department_id`='".$_SESSION['department_id']."'  $sceme_name $old_no $uid $lot_no";        
    }


         $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
       $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
	  
        $this->Query($query);
        $results = $this->fetchArray();
   }
        require_once("views/" . $this->name . "/" . $this->task . ".php");

}    
    
    
    
function report(){

    $priority = $_REQUEST['add_priority'] ? " AND `priority` ='" .$_REQUEST['add_priority'] . "'" : '';
    $remark = $_REQUEST['remark'] ? " AND `remark` like '%" .$_REQUEST['remark'] . "%'" : '';
    $owner = $_REQUEST['owner'] ? " AND `owner` LIKE '%" .$_REQUEST['owner'] . "%'" : '';
    $department = $_REQUEST['department'] ? " AND `department_id` ='" .$_REQUEST['department'] . "'" : '';
    $lot_no = $_REQUEST['lot_no'] ? " AND `lot_no` ='" .$_REQUEST['lot_no'] . "'" : '';

    $days =  date('Y-m-d', strtotime('-'.($_REQUEST['age']-1).' days', strtotime(date('Y-m-d'))));
    $age = $_REQUEST['age'] ? " AND `return_date` IS NULL AND `move_date` <'" .$days . "'" : '';


        $table = $owner?' `file_record` ':' `track_file` ';
        $order = $owner?' `uid` ':' `file_no` ';
        $where = $owner?( $owner ):( $priority." ". $age." ". $remark." ". $assigned_to ." ".$department." ". $lot_no );

if($_REQUEST['search']){
    if($_SESSION['utype']=='Admin'){
    
        $uquery = "SELECT * FROM $table WHERE 1 $where  GROUP BY $order ORDER BY `id` DESC"; 
    }else {
        
        // $uquery = "SELECT * FROM `file_record` WHERE 1  $sceme_name $remark $priority $lot_no";   
         $uquery = "SELECT * FROM $table WHERE 1 $where GROUP BY $order ORDER BY `id` DESC";      
       // echo  $uquery = "SELECT * FROM $table WHERE 1 $priority $age $remark $assigned_to $owner $department $lot_no GROUP BY $order ORDER BY `id` DESC";      
    }

         $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
    }
        require_once("views/" . $this->name . "/" . $this->task . ".php");

}   
    
function file_log(){
        $dateFrom = $_REQUEST['from_date'] ? " and date_created LIKE '" . $_REQUEST['from_date'] . "%'" : '';
        $to_date =  date('Y-m-d', strtotime('-'.($_REQUEST['age']-1).' days', strtotime($_REQUEST['to_date'])));
        $dateTo   = $_REQUEST['to_date'] ? " and date_created LIKE '" . $_REQUEST['to_date'] . "%'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date_created between '" . $_REQUEST['from_date'] . "' and '" . $to_date . "'" : $bydate;

        $uquery = "SELECT * FROM `activity_log` WHERE 1 $date ORDER BY `id` DESC ";      


         $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
    
        require_once("views/" . $this->name . "/" . $this->task . ".php");
}    
        
function server_log(){
        $dateFrom = $_REQUEST['from_date'] ? " and activity_date LIKE '" . $_REQUEST['from_date'] . "%'" : '';
        $to_date =  date('Y-m-d', strtotime('-'.($_REQUEST['age']-1).' days', strtotime($_REQUEST['to_date'])));
        $dateTo   = $_REQUEST['to_date'] ? " and activity_date LIKE '" . $_REQUEST['to_date'] . "%'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and activity_date between '" . $_REQUEST['from_date'] . "' and '" . $to_date . "'" : $bydate;

        $uquery = "SELECT * FROM `server_log` WHERE 1 $date ORDER BY `id` DESC ";      


         $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
    
        require_once("views/" . $this->name . "/" . $this->task . ".php");
}    
    
    
    function figer_print(){        

        $uquery = "SELECT * FROM `users` WHERE `finger_print`!='1' AND `utype`='Employee' AND `status`=1 ORDER BY `id` DESC ";      


         $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
    
        require_once("views/" . $this->name . "/" . $this->task . ".php");
}   
    
    function finger_update_save()
        {
        $id = $_REQUEST['id'];
        $fingre_id = $_REQUEST['fingre_id'];
        $hd_quality = $_REQUEST['hd_quality'];
        $hd_base64iostemp = $_REQUEST['hd_base64iostemp'];
        $fingre_id = $_REQUEST['fingre_id'];
        $name = $_REQUEST['name'];
        $finger_print = $_REQUEST['finger_print'];
         $query = "update users set quality='" . $hd_quality . "', base64iostemp='" . $hd_base64iostemp . "', finger_print='" . $finger_print . "' WHERE id='" . $id . "'";
        $this->Query($query);
        //$useer_detail = mysql_fetch_array(mysql_query("select * from users where id = '".$id."'"));
        
        //$activity = $useer_detail['username']."Finger update by".$_SESSION['username'];       
                        //$log =mysql_query("INSERT INTO `server_log`( `system_ip`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");
        if ($this->Execute())
            {
            $_SESSION['error'] = "Finger Print Updated Successfully";
            $_SESSION['errorclass'] = ERRORCLASS;
            }

        $this->show();
        }


    
}
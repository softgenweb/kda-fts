<?php

require_once('dbaccess.php');
require_once('textconfig/config.php');


if (file_exists('configuration.php')) {
    
    require_once('configuration.php');
}


class ftsClass extends DbAccess
{
    public $view = '';
    public $name = 'fts';
    
    
    
    /***************************************************** POST START **********************************************************/
    
    
    
    function show()
    {
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        $dateTo   = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        //$emp = $_SESSION['adminid']?" and transfer_to='".$_SESSION['adminid']."'":" and transfer_by='".$_SESSION['adminid']."'";        
        
        $emp = $_SESSION['adminid'] ? " and (transfer_to='" . $_SESSION['adminid'] . "' ||  transfer_by='" . $_SESSION['adminid'] . "')" : '';
        
        //$uquery ="select * from (select * from fts_move_file order by id DESC ) fts_move_file where 1 $emp";    
        
        if ($_SESSION['utype'] == 'Admin') {
            $uquery = "select * from (select * from fts_move_file where 1 $date ORDER BY id DESC) AS x GROUP BY file_no";
            // $uquery = "select * from file_record GROUP BY file_no";
        } else {
            $uquery = "select * from (select * from fts_move_file where 1 $emp $date ORDER BY id DESC) AS x GROUP BY file_no";
            // $uquery = "select * from file_record GROUP BY file_no";
        }
        
        //$uquery ="select * from tender where 1";
        
        $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/show.php");
    }
    
    
    
    
    
    
  /*  "UPDATE `plot_booking` SET `referred_by`='',`referred_user_id`='',`commission`='',`commission_amount`='' WHERE `referred_by`='dealer'";*/
    
    
    
    
    function addnew()
    {
        if ($_REQUEST['id']) {
            $query_com = "SELECT * FROM  file_record WHERE id=" . $_REQUEST['id'];
            // $query_com ="SELECT * FROM  fts_initiate_file WHERE id=".$_REQUEST['id'];
            $this->Query($query_com);
            $results = $this->fetchArray();
            require_once("views/" . $this->name . "/" . $this->task . ".php");
        } else {
            
            
            require_once("views/" . $this->name . "/" . $this->task . ".php");
        }
    }
    
    
    
    
    
    
    
    function save()
    {
        
        $old_no      = mysql_real_escape_string($_REQUEST['old_no']);
        $new_no      = mysql_real_escape_string($_REQUEST['new_no']);
        $file_name   = mysql_real_escape_string($_REQUEST['file_name']);
        $subject     = mysql_real_escape_string($_REQUEST['subject']);
        $description = mysql_real_escape_string($_REQUEST['file_desc']);
        $owner       = mysql_real_escape_string($_REQUEST['owner']);
        $created_by  = $_SESSION['adminid'];
        
        
        
        if (!$_REQUEST['id']) {
            
            $query = "INSERT INTO `file_record`(`old_no`, `new_no`, `file_name`, `subject`, `description`, `owner`, `created_by`, `created_date`, `modified_by`, `modify_date`) VALUES ('" . $old_no . "','" . $new_no . "','" . $file_name . "','" . $subject . "','" . $description . "','" . $owner . "','" . $created_by . "','" . date('Y-m-d H:i:s') . "','" . $created_by . "','" . date('Y-m-d H:i:s') . "')";
            
            // exit();
            $ece    = mysql_query($query);
            $id     = mysql_insert_id();
            $uid    = $_SESSION['fyear'] . $id;
            $update = mysql_query("UPDATE `file_record` set `uid`='" . $uid . "' where id='" . $id . "'");
            
            // $move= mysql_query("INSERT INTO `fts_move_file`(`initiate_file_id`, `file_no`, `remark`, `initiate_email`, `actiontaken`, `transfer_by`, `transfer_to`, `initiate_by_action`, `date`, `datetime`, `status`, `byself`) VALUES ('".$id."', '".$file_no."', '".$subject."', '".$initiate_email."', '".$actiontaken."', '".$initiate_id."', '".$emp_sendto."', '".$initiate_by_action."', '".$date."', '".$datetime."', '".$status."', '1')");
            
            
            $_SESSION['error']      = ADDNEWRECORD;
            $_SESSION['errorclass'] = ERRORCLASS;
            header("location:index.php?control=fts");
            
        } else {
            $id = $_REQUEST['id'];
            
            $update = "UPDATE `file_record` SET `old_no`='" . $old_no . "',`new_no`='" . $new_no . "',`file_name`='" . $file_name . "',`subject`='" . $subject . "',`description`='" . $description . "',`owner`='" . $owner . "',`modified_by`='" . $created_by . "',`modify_date`='" . date('Y-m-d H:i:s') . "' WHERE id='" . $id . "'";
            
            $exe_update = mysql_query($update);
            
            // $update1 = mysql_query("update fts_move_file set  remark='".$subject."', initiate_email='".$initiate_email."', transfer_by='".$initiate_id."', transfer_to='".$emp_sendto."', actiontaken='".$actiontaken."', initiate_by_action='".$initiate_by_action."', date='".$date."', datetime='".$datetime."' where initiate_file_id='".$id."' and byself='1'");
            
            $_SESSION['error']      = UPDATERECORD;
            $_SESSION['errorclass'] = ERRORCLASS;
            
            // $this->show();
            header("location:index.php?control=fts");
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    
    function status()
    {
        
        $res = mysql_fetch_array(mysql_query("SELECT file_no FROM  fts_initiate_file WHERE id='" . $_REQUEST['id'] . "'"));
        
        $query = mysql_query("update fts_initiate_file set status=" . $_REQUEST['status'] . " WHERE file_no='" . $res['file_no'] . "'");
        
        $query1 = mysql_query("update fts_move_file set status=" . $_REQUEST['status'] . " WHERE file_no='" . $res['file_no'] . "'");
        
        
        
        $this->task = "show";
        
        $this->view = 'show';
        
        
        
        
        
        $_SESSION['error'] = STATUS;
        
        $_SESSION['errorclass'] = ERRORCLASS;
        
        $this->show();
        
        //header("location:index.php?control=fts");
        
    }
    
    
    
    function delete()
    {
        
        
        
        $query = "DELETE FROM fts_initiate_file WHERE id in (" . $_REQUEST['id'] . ")";
        
        $this->Query($query);
        
        $this->Execute();
        
        //$this->task="show";
        
        //$this->view ='show';    
        
        
        
        $_SESSION['error'] = DELETE;
        
        $_SESSION['errorclass'] = ERRORCLASS;
        
        $this->show();
        
        //header("location:index.php?control=tender&task=show");
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    function fts_detail()
    {
        
        $file_no = $_REQUEST['file_no'];
        
        $query_com = "SELECT * FROM  fts_move_file WHERE file_no='" . $_REQUEST['file_no'] . "'";
        
        $this->Query($query_com);
        
        
        
        $results = $this->fetchArray();
        
        require_once("views/" . $this->name . "/" . $this->task . ".php");
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    function move_addnew()
    {
        
        $file_no = $_REQUEST['file_no'];
        
        if ($file_no) {
            
            $query_com = "SELECT * FROM  fts_move_file WHERE file_no='" . $_REQUEST['file_no'] . "'";
            
            $this->Query($query_com);
            
            
            
            $results = $this->fetchArray();
            
            require_once("views/" . $this->name . "/" . $this->task . ".php");
            
        }
        
        
        
    }
    
    
    
    
    
    function move_edit()
    {
        
        $id = $_REQUEST['id'];
        
        if ($id) {
            
            $query_com = "SELECT * FROM  fts_move_file WHERE id='" . $_REQUEST['id'] . "'";
            
            $this->Query($query_com);
            
            
            
            $results = $this->fetchArray();
            
            require_once("views/" . $this->name . "/" . $this->task . ".php");
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    function move_save()
    {
        
        
        
        $file_no          = $_REQUEST['file_no'];
        $initiate_file_id = $_REQUEST['initiate_file_id'];
        
        $remark = $_POST['remark'];
        
        $initiate_by_action = $_REQUEST['initiate_by_action'] ? $_REQUEST['initiate_by_action'] : 'Pending';
        
        $initiate_email = $_REQUEST['initiate_email'];
        $transfer_by    = $_REQUEST['initiate_id'];
        
        $transfer_to = $_POST['emp_sendto'];
        $actiontaken = $_REQUEST['actiontaken'];
        
        $date     = date('d-m-Y');
        $datetime = date("h:i:sa");
        
        $status = 1;
        
        
        
        
        
        
        
        
        
        if (!$_REQUEST['id']) {
            
            
            
            $query = "INSERT INTO `fts_move_file`(`initiate_file_id`, `file_no`, `remark`, `initiate_email`, `transfer_by`, `transfer_to`, `actiontaken`, `initiate_by_action`, `date`, `datetime`, `status`) VALUES ('" . $initiate_file_id . "', '" . $file_no . "', '" . $remark . "','" . $initiate_email . "','" . $transfer_by . "','" . $transfer_to . "','" . $actiontaken . "','" . $initiate_by_action . "','" . $date . "','" . $datetime . "','" . $status . "')";
            
            
            
            $ece = mysql_query($query);
            
            
            
            $close = mysql_query("UPDATE `fts_initiate_file`  set emp_sendto='" . $transfer_to . "', actiontaken= '" . $actiontaken . "', `initiate_by_action`='" . $initiate_by_action . "', `close_date`='" . $date . "', `date`='" . $date . "', datetime='" . $datetime . "' where file_no='" . $_REQUEST['file_no'] . "'");
            
            
            
            $_SESSION['error'] = ADDNEWRECORD;
            
            $_SESSION['errorclass'] = ERRORCLASS;
            
            //header("location:index.php?control=fts");
            
            header("location:index.php?control=fts&task=fts_detail&file_no=$file_no");
            
        } else {
            
            $query = "UPDATE `fts_move_file`  set `remark`='" . $remark . "', `initiate_email`='" . $initiate_email . "', `transfer_by`='" . $transfer_by . "', `transfer_to`='" . $transfer_to . "', `actiontaken`='" . $actiontaken . "', `initiate_by_action`='" . $initiate_by_action . "', `date`='" . $date . "', `datetime`='" . $datetime . "' where id='" . $_REQUEST['id'] . "'";
            
            
            
            $ece = mysql_query($query);
            
            
            
            
            
            $close = mysql_query("UPDATE `fts_initiate_file`  set emp_sendto='" . $transfer_to . "', actiontaken= '" . $actiontaken . "', `initiate_by_action`='" . $initiate_by_action . "', `close_date`='" . $date . "', `date`='" . $date . "', datetime='" . $datetime . "' where file_no='" . $_REQUEST['file_no'] . "'");
            
            
            
            
            
            $_SESSION['error'] = UPDATERECORD;
            
            $_SESSION['errorclass'] = ERRORCLASS;
            
            //$this->show();
            
            header("location:index.php?control=fts&task=fts_detail&file_no=$file_no");
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    function pending()
    {
        
        
        
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        
        $dateTo = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        
        $bydate = $dateFrom ? $dateFrom : $dateTo;
        
        
        
        $date = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        
        
        
        
        
        $emp = $_SESSION['adminid'] ? " and emp_sendto='" . $_SESSION['adminid'] . "'" : '';
        
        
        
        if ($_SESSION['utype'] == 'Admin') {
            
            $uquery = "SELECT * FROM fts_initiate_file where initiate_by_action='Pending' $date ORDER BY id DESC";
            
        }
        
        else {
            
            $uquery = "SELECT * FROM fts_initiate_file where initiate_by_action='Pending' $emp $date ORDER BY id DESC";
            
        }
        
        
        
        //$uquery ="select * from tender where 1";
        
        $this->Query($uquery);
        
        $uresults = $this->fetchArray();
        
        $tdata = count($uresults);
        
        /* Paging start here */
        
        $page = intval($_REQUEST['page']);
        
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        
        $adjacents = intval($_REQUEST['adjacents']);
        
        $tdata = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        
        $tdata = floor($tdata);
        
        if ($page <= 0)
            $page = 1;
        
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        
        $this->Query($query);
        
        $results = $this->fetchArray();
        
        
        
        require_once("views/" . $this->name . "/pending.php");
        
    }
    
    
    
    
    
    function pending_desk()
    {
        
        
        
        
        
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        
        $dateTo = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        
        $bydate = $dateFrom ? $dateFrom : $dateTo;
        
        
        
        $date = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        
        
        
        
        
        $emp = $_SESSION['adminid'] ? " and emp_sendto='" . $_SESSION['adminid'] . "'" : '';
        
        
        
        if ($_SESSION['utype'] == 'Admin') {
            
            $uquery = "SELECT * FROM fts_initiate_file where initiate_by_action='Pending' $date ORDER BY id DESC";
            
        }
        
        else {
            
            $uquery = "SELECT * FROM fts_initiate_file where initiate_by_action='Pending' $emp $date ORDER BY id DESC";
            
        }
        
        
        
        //$uquery ="select * from tender where 1";
        
        $this->Query($uquery);
        
        $uresults = $this->fetchArray();
        
        $tdata = count($uresults);
        
        /* Paging start here */
        
        $page = intval($_REQUEST['page']);
        
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        
        $adjacents = intval($_REQUEST['adjacents']);
        
        $tdata = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        
        $tdata = floor($tdata);
        
        if ($page <= 0)
            $page = 1;
        
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        
        $this->Query($query);
        
        $results = $this->fetchArray();
        
        
        
        require_once("views/" . $this->name . "/pending_desk.php");
        
    }
    
    
    
    
    
    function reject()
    {
        
        
        
        $dateFrom = $_REQUEST['from_date'] ? " and fif.date ='" . $_REQUEST['from_date'] . "'" : '';
        
        $dateTo = $_REQUEST['to_date'] ? " and fif.date ='" . $_REQUEST['to_date'] . "'" : '';
        
        $bydate = $dateFrom ? $dateFrom : $dateTo;
        
        
        
        $date = ($dateFrom && $dateTo) ? " and fif.date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        
        
        
        
        
        //$emp = $_SESSION['adminid']?" and transfer_to='".$_SESSION['adminid']."'":" and transfer_by='".$_SESSION['adminid']."'";
        
        
        
        $emp = $_SESSION['adminid'] ? " and (fmf.transfer_to='" . $_SESSION['adminid'] . "' ||  fmf.transfer_by='" . $_SESSION['adminid'] . "')" : '';
        
        
        
        if ($_SESSION['utype'] == 'Admin') {
            
            $uquery = "SELECT fif.* FROM fts_initiate_file fif INNER JOIN fts_move_file fmf ON fif.initiate_by_action=fmf.initiate_by_action where fif.initiate_by_action='Reject' $date ORDER BY id DESC";
            
        }
        
        else {
            
            
            
            $uquery = "SELECT fif.* FROM fts_initiate_file fif INNER JOIN fts_move_file fmf ON fif.initiate_by_action=fmf.initiate_by_action where fif.initiate_by_action='Reject' $emp $date ORDER BY id DESC";
            
        }
        
        
        
        //$uquery ="select * from tender where 1";
        
        $this->Query($uquery);
        
        $uresults = $this->fetchArray();
        
        $tdata = count($uresults);
        
        /* Paging start here */
        
        $page = intval($_REQUEST['page']);
        
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        
        $adjacents = intval($_REQUEST['adjacents']);
        
        $tdata = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        
        $tdata = floor($tdata);
        
        if ($page <= 0)
            $page = 1;
        
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        
        $this->Query($query);
        
        $results = $this->fetchArray();
        
        
        
        require_once("views/" . $this->name . "/reject.php");
        
    }
    
    
    
    
    
    
    
    function close()
    {
        
        
        
        $dateFrom = $_REQUEST['from_date'] ? " and fif.date ='" . $_REQUEST['from_date'] . "'" : '';
        
        $dateTo = $_REQUEST['to_date'] ? " and fif.date ='" . $_REQUEST['to_date'] . "'" : '';
        
        $bydate = $dateFrom ? $dateFrom : $dateTo;
        
        
        
        $date = ($dateFrom && $dateTo) ? " and fif.date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        
        
        
        
        
        $emp = $_SESSION['adminid'] ? " and (fmf.transfer_to='" . $_SESSION['adminid'] . "' ||  fmf.transfer_by='" . $_SESSION['adminid'] . "')" : '';
        
        
        
        //$uquery ="select * from (select * from fts_move_file order by id DESC ) fts_move_file where 1 $emp";    
        
        if ($_SESSION['utype'] == 'Admin') {
            
            $uquery = "SELECT fif.* FROM fts_initiate_file fif INNER JOIN fts_move_file fmf ON fif.initiate_by_action=fmf.initiate_by_action where fif.initiate_by_action='Close' $date ORDER BY id DESC";
            
        }
        
        else {
            
            $uquery = "SELECT fif.* FROM fts_initiate_file fif INNER JOIN fts_move_file fmf ON fif.initiate_by_action=fmf.initiate_by_action where fif.initiate_by_action='Close' $emp $date ORDER BY id DESC";
            
        }
        
        
        
        //$uquery ="select * from tender where 1";
        
        $this->Query($uquery);        
        $uresults = $this->fetchArray();        
        $tdata = count($uresults);        
        /* Paging start here */        
        $page = intval($_REQUEST['page']);        
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default        
        $adjacents = intval($_REQUEST['adjacents']);        
        $tdata = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];//         
        $tdata = floor($tdata);        
        if ($page <= 0)
            $page = 1;        
        if ($adjacents <= 0)
           $tdata ? ($adjacents = 4) : 0;
        
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
        
        require_once("views/" . $this->name . "/close.php");
        
    }
    
    
    function search()
    {
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        $dateTo   = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        //$emp = $_SESSION['adminid']?" and transfer_to='".$_SESSION['adminid']."'":" and transfer_by='".$_SESSION['adminid']."'";        
        
        $emp = $_SESSION['adminid'] ? " and (transfer_to='" . $_SESSION['adminid'] . "' ||  transfer_by='" . $_SESSION['adminid'] . "')" : '';
        
        //$uquery ="select * from (select * from fts_move_file order by id DESC ) fts_move_file where 1 $emp";    
        
        if ($_SESSION['utype'] == 'Admin') {
            $uquery = "select * from (select * from fts_move_file where 1 $date ORDER BY id DESC) AS x GROUP BY file_no";
        } else {
            $uquery = "select * from (select * from fts_move_file where 1 $emp $date ORDER BY id DESC) AS x GROUP BY file_no";
        }
        
        //$uquery ="select * from tender where 1";
        
        $this->Query($uquery);        
        $uresults = $this->fetchArray();        
        $tdata = count($uresults);        
        /* Paging start here */        
        $page = intval($_REQUEST['page']);        
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default        
        $adjacents = intval($_REQUEST['adjacents']);        
        $tdata = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];//         
        $tdata = floor($tdata);        
        if ($page <= 0)
            $page = 1;        
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;        
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;        
        /* Paging end here */        
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;        
        $this->Query($query);        
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/search.php");
    }
    
    
    
    function report()
    {
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        $dateTo   = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        //$emp = $_SESSION['adminid']?" and transfer_to='".$_SESSION['adminid']."'":" and transfer_by='".$_SESSION['adminid']."'";        
        
        $emp = $_SESSION['adminid'] ? " and (transfer_to='" . $_SESSION['adminid'] . "' ||  transfer_by='" . $_SESSION['adminid'] . "')" : '';
        
        //$uquery ="select * from (select * from fts_move_file order by id DESC ) fts_move_file where 1 $emp";    
        
        if ($_SESSION['utype'] == 'Admin') {
            $uquery = "select * from (select * from fts_move_file where 1 $date ORDER BY id DESC) AS x GROUP BY file_no";
        } else {
            $uquery = "select * from (select * from fts_move_file where 1 $emp $date ORDER BY id DESC) AS x GROUP BY file_no";
        }
        
        //$uquery ="select * from tender where 1";        
        $this->Query($uquery);        
        $uresults = $this->fetchArray();        
        $tdata = count($uresults);        
        /* Paging start here */        
        $page = intval($_REQUEST['page']);        
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default        
        $adjacents = intval($_REQUEST['adjacents']);  
        $tdata = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        
        $tdata = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/report.php");
    }
    
    
    
    
    
    
    
    
}
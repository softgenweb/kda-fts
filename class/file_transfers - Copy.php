 <?php

require_once('dbaccess.php');
require_once('textconfig/config.php');


date_default_timezone_set("Asia/Kolkata");


if (file_exists('configuration.php')) {
    
    require_once('configuration.php');
}


class file_transferClass extends DbAccess
{
    public $view = '';
    public $name = 'file_transfer';
    
    /*Show all new files*/
    function show()
    {
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        $dateTo   = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        
        
        if ($_SESSION['department_id'] == 'Admin' || $_SESSION['department_id'] == '2') {
        // if ($_SESSION['utype'] == 'Admin' || $_SESSION['utype'] == 'Record Room') {
            $uquery = "SELECT * FROM `file_record` WHERE 1 $dateFrom $dateTo $bydate $date";
        } else {
            $uquery = "SELECT * FROM `file_record` WHERE `file_status`='1'  AND `department_id`='" . $_SESSION['department_id'] . "'";
            // $uquery = "SELECT * FROM `file_record` WHERE `file_status`='1' AND `file_location`='1' AND `department_id`='".$_SESSION['department_id']."'";
        }
        
        
        $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
        // require_once("views/" . $this->name . "/show.php");
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    }
    
    
    /*Add new file page*/
    function addnew()
    {
        
        if ($_REQUEST['id']) {
            $query_com = "SELECT * FROM  file_record WHERE id=" . $_REQUEST['id'];
            
            $this->Query($query_com);
            $results = $this->fetchArray();
            
            require_once("views/" . $this->name . "/" . $this->task . ".php");
        } else {
            require_once("views/" . $this->name . "/" . $this->task . ".php");
        }
    }
    
    
    /* Save new file*/
    function save()
    {
        
        $old_no      = mysql_real_escape_string($_REQUEST['old_no']);
        $new_no      = mysql_real_escape_string($_REQUEST['new_no']);
        // $subject     = mysql_real_escape_string($_REQUEST['subject']);
        // $description = mysql_real_escape_string($_REQUEST['file_desc']);
        // $owner       = mysql_real_escape_string($_REQUEST['owner']);
        $department  = $_REQUEST['department']?$_REQUEST['department']:$_SESSION['department_id'];
        $department_id  = mysql_real_escape_string($_REQUEST['department_id']);
     
        $uid1        = mysql_real_escape_string($_REQUEST['uid']);
        $old_assigned  = $_REQUEST['old_assigned'];

        /*=========New Change==========*/
        $sceme_name     =   mysql_real_escape_string($_REQUEST['sceme_name']);
        $lot_no        =    mysql_real_escape_string($_REQUEST['lot_no']);
        $allotee        =   mysql_real_escape_string($_REQUEST['allotee']);
        $zone        =      mysql_real_escape_string($_REQUEST['zone']);
        $property_no   =    mysql_real_escape_string($_REQUEST['property_no']);
        $category        =  mysql_real_escape_string($_REQUEST['category']);
        $name        =      mysql_real_escape_string($_REQUEST['name']);
        $file_name        = mysql_real_escape_string($_REQUEST['file_name']);
        $contract_name   =  mysql_real_escape_string($_REQUEST['contract_name']);
        $work_name        = mysql_real_escape_string($_REQUEST['work_name']);
        $mb_num        =    mysql_real_escape_string($_REQUEST['mb_num']);
        $address        =   mysql_real_escape_string($_REQUEST['address']);
        $detail        =    mysql_real_escape_string($_REQUEST['detail']);
        $pentition_id   =   mysql_real_escape_string($_REQUEST['pentition_id']);
        $court_name      =  mysql_real_escape_string($_REQUEST['court_name']);
        $related_depart  =  mysql_real_escape_string($_REQUEST['related_depart']);
        $advocate_name   =  mysql_real_escape_string($_REQUEST['advocate_name']);
        $pensioner_name  =  mysql_real_escape_string($_REQUEST['pensioner_name']);
        $property_detail =  mysql_real_escape_string($_REQUEST['property_detail']);
        $year       =       mysql_real_escape_string($_REQUEST['year']);
        $section_name=      mysql_real_escape_string($_REQUEST['section_name']);
        $file_type   =      mysql_real_escape_string($_REQUEST['file_type']);
        $father_name  =     mysql_real_escape_string($_REQUEST['father_name']);
        $department_name =  mysql_real_escape_string($_REQUEST['department_name']);
        $post        =      mysql_real_escape_string($_REQUEST['post']);
        $dob        =       mysql_real_escape_string($_REQUEST['dob']);
        $appointment_date =  mysql_real_escape_string($_REQUEST['appointment_date']);
        $retirement_date =   mysql_real_escape_string($_REQUEST['retirement_date']);
        $family_pensioner =  mysql_real_escape_string($_REQUEST['family_pensioner']);
        $relation =         mysql_real_escape_string($_REQUEST['relation']);
        $priority =      mysql_real_escape_string($_REQUEST['add_priority']);
        $file_rack_location =      mysql_real_escape_string($_REQUEST['file_rack_location']);
        $empl_list =     $_REQUEST['empl_list']?$_REQUEST['empl_list']:$_SESSION['adminid'];

        
        
        if ($_SESSION['department_id'] == 'Admin' || $_SESSION['department_id'] == '2') {
        // if ($_SESSION['utype'] == 'Admin' || $_SESSION['utype'] == 'Record Room') {
            $dispatched_to = mysql_real_escape_string($_REQUEST['assigned_to']);
            $move_type     = "2";
            
        } else {
            $assigned_to = mysql_real_escape_string($_REQUEST['assigned_to']);
            $move_type   = "1";
        }
        $created_by = $_SESSION['adminid'];
        
        if (!$_REQUEST['id']) {
            
           $query = "INSERT INTO `file_record`(`old_no`, `new_no`, `file_name`, `zone`, `category`, `property_no`, `sceme_name`, `allotee`, `subject`, `description`, `owner`, `lot_no`, `department_id`, `name`, `contract_name`, `work_name`, `mb_num`, `address`, `detail`, `pentition_id`, `court_name`, `related_depart`, `advocate_name`, `pensioner_name`, `property_detail`, `year`, `section_name`, `file_type`, `father_name`, `department_name`, `post`, `dob`, `appointment_date`, `retirement_date`, `family_pensioner`, `relation`, `assigned_to`, `dispatched_to`, `priority`, `file_location`, `created_by`, `created_date`, `modified_by`, `modify_date`,file_rack_location) VALUES ('".$old_no."', '".$new_no."', '".$file_name."', '".$zone."', '".$category."', '".$property_no."', '".$sceme_name."', '".$allotee."', '".$subject."', '".$description."', '".$owner."', '".$lot_no."', '".$department."', '".$name."', '".$contract_name."', '".$work_name."', '".$mb_num."', '".$address."', '".$detail."', '".$pentition_id."', '".$court_name."', '".$related_depart."', '".$advocate_name."', '".$pensioner_name."', '".$property_detail."', '".$year."', '".$section_name."', '".$file_type."', '".$father_name."', '".$department_name."', '".$post."', '".$dob."', '".$appointment_date."', '".$retirement_date."', '".$family_pensioner."', '".$relation."', '".$assigned_to."', '".$dispatched_to."', '".$priority."', '".$department."','" . $created_by . "','" . date('Y-m-d H:i:s') . "','" . $created_by . "','" . date('Y-m-d H:i:s') . "', '".$file_rack_location."')";
            // 
            $ece     = mysql_query($query);
            $id      = mysql_insert_id();           
            
        
            $key = $this->departmentNameKey($department);   
            $number_of_digits = $this->count_digit($id);    
            
           $uid     = $key . $_SESSION['fyear'] .$number_of_digits. $id;
           $link_UID  = "('".$uid."')";
        
            $update  = mysql_query("UPDATE `file_record` set `uid`='" . $uid . "' where id='" . $id . "'");
            $move_to = $dispatched_to ? $dispatched_to : $assigned_to;
            
            // if($_SESSION['department_id']=="2"){
            $track = mysql_query("INSERT INTO `track_file`(`record_id`, `file_no`, `created_by`, `move_from`, `from_depart`, `move_to`, `move_type`, `to_depart`, `department_id`, `priority`, `move_date`, `file_status`) VALUES ('" . $id . "', '" . $uid . "', '" . $created_by . "', '" . $created_by . "', '" . $_SESSION['department_id'] . "', '" . $empl_list . "', '" . $move_type . "', '" . $department . "', '" . $department . "', '" . $priority . "', '" . date('Y-m-d H:i:s') . "', '1')"); 
            if($_SESSION['department_id']=="2"){
            /*=======Activity=======*/
            // 1. Craeted  
            $activity = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','New file Created','1','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','0 days 00:00','" . date('Y-m-d H:i:s') . "')");
            // $activity = mysql_query("INSERT INTO `activity_log`( `user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','New file Created','1','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','" . $department . "','" .$empl_list. "','0 days 00:00','" . date('Y-m-d H:i:s') . "')");

            $lst_act = mysql_fetch_array(mysql_query("SELECT `date_created` FROM `activity_log` WHERE `file_no`='" . $uid . "' ORDER BY `id` DESC LIMIT 1"));
            
            $datetime1 = new DateTime();
            $datetime2 = new DateTime(substr($lst_act['date_created'], 0, 16));
            $interval  = $datetime1->diff($datetime2);
            $elapsed   = $interval->format('%a days %H:%I');

            //3. Assign, 
            $activity1 = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','Assigning File','3','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','" . $department . "','" . $empl_list . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "')");
         
        }else{

            $activity = mysql_query("INSERT INTO `activity_log`( `user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','New file Created','1','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','" . $department . "','" .$empl_list. "','0 days 00:00','" . date('Y-m-d H:i:s') . "')"); 
        }
            /*===========Gsingh==============*/
            if($old_assigned=="1"){

              if($_SESSION['department_id']=='2'){
                mysql_query("UPDATE `track_file` SET `file_status`='0', `file_location`='".$_SESSION['department_id']."', `return_date`='" . date('Y-m-d H:i:s') . "' WHERE `file_no` = '" . $uid . "'");
            }/*else{
                mysql_query("UPDATE `track_file` SET `file_status`='3', `file_location`='".$_SESSION['department_id']."' WHERE `file_no` = '" . $uid . "'");
            }*/
                mysql_query("UPDATE `file_record` SET `file_status`='3', `file_location`='".$_SESSION['department_id']."', `close_date`='" . date('Y-m-d H:i:s') . "', `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."' WHERE uid = '" . $uid . "'");
            
            
            /*=======Activity=======*/
            $lst_act = mysql_fetch_array(mysql_query("SELECT * FROM `activity_log` WHERE `file_no`='" . $uid . "' ORDER BY `id` DESC LIMIT 1"));
            
            $datetime1 = new DateTime();
            $datetime2 = new DateTime(substr($lst_act['date_created'], 0, 16));
            $interval  = $datetime1->diff($datetime2);
            $elapsed   = $interval->format('%a days %H:%I');
          
            // 1. Craeted, 2. Edited, 3. Assign, 4. dispatch, 5. receive back, 6. Accept, 7. Rejected
            $activity  = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','Receive Assigned File','5','" . $lst_act['to_depart'] . "','" . $lst_act['to_user'] . "','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "')");
               
            }
            /*=============================*/
            if($_SESSION['department_id']=='2'){
           
            $_SESSION['errorclass'] = ERRORCLASS;
            if($old_assigned=="1"){
                $_SESSION['error'] = '<b>File has been Sucessfully Received.</b><br> Do you want print  <a class="btn btn-primary" target="_blank"  href="script/print/receive_print.php?empl='.$empl_list.'&uid='.$link_UID.'" style="padding: 2px;" onclick="hidePopupbox()">Yes</a>';
               header("location:index.php?control=file_transfer&task=received&rr_files=1");
            }else{
                $_SESSION['error']   = '<b>File has been Sucessfully Assign</b>.<br> Do you want print   <a class="btn btn-primary" target="_blank" href="script/print/assign_print.php?depart='.$department.'&empl='.$empl_list.'&uid='.$link_UID.'"  style="padding: 2px;"  onclick="hidePopupbox()">Yes</a>';
                header("location:index.php?control=file_transfer&task=assigned&addFile=1");
            }
            }else if($_SESSION['department_id']!='Admin'){
            // if($_SESSION['utype']!='Admin'){
            $_SESSION['error']      = ADDNEWRECORD;
            $_SESSION['errorclass'] = ERRORCLASS;
            $_SESSION['check'] = "1";
               header("location:index.php?control=file_transfer&task=received");
            }else{
            $_SESSION['error']      = ADDNEWRECORD;
            $_SESSION['errorclass'] = ERRORCLASS;
               header("location:index.php?control=file_transfer&task=addnew");
            }
          
            
        } else {
            
            $id = $_REQUEST['id'];
            
            $update     = "UPDATE `file_record` SET `old_no`='" . $old_no . "',`new_no`='" . $new_no . "',`file_name`='" . $file_name . "',`zone`='" . $zone . "',`category`='" . $category . "',`property_no`='" . $property_no . "',`sceme_name`='" . $sceme_name . "',`allotee`='" . $allotee . "',`lot_no`='" . $lot_no . "',`subject`='" . $subject . "',`description`='" . $description . "',`priority`='" . $priority . "',`owner`='" . $owner . "',`department_id`='" . $department . "',`name`='" . $name . "',`contract_name`='" . $contract_name . "',`work_name`='" . $work_name . "',`mb_num`='" . $mb_num . "',`address`='" . $address . "',`detail`='" . $detail . "',`pentition_id`='" . $pentition_id . "',`court_name`='" . $court_name . "',`related_depart`='" . $related_depart . "',`advocate_name`='" . $advocate_name . "',`pensioner_name`='" . $pensioner_name . "',`property_detail`='" . $property_detail . "',`year`='" . $year . "',`section_name`='" . $section_name . "',`file_type`='" . $file_type . "',`father_name`='" . $father_name . "',`department_name`='" . $department_name . "',`post`='" . $post . "',`dob`='" . $dob . "',`appointment_date`='" . $appointment_date . "',`retirement_date`='" . $retirement_date . "',`family_pensioner`='" . $family_pensioner . "',`relation`='" . $relation . "',`modified_by`='" . $created_by . "',`modify_date`='" . date('Y-m-d H:i:s') . "', `file_rack_location`='".$file_rack_location."' WHERE id='" . $id . "'";
            // exit;
            $exe_update = mysql_query($update);
            
            // $track = mysql_query("UPDATE `file_record` SET `move_from`='" . $move_from . "', `move_to`='" . $move_to . "', `move_type`='" . $move_type . "', `department_id`='" . $department_id . "', `priority`='" . $priority . "' WHERE `record_id`='" . $id . "'");
             // echo "INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid1 . "','Edit File','2','" . $department . "','" . $_SESSION['adminid'] . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "')"; exit;
            /*=======Activity=======*/
          // echo  "SELECT `date_created` FROM `activity_log` WHERE `file_no`='" . $uid1 . "' ORDER BY `id` DESC LIMIT 1"; exit;
            $lst_act = mysql_fetch_array(mysql_query("SELECT `date_created` FROM `activity_log` WHERE `file_no`='" . $uid1 . "' ORDER BY `id` DESC LIMIT 1"));
            //$lst_act['date_created'];
            $datetime1 = new DateTime();
            $datetime2 = new DateTime(substr($lst_act['date_created'], 0, 16));
            $interval  = $datetime1->diff($datetime2);
            $elapsed   = $interval->format('%a days %H:%I'); //exit;
            // 1. Created, 2. Edited, 3. Assign, 4. dispatch, 5. receive back, 6. Accept, 7. Rejected
            
            $activity = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid1 . "','Edit File','2','" . $department . "','" . $_SESSION['adminid'] . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "')");

            
            $_SESSION['error']      = UPDATERECORD;
            $_SESSION['errorclass'] = ERRORCLASS;
            
            header("location:index.php?control=file_transfer&task=addnew");
        }
        
    }
    
    
    function file_detail()
    {
        /*VIew file details*/
        $fid = $_REQUEST['fid'];
        
        $query = "SELECT * FROM `file_record` WHERE `id`='" . $fid . "' AND `status`!= 0";
        
        $this->Query($query);
        $results = $this->fetchArray();
        
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    }
    
    function tobe_receive()
    {
        
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        $dateTo   = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        
        // $emp = $_SESSION['adminid'] ? " and (transfer_to='" . $_SESSION['adminid'] . "' ||  transfer_by='" . $_SESSION['adminid'] . "')" : '';
        $depart_id = $_SESSION['department_id'];
        
        if ($_SESSION['department_id'] == 'Admin') {
        // if ($_SESSION['utype'] == 'Admin') {
            $uquery = "SELECT * FROM `file_record` WHERE 1 $dateFrom $dateTo $bydate $date";
            // $uquery = "SELECT * FROM `file_record` WHERE `uid` in  (SELECT `uid` FROM `file_move` WHERE  `assigned_to`!='' $dateFrom $dateTo $bydate $date)";
        } else {
            $uquery = "SELECT * FROM `track_file` WHERE `move_type`='2' AND `to_depart`='" . $_SESSION['department_id'] . "' AND `action`='1'";
            // $uquery = "SELECT * FROM `track_file` WHERE `move_type`='2' AND `move_to`='" . $_SESSION['adminid'] . "' AND `action`='1'";
        }

        $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/" . $this->task . ".php");        
    }
    
    
    function file_action()
    {
        /*Accept/reject file to be received*/
        $files  = $_REQUEST['file_ids'];
        $status = $_REQUEST['action'];
        
        $msg     = ($status == '2' ? 'Accepted (Received)' : 'Rejected');
        $action  = ($status == '2' ? '6' : '7');
        $file_location  = ($status == '2' ? " ,`file_location`='".$_SESSION['department_id']."'" : '');
        $view    = $_REQUEST['view'];
        $remark  = mysql_real_escape_string($_REQUEST['remark']);
        $file_id = implode(',', $files);

        $link_ID = "'".implode("','", $files)."'";

        
        
        $query = mysql_query("UPDATE `track_file` SET `action`='".$status."', `remark`='".$remark."' WHERE `id` in (".$link_ID.") AND `department_id`='".$_SESSION['department_id']."' AND `move_type`='2'");
        for ($i = 0; $i < count($files); $i++) {
            
            /*=======Activity=======*/
    
         $query = mysql_query("UPDATE `track_file` SET `action`='" . $status . "', `file_location`='" . $_SESSION['department_id'] . "', `remark`='" . $remark . "' WHERE `file_no`='" . $files[$i] . "' AND `department_id`='" . $_SESSION['department_id'] . "' AND `move_type`='2'");
            // echo "UPDATE `file_record` SET `file_status`='1' $file_location WHERE `uid`='".$files[$i]."' AND `file_status`='1'";

            $query1 = mysql_query("UPDATE `file_record` SET `file_status`='1', `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."' $file_location WHERE `uid`='".$files[$i]."' ");
            
            
            
            $lst_act   = mysql_fetch_array(mysql_query("SELECT * FROM `activity_log` WHERE `file_no`='".$files[$i]. "' ORDER BY `id` DESC LIMIT 1"));
            //echo "SELECT * FROM `activity_log` WHERE `file_no`='".$files[$i]."' ORDER BY `id` DESC LIMIT 1";
            $datetime1 = new DateTime();
            $datetime2 = new DateTime(substr($lst_act['date_created'], 0, 16));
            $interval  = $datetime1->diff($datetime2);
            $elapsed   = $interval->format('%a days %H:%I');
            
            
            if ($action == '6') {
                $sql = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $files[$i] . "','" . $msg . "','" . $action . "','" . $lst_act['from_depart'] . "','" . $lst_act['from_user'] . "','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "');");

                $_SESSION['error']   = '<b>File Sucessfully '.$msg.'</b>.<br> Do you want print <a class="btn btn-primary" target="_blank" href="script/print/to_be_received_print.php?file_id='.$link_ID.'&last_depart='.$lst_act['from_depart'].'&from_user='.$lst_act['from_user'].'&to_depart='.$_SESSION['department_id'].'&to_user='.$_SESSION['adminid'].'"  style="padding: 2px;" onclick="hidePopupbox()">Yes</a>';
                $_SESSION['action'] = 'accept';
            } else {
                $sql = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $files[$i] . "','" . $msg . "','" . $action . "','" . $lst_act['from_depart'] . "','" . $lst_act['from_user'] . "','" . $lst_act['from_depart'] . "','" . $lst_act['from_user'] . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "');");

                    $_SESSION['error']   = '<b>File Sucessfully '.$msg.'</b>';
                    $_SESSION['action'] = 'reject';
            }
            
            
            // $activity = mysql_query($sql);
        }

        //$_SESSION['error']      = "File Sucessfully " . $msg;
        $activity = "Files ".$msg." by ".($this->userName($_SESSION['adminid']))." (".($this->userDepartment($_SESSION['adminid'])).")"; 
        $log =mysql_query("INSERT INTO `server_log`( `system_ip`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");

        
        $_SESSION['errorclass'] = ERRORCLASS;
        header("location:index.php?control=file_transfer&task=tobe_receive");
    }
    
    
    function received()
    {
        /*Received/accepted file list*/
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        $dateTo   = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;

        $rr_files = $_REQUEST['rr_files']?" AND `file_location`='2'":"";
        
        // $emp = $_SESSION['adminid'] ? " and (transfer_to='" . $_SESSION['adminid'] . "' ||  transfer_by='" . $_SESSION['adminid'] . "')" : '';
        $depart_id = $_SESSION['department_id'];
        $days_ago  = date('Y-m-d', strtotime('-15 days', strtotime(date('Y-m-d'))));
        
        if ($_SESSION['utype'] == 'Admin' || $_SESSION['utype'] == 'Super-Admin' ) {
        // if ($_SESSION['utype'] == 'Admin') {
           $uquery = "SELECT * FROM `file_record` WHERE `file_status`='1' $dateFrom $dateTo $bydate $date";
        } else if ($_SESSION['department_id'] == '2') {
             $uquery = "SELECT * FROM `file_record` WHERE `file_location`!='1' $rr_files $dateFrom $dateTo $bydate $date ORDER BY `modify_date` DESC";
        } else {
              $uquery = "SELECT `uid` FROM `file_record` WHERE `file_location`='".$_SESSION['department_id']."'  AND `file_status`!=2 AND `uid` NOT IN (SELECT `file_no` FROM `track_file` WHERE `move_type`='1' AND `file_status`!='3' AND `file_location`='" . $_SESSION['department_id'] . "' AND `move_to`!='".$_SESSION['adminid']."') ORDER BY `modify_date` DESC ";
            // echo  $uquery = "SELECT `uid` FROM `file_record` WHERE `file_location`='".$_SESSION['department_id']."'  AND `file_status`!=2  ORDER BY `modify_date` DESC ";
        }
        
        $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
         $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    }
    
    
    
    function assign()
    {
        
        $files   = $_REQUEST['file_ids'];
        $file_id = implode(',', $files)?implode(',', $files):$files;
        
        $uquery = "SELECT * FROM `file_record` WHERE  `id` in (" . $file_id . ")";
        // $uquery = "SELECT *,(SELECT `id` FROM `file_move` FM WHERE FM.`uid` = FR.`uid` AND `file_status`='1') as mfid FROM `file_record` FR WHERE  `id` in (".$file_id.")";
        $this->Query($uquery);
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    }  

    function self_assign()
    {
        
        $file_id   = $_REQUEST['fid'];
        $uid   = $_REQUEST['uid'];
        // $file_id = implode(',', $files);
        
        $uquery = mysql_query("UPDATE `file_record` SET `file_location`='".$_SESSION['department_id']."', `modified_by`='".$_SESSION['adminid']."', `modify_date`='".date('Y-m-d H:i:s')."' WHERE `id`='".$file_id."'");

   // 1. Craeted, 2. Edited, 3. Assign, 4. dispatch, 5. receive back, 6. Accept, 7. Rejected
        $activity = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','Self Assigning File','0','1','0','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','0 days 00:00','" . date('Y-m-d H:i:s') . "')");
 
        $rr_files = ($_SESSION['department_id']=="2")?("&rr_files=1"):"";
        header('location:index.php?control=file_transfer&task=received'.$rr_files);
    }
    
    
    function assigned()
    {
        /*Assigned file list*/
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        $dateTo   = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        
        // $emp = $_SESSION['adminid'] ? " and (transfer_to='" . $_SESSION['adminid'] . "' ||  transfer_by='" . $_SESSION['adminid'] . "')" : '';
        $depart_id = $_SESSION['department_id'];
        
        if ($_SESSION['department_id'] == 'Admin') {
        // if ($_SESSION['utype'] == 'Admin') {
            $uquery = "SELECT * FROM `file_record` WHERE 1 $dateFrom $dateTo $bydate $date";
     
        } elseif ($_SESSION['department_id'] == '2') {
        // } elseif ($_SESSION['utype'] == 'Record Room') {
           $uquery = "SELECT * FROM (SELECT * FROM `track_file` WHERE `file_status`!='0' AND (`return_date` IS NULL || `return_date`='')) as `fts` GROUP BY `file_no` ORDER BY `id` DESC";
            // $uquery = "SELECT * FROM `track_file` WHERE `move_type`='1' AND `file_status`!='0' AND `return_date` IS NULL GROUP BY `file_no` ORDER BY `id` DESC";
            // $uquery = "SELECT * FROM `track_file` WHERE `move_type`='1' AND `file_status`!='0' AND `return_date`='' GROUP BY `file_no` ORDER BY `id` DESC";

        } else {
           $uquery = "SELECT * FROM `track_file` WHERE `move_type`='1' AND `file_status`!='3' AND `file_location`='" . $_SESSION['department_id'] . "' AND `move_to`!='".$_SESSION['adminid']."' ORDER BY `id` DESC";


        }
        
        $this->Query($uquery);
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    }
    
    function assign_file()
    {
        $record_id     = $_REQUEST['record_id'];
        $uid           = $_REQUEST['uid'];
        $assign_to     = $_REQUEST['assign'];
        $priority     =  $_REQUEST['priority'];
        $move_from     = $_SESSION['adminid'];
        $department_id = $_SESSION['department_id']=='2'?$this->userDepartment_id($assign_to):$_SESSION['department_id'];
        // $department_id = $_SESSION['utype']=='Record Room'?$this->userDepartment_id($assign_to):$_SESSION['department_id'];

        $file_status  = $_SESSION['department_id']=='2'?'1':'2';
        // $file_status  = $_SESSION['utype']=='Record Room'?'1':'2';

        $link_ID = "'".implode("','", $uid)."'";
        
        for ($i = 0; $i < count($record_id); $i++) {
            
            $data = mysql_fetch_array(mysql_query("SELECT * FROM `file_record` WHERE `id`='".$record_id[$i]."'"));
            
            $query = mysql_query("INSERT INTO `track_file`(`record_id`, `file_no`, `created_by`, `move_from`, `from_depart`, `move_to`, `to_depart`, `move_type`, `department_id`, `move_date`, `priority`, `file_status`, `file_location`, `action`) VALUES ('" . $record_id[$i] . "','" . $uid[$i] . "','" . ($data['created_by']?$data['created_by']:"1") . "','" . $move_from . "','" . $_SESSION['department_id'] . "','" . $assign_to . "','" . $department_id . "','1','" . $department_id . "','" . date('Y-m-d H:i:s') . "','" . $priority[$i] . "','2','" . $department_id . "','2');");
            
            $sql = mysql_query("UPDATE `file_record` SET `file_status`='".$file_status."', `file_location`='".$department_id."', `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."' WHERE `id`='" . $record_id[$i] . "'");
            
            
            /*=======Activity=======*/
            // echo "SELECT `date_created` FROM `activity_log` WHERE `file_no`='".$uid[$i]."' ORDER BY `id` DESC LIMIT 1<br>";
            $lst_act = mysql_fetch_array(mysql_query("SELECT `date_created` FROM `activity_log` WHERE `file_no`='" . $uid[$i] . "' ORDER BY `id` DESC LIMIT 1"));
            
            $datetime1 = new DateTime();
            $datetime2 = new DateTime(substr($lst_act['date_created'], 0, 16));
            $interval  = $datetime1->diff($datetime2);
            $elapsed   = $interval->format('%a days %H:%I');
            
            
            // 1. Craeted, 2. Edited, 3. Assign, 4. dispatch, 5. receive back, 6. Accept, 7. Rejected
            $activity = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid[$i] . "','Assigning File','3','" . $department_id . "','" . $move_from . "','" . $department_id . "','" . $assign_to . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "')");
            
            
        }
        // exit;  
        $_SESSION['error']   = '<b>File has been Sucessfully Assign</b>.<br> Do you want print   <a class="btn btn-primary" target="_blank" href="script/print/assign_print.php?depart='.$department_id.'&empl='.$assign_to.'&uid=('.$link_ID.')"  style="padding: 2px;"  onclick="hidePopupbox()">Yes</a>';
        $_SESSION['errorclass'] = SUCCESSCLASS;


        // header('location:index.php?control=file_transfer&task=received');
        header('location:index.php?control=file_transfer&task=assigned');
        
    }
    
    
    function dispatch()
    {
        
        $files   = $_REQUEST['file_ids'];
        $file_id = implode(',', $files)?implode(',', $files):$files;
        
        $uquery = "SELECT * FROM `file_record`  WHERE  `id` in (" . $file_id . ")";
        // $uquery = "SELECT *,(SELECT `id` FROM `file_move` FM WHERE FM.`uid` = FR.`uid` AND `file_status`='1') as mfid FROM `file_record` FR WHERE  `id` in (".$file_id.")";
        $this->Query($uquery);
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    }
    
    function dispatch_file()
    {
        $record_id     = $_REQUEST['record_id'];
        $uid           = $_REQUEST['uid'];
        $remark        = $_REQUEST['remark'];
        $move_from     = $_SESSION['adminid'];
        $priority     = $_REQUEST['priority'];
         $department_id = $_REQUEST['dispatch'];

         $chk_user = $this->ChkUser($department_id);
        //exit();
        if($chk_user!=""){
                $move_to     = mysql_fetch_array(mysql_query("SELECT * FROM `users` WHERE `post_id`=1 AND `department_id`='" . $department_id . "' AND `status`=1"));
                $dispatch_to = $move_to['id'];
                
                for ($i = 0; $i < count($record_id); $i++) {
                    
                    $data = mysql_fetch_array(mysql_query("SELECT * FROM `file_record` WHERE `id`='".$record_id[$i]."'"));
        
                    $chk = mysql_fetch_array(mysql_query("SELECT * FROM `track_file` WHERE `record_id`='".$record_id[$i]."' AND `move_from`='".$move_from."' AND `move_to`='".$dispatch_to."' AND `move_type`='2' AND `department_id`='".$department_id."' AND `action`=3 ORDER BY `id` DESC LIMIT 1"));
                    if($chk['id']==''){
             
                $query = mysql_query("INSERT INTO `track_file`(`record_id`, `file_no`, `created_by`, `move_from`, `from_depart`, `move_to`, `to_depart`, `move_type`, `department_id`, `move_date`, `priority`, `file_status`, `file_location`, `remark`, `action`) VALUES ('" . $record_id[$i] . "','" . $uid[$i] . "','" . ($data['created_by']?$data['created_by']:"1") . "','" . $move_from . "','" . $_SESSION['department_id'] . "','" . $dispatch_to . "','" . $department_id . "','2','" . $department_id . "','" . date('Y-m-d H:i:s') . "','" . $priority[$i] . "','2','" . $_SESSION['department_id'] . "','" . $remark . "','1');");
                //exit;
                    }else{
                        
                        $sql = mysql_query("UPDATE `track_file` SET `action`='1', `remark`= NULL, `move_date`='" . date('Y-m-d H:i:s') . "' WHERE `id`='" . $chk['id'] . "'");
                        // $sql = mysql_query("UPDATE `track_file` SET `action`='1', `remark`='', `move_date`='" . date('Y-m-d H:i:s') . "' WHERE `id`='" . $chk['id'] . "'");
        
                        $sql1 = mysql_query("UPDATE `file_record` SET `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."' WHERE `uid`='" . $chk['file_no'] . "'");  
                    
        
                    }
                     $sql = mysql_query("UPDATE `file_record` SET `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."', `file_status`='2' WHERE `id`='" . $record_id[$i] . "'");
                     // $sql = mysql_query("UPDATE `file_record` SET `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."' WHERE `id`='" . $record_id[$i] . "'");
                    
                    
                    /*=======Activity=======*/
                    $lst_act = mysql_fetch_array(mysql_query("SELECT * FROM `activity_log` WHERE `file_no`='" . $uid[$i] . "' ORDER BY `id` DESC LIMIT 1"));
                    
                    $datetime1 = new DateTime();
                    $datetime2 = new DateTime(substr($lst_act['date_created'], 0, 16));
                    $interval  = $datetime1->diff($datetime2);
                    $elapsed   = $interval->format('%a days %H:%I');
        
                    // 1. Craeted, 2. Edited, 3. Assign, 4. dispatch, 5. receive back, 6. Accept, 7. Rejected
                    $activity  = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid[$i] . "','Dispach File','4','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "')");
                    
                    
                }
                // exit;  
                $_SESSION['error']    = "Sucessfully Dispatched ";
                $_SESSION['check'] = "1";
                // $_SESSION['error']    = "Sucessfully Dispatched <a  href='script/print/dispatch_print.php?depart='.$department_id.'&uid=('.implode(",", $uid)'>Print</a>";
                $_SESSION['errorclass'] = SUCCESSCLASS;
        
        
                header('location:index.php?control=file_transfer&task=received');
                // header('location:index.php?control=file_transfer&task=dispatched');
            }else{
                $_SESSION['error']    = "No User Fond in this department, Please try again";
                $_SESSION['errorclass'] = SUCCESSCLASS;
                header('location:index.php?control=file_transfer&task=received');
            }
        
    }
    
    function again_dispatch()
    {
        $id  = $_REQUEST['id'];
        $fid  = $_REQUEST['fid'];
        $sql = mysql_query("UPDATE `track_file` SET `action`='1', `remark` = NULL, `move_date`='" . date('Y-m-d H:i:s') . "' WHERE `id`='" . $id . "'");
        // $sql = mysql_query("UPDATE `track_file` SET `action`='1', `remark`='', `move_date`='" . date('Y-m-d H:i:s') . "' WHERE `id`='" . $id . "'");

         $sql1 = mysql_query("UPDATE `file_record` SET `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."' WHERE `uid`='" . $fid . "'");

         $depart = mysql_fetch_array(mysql_query("SELECT `to_depart` FROM `track_file` WHERE `file_no`='".$fid."' AND `action`='3' ORDER BY `id` DESC  LIMIT 1"));

        /*==========Server Log========*/
        $activity = "Again Dispatch File (".$fid.") by ".($this->userName($_SESSION['adminid']))." (".($this->userDepartment($_SESSION['adminid'])).") to ".($this->departmentName($depart['to_depart']))." Depertment"; 
        $log =mysql_query("INSERT INTO `server_log`( `system_ip`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");
        
        $_SESSION['error']   = "Sucessfully Dispatched";
        $_SESSION['errorclass'] = SUCCESSCLASS;
        header('location:index.php?control=file_transfer&task=dispatched');
    }
    
    
    
    function dispatched()
    {
        /*Dispatched file list*/
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        $dateTo   = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        
        
        $user = $_SESSION['adminid'];
        
        if ($_SESSION['department_id'] == 'Admin') {
        // if ($_SESSION['utype'] == 'Admin') {
             $uquery = "SELECT `temp`.* FROM (SELECT * FROM `track_file` WHERE 1 $dateFrom $dateTo $bydate $date ORDER BY `id` DESC) `temp` GROUP BY `temp`.`record_id` ORDER BY `temp`.`id` DESC";
           // echo  $uquery = "SELECT * FROM `track_file` WHERE 1 $dateFrom $dateTo $bydate $date GROUP BY `record_id` ORDER BY `id` DESC";
            
        } else {
            $uquery = "SELECT * FROM `track_file` WHERE `move_type`='2' AND `from_depart`='" . $_SESSION['department_id'] . "' AND `action`!='2' ORDER BY `id` DESC";
            // $uquery = "SELECT * FROM `track_file` WHERE `move_type`='2' AND `move_from`='" . $_SESSION['adminid'] . "' AND `action`!='2' ORDER BY `id` DESC";
            
        }
        
        $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
        // require_once("views/" . $this->name . "/show.php");
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    }

      
    function receive()
    {
        
        $files   = $_REQUEST['file_ids'];
        $_SESSION['assigned_user']  = is_array($_REQUEST['user_id'])?($_REQUEST['user_id'][0]):$_REQUEST['user_id'];
        $file_id = implode(',', $files)?implode(',', $files):$files;
        
        $uquery = "SELECT * FROM `file_record` WHERE  `id` in (" . $file_id . ")";
        // $uquery = "SELECT *,(SELECT `id` FROM `file_move` FM WHERE FM.`uid` = FR.`uid` AND `file_status`='1') as mfid FROM `file_record` FR WHERE  `id` in (".$file_id.")";
        $this->Query($uquery);
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    }   
    
    function recieve_back()
    {
        /*Receive back assigned file*/
        $trake_id = $_REQUEST['file_ids'];
        $empl_id = $_REQUEST['user'];

        $link_ID = "'".implode("','", $trake_id)."'";
        //$id = implode(',', $trake_id);

        
        for ($i = 0; $i < count($trake_id); $i++) {
            
            if($_SESSION['department_id']=='2'){
            // if($_SESSION['utype']=='Record Room'){
                mysql_query("UPDATE `track_file` SET `file_status`='0', `file_location`='".$_SESSION['department_id']."', `return_date`='" . date('Y-m-d H:i:s') . "' WHERE `file_no` = '" . $trake_id[$i] . "'");
            }else{
                mysql_query("UPDATE `track_file` SET `file_status`='3', `file_location`='".$_SESSION['department_id']."' WHERE `file_no` = '" . $trake_id[$i] . "'");
            }
                mysql_query("UPDATE `file_record` SET `file_status`='3', `file_location`='".$_SESSION['department_id']."', `close_date`='" . date('Y-m-d H:i:s') . "', `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."' WHERE uid = '" . $trake_id[$i] . "'");
            
            
            /*=======Activity=======*/
            $lst_act = mysql_fetch_array(mysql_query("SELECT * FROM `activity_log` WHERE `file_no`='" . $trake_id[$i] . "' ORDER BY `id` DESC LIMIT 1"));
            
            $datetime1 = new DateTime();
            $datetime2 = new DateTime(substr($lst_act['date_created'], 0, 16));
            $interval  = $datetime1->diff($datetime2);
            $elapsed   = $interval->format('%a days %H:%I');
          
            // 1. Craeted, 2. Edited, 3. Assign, 4. dispatch, 5. receive back, 6. Accept, 7. Rejected
            $activity  = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $trake_id[$i] . "','Receive Assigned File','5','" . $lst_act['to_depart'] . "','" . $lst_act['to_user'] . "','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "')");
            
            
        }
       
        $_SESSION['error'] = '<b>File has been Sucessfully Received.</b><br> Do you want print  <a class="btn btn-primary" target="_blank"  href="script/print/receive_print.php?empl='.$empl_id.'&uid=('.$link_ID.')" style="padding: 2px;" onclick="hidePopupbox()">Yes</a>';
        $_SESSION['errorclass'] = SUCCESSCLASS;


        header("location:index.php?control=file_transfer&task=assigned");
    }
    
    
    function exceeded()
    {
        /*Exceeded File list*/
        
        $dateFrom = $_REQUEST['from_date'] ? " and date ='" . $_REQUEST['from_date'] . "'" : '';
        $dateTo   = $_REQUEST['to_date'] ? " and date ='" . $_REQUEST['to_date'] . "'" : '';
        $bydate   = $dateFrom ? $dateFrom : $dateTo;
        $date     = ($dateFrom && $dateTo) ? " and date between '" . $_REQUEST['from_date'] . "' and '" . $_REQUEST['to_date'] . "'" : $bydate;
        
        $user     = $_SESSION['adminid'];
        $days_ago = date('Y-m-d', strtotime('-15 days', strtotime(date('Y-m-d'))));
        
        if ($_SESSION['department_id'] == 'Admin') {
        // if ($_SESSION['utype'] == 'Admin') {
            $uquery = "SELECT * FROM `track_file` WHERE `file_status`='2' AND `action`='2' AND `move_type`='1' AND `move_date`<'" . $days_ago . "' ORDER BY `move_date` ASC";
            
            } else {
                
            $uquery = "SELECT * FROM `track_file` WHERE `file_status`='2' AND `action`='2' AND `move_type`='1' AND `move_from`='" . $user . "' AND `move_date`<'" . $days_ago . "' ORDER BY `move_date` ASC";

            }
        
        $this->Query($uquery);
        $uresults           = $this->fetchArray();
        $tdata              = count($uresults);
        /* Paging start here */
        $page               = intval($_REQUEST['page']);
        $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
        $adjacents          = intval($_REQUEST['adjacents']);
        $tdata              = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
        $tdata              = floor($tdata);
        if ($page <= 0)
            $page = 1;
        if ($adjacents <= 0)
            $tdata ? ($adjacents = 4) : 0;
        $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
        
        /* Paging end here */
        $query = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
        $this->Query($query);
        $results = $this->fetchArray();
        
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    }
    
    
    
    
    function pending_list()
    {
        
        if ($_SESSION['department_id'] == 'Admin') {
        // if ($_SESSION['utype'] == 'Admin') {
            $uquery = "SELECT * FROM `track_file` WHERE `file_status`='2' AND `action`=2 AND `move_type`=1 AND `move_date`<'" . $days_ago . "' ORDER BY `move_date` DESC";
            
        } else {
            $uquery = "SELECT * FROM `track_file` WHERE `file_status`='2' AND `action`=2 AND `move_type`=1 AND `move_from`='" . $user . "' AND `move_date`<'" . $days_ago . "' ORDER BY `move_date` DESC";
            
            
        }
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    } 
    
    
    function rack_save(){
        $id = $_REQUEST['id'];
        $file_rack_location = $_REQUEST['file_rack_location'];
        
           mysql_query("UPDATE `file_record` SET  `file_rack_location`='".$file_rack_location."' WHERE id='".$id."'");
        $uid = mysql_fetch_array(mysql_query("SELECT `uid` FROM `file_record` WHERE `id`='".$id."'"));
                   /*==========Server Log========*/
        $activity = "Update Rack Location of file (".$uid['uid'].") by ".($this->userName($_SESSION['adminid']))." (".($this->userDepartment($_SESSION['adminid'])).") "; 

        $log =mysql_query("INSERT INTO `server_log`( `system_ip`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");

        $_SESSION['back_btn']="1";       
       header('Location:index.php?control=file_transfer&task=file_detail&fid='.$id);
       // require_once("views/" . $this->name . "/" . $this->task . ".php");
    }
    
    function lot_save(){
        $id = $_REQUEST['id'];
        $lot_no = $_REQUEST['lot_no'];
        
           mysql_query("UPDATE `file_record` SET  `lot_no`='".$lot_no."' WHERE id='".$id."'");

        $uid = mysql_fetch_array(mysql_query("SELECT `uid` FROM `file_record` WHERE `id`='".$id."'"));
                   /*==========Server Log========*/
        $activity = "Update LOT No. of file (".$uid['uid'].") by ".($this->userName($_SESSION['adminid']))." (".($this->userDepartment($_SESSION['adminid'])).") "; 

        $log =mysql_query("INSERT INTO `server_log`( `system_ip`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");

       $_SESSION['back_btn']="1";
       header('Location:index.php?control=file_transfer&task=file_detail&fid='.$id);
       /// require_once("views/" . $this->name . "/" . $this->task . ".php");
    }  
    
     
    function receiveFrom(){        
        $files   = $_REQUEST['file_ids'];
        $_SESSION['assigned_user']  = is_array($_REQUEST['user_id'])?($_REQUEST['user_id'][0]):$_REQUEST['user_id'];
        $file_id = implode(',', $files)?implode(',', $files):$files;
        
        $uquery = "SELECT * FROM `file_record` WHERE  `id` in (" . $file_id . ")";
        // $uquery = "SELECT *,(SELECT `id` FROM `file_move` FM WHERE FM.`uid` = FR.`uid` AND `file_status`='1') as mfid FROM `file_record` FR WHERE  `id` in (".$file_id.")";
        $this->Query($uquery);
        $results = $this->fetchArray();
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    } 
    
    
        
    
/*==============Gsingh================*/
    function receive_direct_file()
    {  
        /*=============Self Assign==============*/
        $file_id   = $_REQUEST['record_id'];
        $uid   = $_REQUEST['uid'];
        $empl_id = $_REQUEST['empl_id'];
        $department_id = $_REQUEST['department'];

        $link_UID  = "('".$uid."')";

         
        // $file_id = implode(',', $files);
       // echo "<br>INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $empl_id . "','" . $uid . "','Self Assigning File','0','1','0','" . $department_id . "','" . $empl_id . "','0 days 00:00','" . date('Y-m-d H:i:s') . "')";
        // exit();
        $uquery = mysql_query("UPDATE `file_record` SET `file_location`='".$_SESSION['department_id']."', `modified_by`='".$_SESSION['adminid']."', `modify_date`='".date('Y-m-d H:i:s')."' WHERE `id`='".$file_id."'");

         $activity = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $empl_id . "','" . $uid . "','Self Assigning File','0','1','0','" . $department_id . "','" . $empl_id . "','0 days 00:00','" . date('Y-m-d H:i:s') . "')");

        $data = mysql_fetch_array(mysql_query("SELECT * FROM `file_record` WHERE `id`='".$file_id."'"));

        $query = mysql_query("INSERT INTO `track_file`(`record_id`, `file_no`, `created_by`, `move_from`, `from_depart`, `move_to`, `to_depart`, `move_type`, `department_id`, `move_date`, `priority`, `file_status`, `file_location`, `action`) VALUES ('" . $file_id . "','" . $uid. "','" . ($data['created_by']?$data['created_by']:"1") . "','0','1','" . $empl_id . "','" . $department_id . "','1','" . $department_id . "','" . date('Y-m-d H:i:s') . "','" . $data['priority'] . "','0','" . $department_id . "','');");


         /*============Receive Back=========*/
    
            if($_SESSION['department_id']=='2'){
                mysql_query("UPDATE `track_file` SET `file_status`='0', `file_location`='".$_SESSION['department_id']."', `return_date`='" . date('Y-m-d H:i:s') . "' WHERE `file_no` = '" . $uid . "'");
            }else{
                mysql_query("UPDATE `track_file` SET `file_status`='3', `file_location`='".$_SESSION['department_id']."' WHERE `file_no` = '" . $uid . "'");
            }
                mysql_query("UPDATE `file_record` SET `file_status`='3', `file_location`='".$_SESSION['department_id']."', `close_date`='" . date('Y-m-d H:i:s') . "', `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."' WHERE uid = '" . $uid . "'");
            
            
            /*=======Activity=======*/
            $lst_act = mysql_fetch_array(mysql_query("SELECT * FROM `activity_log` WHERE `file_no`='" . $uid . "' ORDER BY `id` DESC LIMIT 1"));
            
            $datetime1 = new DateTime();
            $datetime2 = new DateTime(substr($lst_act['date_created'], 0, 16));
            $interval  = $datetime1->diff($datetime2);
            $elapsed   = $interval->format('%a days %H:%I');
          
            // 1. Craeted, 2. Edited, 3. Assign, 4. dispatch, 5. receive back, 6. Accept, 7. Rejected
            $activity  = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','Receive Assigned File','5','" . $lst_act['to_depart'] . "','" . $lst_act['to_user'] . "','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "')");
            
            
        //}
       
        $_SESSION['error'] = '<b>File has been Sucessfully Received.</b><br> Do you want print  <a class="btn btn-primary" target="_blank"  href="script/print/receive_print.php?empl='.$empl_id.'&uid='.$link_UID.'" style="padding: 2px;" onclick="hidePopupbox()">Yes</a>';
        $_SESSION['errorclass'] = SUCCESSCLASS;

        $rr_files = ($_SESSION['department_id']=="2")?("&rr_files=1"):"";
        header('location:index.php?control=file_transfer&task=received'.$rr_files);
        // header("location:index.php?control=file_transfer&task=assigned");

    }

    function daily_report(){
        $to_user = $_REQUEST['empl_list'];
        $to_depart = $_REQUEST['department'];
        $activity_type = $_REQUEST['activity_type'];
        if($activity_type ==''){
            require_once("views/" . $this->name . "/" . $this->task . ".php");
            }
         else{
            if($activity_type=="3"){
         $query = "SELECT * FROM `activity_log` WHERE `activity_type`='".$activity_type."' AND `date_created` LIKE '".date('Y-m-d')."%' AND `to_depart`='".$to_depart."' AND `to_user`='".$to_user."' GROUP BY `file_no` ORDER BY `id` DESC";
        }else{
            $query = "SELECT * FROM `activity_log` WHERE `activity_type`='".$activity_type."' AND `date_created` LIKE '".date('Y-m-d')."%' AND `from_depart`='".$to_depart."' AND `from_user`='".$to_user."' GROUP BY `file_no` ORDER BY `id` DESC";
        }


        $this->Query($query);
        $results = $this->fetchArray();
       require_once("views/" . $this->name . "/" . $this->task . ".php");
        }        
    }


    function show_list(){

        $to_user = $_REQUEST['empl_list'];
        $to_depart = $_REQUEST['department'];
        $activity_type = $_REQUEST['activity_type'];

        $query = mysql_query("SELECT * FROM `activity_log` WHERE `activity_type`='".$activity_type."' AND `date_created` LIKE '".date('Y-m-d')."%' AND `to_depart`='".$to_depart."' AND `to_user`='".$to_user."' GROUP BY `file_no` ORDER BY `id` DESC");

        $this->Query($query);
        $results = $this->fetchArray();
        header('location:index.php?control=file_transfer&task=daily_report');
    }

function addnew_file(){
     if ($_REQUEST['id']) {
        $query_com = "SELECT * FROM  file_record WHERE id=" . $_REQUEST['id'];
        
        $this->Query($query_com);
        $results = $this->fetchArray();
        
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    } else {
        require_once("views/" . $this->name . "/" . $this->task . ".php");
    }   
}

function save_file()
    {
        
        $old_no      = mysql_real_escape_string($_REQUEST['old_no']);
        $new_no      = mysql_real_escape_string($_REQUEST['new_no']);
        $department  = $_REQUEST['department']?$_REQUEST['department']:$_SESSION['department_id'];
        // $department_id  = mysql_real_escape_string($_REQUEST['department_id']);
     
        $uid1        = mysql_real_escape_string($_REQUEST['uid']);
        $old_assigned  = $_REQUEST['old_assigned'];

        /*=========New Change==========*/
        $sceme_name     =   mysql_real_escape_string($_REQUEST['sceme_name']);
        $lot_no        =    mysql_real_escape_string($_REQUEST['lot_no']);
        $allotee        =   mysql_real_escape_string($_REQUEST['allotee']);
        $zone        =      mysql_real_escape_string($_REQUEST['zone']);
        $property_no   =    mysql_real_escape_string($_REQUEST['property_no']);
        $category        =  mysql_real_escape_string($_REQUEST['category']);
        $name        =      mysql_real_escape_string($_REQUEST['name']);
        $file_name        = mysql_real_escape_string($_REQUEST['file_name']);
        $contract_name   =  mysql_real_escape_string($_REQUEST['contract_name']);
        $work_name        = mysql_real_escape_string($_REQUEST['work_name']);
        $mb_num        =    mysql_real_escape_string($_REQUEST['mb_num']);
        $address        =   mysql_real_escape_string($_REQUEST['address']);
        $detail        =    mysql_real_escape_string($_REQUEST['detail']);
        $pentition_id   =   mysql_real_escape_string($_REQUEST['pentition_id']);
        $court_name      =  mysql_real_escape_string($_REQUEST['court_name']);
        $related_depart  =  mysql_real_escape_string($_REQUEST['related_depart']);
        $advocate_name   =  mysql_real_escape_string($_REQUEST['advocate_name']);
        $pensioner_name  =  mysql_real_escape_string($_REQUEST['pensioner_name']);
        $property_detail =  mysql_real_escape_string($_REQUEST['property_detail']);
        $year       =       mysql_real_escape_string($_REQUEST['year']);
        $section_name=      mysql_real_escape_string($_REQUEST['section_name']);
        $file_type   =      mysql_real_escape_string($_REQUEST['file_type']);
        $father_name  =     mysql_real_escape_string($_REQUEST['father_name']);
        $department_name =  mysql_real_escape_string($_REQUEST['department_name']);
        $post        =      mysql_real_escape_string($_REQUEST['post']);
        $dob        =       mysql_real_escape_string($_REQUEST['dob']);
        $appointment_date =  mysql_real_escape_string($_REQUEST['appointment_date']);
        $retirement_date =   mysql_real_escape_string($_REQUEST['retirement_date']);
        $family_pensioner =  mysql_real_escape_string($_REQUEST['family_pensioner']);
        $relation =         mysql_real_escape_string($_REQUEST['relation']);
        $priority =      mysql_real_escape_string($_REQUEST['add_priority']);
        $file_rack_location =      mysql_real_escape_string($_REQUEST['file_rack_location']);
        $empl_list = $_REQUEST['empl_list']?$_REQUEST['empl_list']:$_SESSION['adminid'];

        $created_by = $_SESSION['adminid'];

        if($_SESSION['utype']=="Admin" || $_SESSION['utype']=="Super-Admin"){
        $file_loc = "2";
    }else{
        $file_loc = $_SESSION['department_id'];
    }
        
        if (!$_REQUEST['id']) {
            
           $query = "INSERT INTO `file_record`(`old_no`, `new_no`, `file_name`, `zone`, `category`, `property_no`, `sceme_name`, `allotee`, `subject`, `description`, `owner`, `lot_no`, `department_id`, `name`, `contract_name`, `work_name`, `mb_num`, `address`, `detail`, `pentition_id`, `court_name`, `related_depart`, `advocate_name`, `pensioner_name`, `property_detail`, `year`, `section_name`, `file_type`, `father_name`, `department_name`, `post`, `dob`, `appointment_date`, `retirement_date`, `family_pensioner`, `relation`, `assigned_to`, `dispatched_to`, `priority`, `file_location`, `created_by`, `created_date`, `modified_by`, `modify_date`,file_rack_location) VALUES ('".$old_no."', '".$new_no."', '".$file_name."', '".$zone."', '".$category."', '".$property_no."', '".$sceme_name."', '".$allotee."', '".$subject."', '".$description."', '".$owner."', '".$lot_no."', '".$department."', '".$name."', '".$contract_name."', '".$work_name."', '".$mb_num."', '".$address."', '".$detail."', '".$pentition_id."', '".$court_name."', '".$related_depart."', '".$advocate_name."', '".$pensioner_name."', '".$property_detail."', '".$year."', '".$section_name."', '".$file_type."', '".$father_name."', '".$department_name."', '".$post."', '".$dob."', '".$appointment_date."', '".$retirement_date."', '".$family_pensioner."', '".$relation."', '".$assigned_to."', '".$dispatched_to."', '".$priority."', '".$file_loc."','" . $created_by . "','" . date('Y-m-d H:i:s') . "','" . $created_by . "','" . date('Y-m-d H:i:s') . "', '".$file_rack_location."')";
            
            $ece     = mysql_query($query);
            $id      = mysql_insert_id();           
            
        
            $key = $this->departmentNameKey($department);   
            $number_of_digits = $this->count_digit($id);    
            
            $uid     = $key . $_SESSION['fyear'] .$number_of_digits. $id;
        
            $update  = mysql_query("UPDATE `file_record` set `uid`='" . $uid . "' where id='" . $id . "'");
            if($_SESSION['department_id']=='2' || $_SESSION['utype']=="Admin" || $_SESSION['utype']=="Super-Admin"){

                if($_SESSION['utype']=="Admin" || $_SESSION['utype']=="Super-Admin"){
                    $to_user = $this->ChkUser("2");
                }else{
                    $to_user = $_SESSION['adminid'];
                }

             $activity = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','New file Created','1','2','" . $to_user . "','0 days 00:00','" . date('Y-m-d H:i:s') . "')");

            }else{
        
            $move_from     = mysql_fetch_array(mysql_query("SELECT * FROM `users` WHERE  `department_id`='" . $department . "' AND `status`= '1' AND `login`='1'"));
            // $move_from     = mysql_fetch_array(mysql_query("SELECT * FROM `users` WHERE  `department_id`='" . $department . "' AND `status`= 1"));
            $dispatch_from = $_REQUEST['empl_list']?$_REQUEST['empl_list']:$move_from['id'];

             $activity = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','New file Created','1','" . $department . "','" . $dispatch_from . "','0 days 00:00','" . date('Y-m-d H:i:s') . "')");

    /*=============================================*/
    /*=============================================*/

    $remark        = $_REQUEST['remark'];
    $move_to     = $_SESSION['adminid'];
    // $priority       = $_REQUEST['priority'];
    $department = $_REQUEST['department']; 

    /*============Dispatch File========*/

    $data = mysql_fetch_array(mysql_query("SELECT * FROM `file_record` WHERE `id`='".$id."'"));

    $query = mysql_query("INSERT INTO `track_file`(`record_id`, `file_no`, `created_by`, `move_from`, `from_depart`, `move_to`, `to_depart`, `move_type`, `department_id`, `move_date`, `priority`, `file_status`, `file_location`, `remark`, `action`) VALUES ('" . $id . "','" . $data['uid'] . "','" . ($data['created_by']?$data['created_by']:"1") . "','" . $dispatch_from . "','" . $department . "','" . $move_to . "','" . $_SESSION['department_id'] . "','2','" . $_SESSION['department_id'] . "','" . date('Y-m-d H:i:s') . "','" . $data['priority'] . "','2','" . $_SESSION['department_id'] . "','" . $remark . "','1')");


    $sql = mysql_query("UPDATE `file_record` SET `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."', `file_status`='2' WHERE `id`='" . $id . "'");

    $activity  = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','Dispach File','4','" . $department . "','" . $dispatch_from . "','" . $department . "','" . $dispatch_from . "','0 days 00:00','" . date('Y-m-d H:i:s') . "')");


    /*============Accept File=========*/
    $status = "2";

    $msg     = 'Accepted (Received)';
    $action  = '6';
    $file_location  = " ,`file_location`='".$_SESSION['department_id']."'";
    $remark  = mysql_real_escape_string($_REQUEST['remark']);

    $link_ID = "'".$data['uid']."'";

      

    $query = mysql_query("UPDATE `track_file` SET `action`='" . $status . "', `file_location`='" . $_SESSION['department_id'] . "', `remark`='" . $remark . "' WHERE `file_no`='" . $data['uid'] . "' AND `department_id`='" . $_SESSION['department_id'] . "' AND `move_type`='2'");
    // echo "UPDATE `file_record` SET `file_status`='1' $file_location WHERE `uid`='".$data['uid']."' AND `file_status`='1'";

    $query1 = mysql_query("UPDATE `file_record` SET `file_status`='1', `modify_date`='".date('Y-m-d H:i:s')."', `modified_by`='".$_SESSION['adminid']."' $file_location WHERE `uid`='".$data['uid']."' ");



    $lst_act   = mysql_fetch_array(mysql_query("SELECT * FROM `activity_log` WHERE `file_no`='".$data['uid']. "' ORDER BY `id` DESC LIMIT 1"));

    $datetime1 = new DateTime();
    $datetime2 = new DateTime(substr($lst_act['date_created'], 0, 16));
    $interval  = $datetime1->diff($datetime2);
    $elapsed   = $interval->format('%a days %H:%I');

    $sql = "INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $data['uid'] . "','" . $msg . "','" . $action . "','" . $lst_act['from_depart'] . "','" . $lst_act['from_user'] . "','" . $_SESSION['department_id'] . "','" . $_SESSION['adminid'] . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "')";


    $_SESSION['error']   = '<b>File Sucessfully '.$msg.'</b>.<br> Do you want print <a class="btn btn-primary" target="_blank" href="script/print/to_be_received_print.php?file_id='.$link_ID.'&last_depart='.$lst_act['from_depart'].'&from_user='.$lst_act['from_user'].'&to_depart='.$_SESSION['department_id'].'&to_user='.$_SESSION['adminid'].'"  style="padding: 2px;" onclick="hidePopupbox()">Yes</a>';
    $_SESSION['action'] = 'accept';
   
    $act = mysql_query($sql);
   
    $activity = "File ".$msg." by ".($this->userName($_SESSION['adminid']))." (".($this->userDepartment($_SESSION['adminid'])).")"; 
    $log =mysql_query("INSERT INTO `server_log`( `system_ip`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");

        /*=============================================*/
        /*=============================================*/
        /*=============================================*/
            }

        if($_SESSION['department_id']=='2'){
            $_SESSION['check'] = "1";
            $_SESSION['errorclass'] = ERRORCLASS;
            $_SESSION['error']      = ADDNEWRECORD;
            header("location:index.php?control=file_transfer&task=received&rr_files=1");

        }elseif($_SESSION['utype']=="Admin" || $_SESSION['utype']=="Super-Admin"){

            $_SESSION['errorclass'] = ERRORCLASS;
            $_SESSION['error']      = ADDNEWRECORD;
            header("location:index.php?control=file_transfer&task=addnew");
        }else{

            $_SESSION['errorclass'] = ERRORCLASS;
            header("location:index.php?control=file_transfer&task=received");
        }
          
            
        } 
        
    }


function admin_save_new_file()   
    {
        
        $old_no      = mysql_real_escape_string($_REQUEST['old_no']);
        $new_no      = mysql_real_escape_string($_REQUEST['new_no']);
        $department  = $_REQUEST['department']?$_REQUEST['department']:$_SESSION['department_id'];
        $department_id  = mysql_real_escape_string($_REQUEST['department_id']);
     
        $uid1        = mysql_real_escape_string($_REQUEST['uid']);
        $old_assigned  = $_REQUEST['old_assigned'];

        /*=========New Change==========*/
        $sceme_name     =   mysql_real_escape_string($_REQUEST['sceme_name']);
        $lot_no        =    mysql_real_escape_string($_REQUEST['lot_no']);
        $allotee        =   mysql_real_escape_string($_REQUEST['allotee']);
        $zone        =      mysql_real_escape_string($_REQUEST['zone']);
        $property_no   =    mysql_real_escape_string($_REQUEST['property_no']);
        $category        =  mysql_real_escape_string($_REQUEST['category']);
        $name        =      mysql_real_escape_string($_REQUEST['name']);
        $file_name        = mysql_real_escape_string($_REQUEST['file_name']);
        $contract_name   =  mysql_real_escape_string($_REQUEST['contract_name']);
        $work_name        = mysql_real_escape_string($_REQUEST['work_name']);
        $mb_num        =    mysql_real_escape_string($_REQUEST['mb_num']);
        $address        =   mysql_real_escape_string($_REQUEST['address']);
        $detail        =    mysql_real_escape_string($_REQUEST['detail']);
        $pentition_id   =   mysql_real_escape_string($_REQUEST['pentition_id']);
        $court_name      =  mysql_real_escape_string($_REQUEST['court_name']);
        $related_depart  =  mysql_real_escape_string($_REQUEST['related_depart']);
        $advocate_name   =  mysql_real_escape_string($_REQUEST['advocate_name']);
        $pensioner_name  =  mysql_real_escape_string($_REQUEST['pensioner_name']);
        $property_detail =  mysql_real_escape_string($_REQUEST['property_detail']);
        $year       =       mysql_real_escape_string($_REQUEST['year']);
        $section_name=      mysql_real_escape_string($_REQUEST['section_name']);
        $file_type   =      mysql_real_escape_string($_REQUEST['file_type']);
        $father_name  =     mysql_real_escape_string($_REQUEST['father_name']);
        $department_name =  mysql_real_escape_string($_REQUEST['department_name']);
        $post        =      mysql_real_escape_string($_REQUEST['post']);
        $dob        =       mysql_real_escape_string($_REQUEST['dob']);
        $appointment_date =  mysql_real_escape_string($_REQUEST['appointment_date']);
        $retirement_date =   mysql_real_escape_string($_REQUEST['retirement_date']);
        $family_pensioner =  mysql_real_escape_string($_REQUEST['family_pensioner']);
        $relation =         mysql_real_escape_string($_REQUEST['relation']);
        $priority =      mysql_real_escape_string($_REQUEST['add_priority']);
        $file_rack_location =      mysql_real_escape_string($_REQUEST['file_rack_location']);
        $empl_list =     $_REQUEST['empl_list']?$_REQUEST['empl_list']:$_SESSION['adminid'];


         $assigned_to = ($this->ChkUser($department));
       // exit;
        $move_type   = "1";
     
        $created_by = $_SESSION['adminid'];
        
        if ($assigned_to!="") {
            
           $query = "INSERT INTO `file_record`(`old_no`, `new_no`, `file_name`, `zone`, `category`, `property_no`, `sceme_name`, `allotee`, `subject`, `description`, `owner`, `lot_no`, `department_id`, `name`, `contract_name`, `work_name`, `mb_num`, `address`, `detail`, `pentition_id`, `court_name`, `related_depart`, `advocate_name`, `pensioner_name`, `property_detail`, `year`, `section_name`, `file_type`, `father_name`, `department_name`, `post`, `dob`, `appointment_date`, `retirement_date`, `family_pensioner`, `relation`, `assigned_to`, `dispatched_to`, `priority`, `file_location`, `created_by`, `created_date`, `modified_by`, `modify_date`,`file_rack_location`) VALUES ('".$old_no."', '".$new_no."', '".$file_name."', '".$zone."', '".$category."', '".$property_no."', '".$sceme_name."', '".$allotee."', '".$subject."', '".$description."', '".$owner."', '".$lot_no."', '".$department."', '".$name."', '".$contract_name."', '".$work_name."', '".$mb_num."', '".$address."', '".$detail."', '".$pentition_id."', '".$court_name."', '".$related_depart."', '".$advocate_name."', '".$pensioner_name."', '".$property_detail."', '".$year."', '".$section_name."', '".$file_type."', '".$father_name."', '".$department_name."', '".$post."', '".$dob."', '".$appointment_date."', '".$retirement_date."', '".$family_pensioner."', '".$relation."', '".$assigned_to."', '".$dispatched_to."', '".$priority."', '".$department."','" . $created_by . "','" . date('Y-m-d H:i:s') . "','" . $created_by . "','" . date('Y-m-d H:i:s') . "', '".$file_rack_location."')";
            // 
            $ece     = mysql_query($query);
            $id      = mysql_insert_id();           
            
        
            $key = $this->departmentNameKey($department);   
            $number_of_digits = $this->count_digit($id);    
            
            $uid     = $key . $_SESSION['fyear'] .$number_of_digits. $id;
            $link_UID  = "('".$uid."')";
        
            $update  = mysql_query("UPDATE `file_record` set `uid`='" . $uid . "' where id='" . $id . "'");
            $move_to = $dispatched_to ? $dispatched_to : $assigned_to;
            

            $track = mysql_query("INSERT INTO `track_file`(`record_id`, `file_no`, `created_by`, `move_from`, `from_depart`, `move_to`, `move_type`, `to_depart`, `department_id`, `priority`, `move_date`, `file_status`, `file_location`, `action`) VALUES ('" . $id . "', '" . $uid . "', '" . $created_by . "', '" . $created_by . "', '0', '" . $assigned_to . "', '" . $move_type . "', '" . $department . "', '" . $department . "', '" . $priority . "', '" . date('Y-m-d H:i:s') . "', '1', '".$department."', '2')"); 
           
            /*=======Activity=======*/
            // 1. Craeted  
            $activity = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','New file Created','1','0','" . $_SESSION['adminid'] . "','0 days 00:00','" . date('Y-m-d H:i:s') . "')");


            $lst_act = mysql_fetch_array(mysql_query("SELECT `date_created` FROM `activity_log` WHERE `file_no`='" . $uid . "' ORDER BY `id` DESC LIMIT 1"));
            
            $datetime1 = new DateTime();
            $datetime2 = new DateTime(substr($lst_act['date_created'], 0, 16));
            $interval  = $datetime1->diff($datetime2);
            $elapsed   = $interval->format('%a days %H:%I');

            //3. Assign, 
            $activity1 = mysql_query("INSERT INTO `activity_log`(`user_id`, `file_no`, `activity`, `activity_type`, `from_depart`, `from_user`, `to_depart`, `to_user`, `age`, `date_created`) VALUES ('" . $_SESSION['adminid'] . "','" . $uid . "','Assigning File','3','0','" . $_SESSION['adminid'] . "','" . $department . "','" . $assigned_to . "','" . $elapsed . "','" . date('Y-m-d H:i:s') . "')");
 
            $_SESSION['error']      = ADDNEWRECORD;
            $_SESSION['errorclass'] = ERRORCLASS;
            $_SESSION['check'] = "1";
            header("location:index.php?control=file_transfer&task=addnew");
            
        }else{
         $_SESSION['error']    = "<code style='font-size: 17px;'>No User Fond in this department, Please try again</code>";
         $_SESSION['errorclass'] = SUCCESSCLASS;
         header("location:index.php?control=file_transfer&task=addnew");
     }
    }


} 
?>

<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class hrm_departmentClass extends DbAccess {
		public $view='';
		public $name='hrm_department';

		
		
		function show(){	
		$uquery ="select * from department where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
	
		
		
		function save(){
			$name = strtoupper($_POST['name']);
			$key = strtoupper($_POST['key']);
			$date = date('Y-m-d');
			$id   = $_REQUEST['id'];
					if(!$id){
				
				  $query="insert into department (`name`,`key`,`date`,`status`) value('".$name."','".$key."','".$date."','1')";	
				$this->Query($query);	
				$this->Execute();
				
			$_SESSION['error'] = ADDNEWRECORD;	
            $_SESSION['errorclass'] = ERRORCLASS;
				header("location:index.php?control=hrm_department");
				}
				else
				{
				echo	$update="update department set `name`='".$name."', `key`='".$key."' where id='".$_REQUEST['id']."'";
					$this->Query($update);
					$this->Execute();
					
			$_SESSION['error'] = UPDATERECORD;	
            $_SESSION['errorclass'] = ERRORCLASS;
					header("location:index.php?control=hrm_department");
				}
		
		}
		
		
		
		function addnew() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  department WHERE id =".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
				else {
								
						require_once("views/".$this->name."/".$this->task.".php"); 
					}
		}
		
		function status(){
		$query="update department set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		//$this->show();	
		$_SESSION['error'] = ($_REQUEST['status']==0)?'Inactive':'Active';
            $_SESSION['errorclass'] = ERRORCLASS;
		
		header("location:index.php?control=hrm_department");
		}
		
		
		
		function delete(){
		
		$query="DELETE FROM department WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';
		//$this->show();
		header("location:index.php?control=hrm_department");
		
		}
		
		
	}

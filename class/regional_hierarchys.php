<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');

	}
	
	class regional_hierarchyClass extends DbAccess {
		public $view='';
		public $name='regional_hierarchy';
		
		
		/***************************************************** ZONE START **********************************************************/
		
		function show_country(){
			if($_REQUEST['search'])
			
			 {
		    $country_name=$_REQUEST['search'];
			
				$uquery ="select * from country WHERE country_name like '%".$country_name."%'";
			}else {		
		  $uquery ="select * from country where 1"; 
			}
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_country.php"); 
		}

		function addnew_country() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  country WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_country(){
			$country_name=$_POST['country_name'];
			if(!$_REQUEST['id']){
		
		$query="insert into country(country_name,status,datetime) value('".$country_name."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
		header("location:index.php?control=regional_hierarchy&task=show_country");
		}
		else
		{
			$update="update country set country_name='".$country_name."' where id='".$_REQUEST['id']."'";
			$this->Query($update);
			if($this->Execute()) {	
$_SESSION['error'] = UPDATERECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_country();
			//header("location:index.php?control=regional_hierarchy&task=show_country");
		}

		
		}
		
		function country_status(){
		$query = "update country set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
           $this->show_country();
		}
		
		function country_delete(){
		
		$query="DELETE FROM country WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
			if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
} 
 $this->show_country();
		}
		
		/****************************************** ZONE END **********************************************************/
		
		
		/***************************************** STATE START **********************************************************/
		
		function show_state(){
			if($_REQUEST['search'])
			
			 {
		    $state_name=$_REQUEST['search'];
			
				$uquery ="select * from state WHERE state_name like '%".$state_name."%'";
			}else	{
		 $uquery ="select * from state where 1";
			}
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_state.php"); 
		}
		
		function addnew_state() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  state WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_state(){
			$name=$_POST['state_name'];
			$country_id=$_POST['country_id'];
			if(!$_REQUEST['id']){
		
		$query="insert into state(state_name,country_id,status,datetime) value('".$name."','".$country_id."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
        
		header("location:index.php?control=regional_hierarchy&task=show_state");
		}
		else
		{
			$update="update state set state_name='".$name."',country_id='".$country_id."' where id='".$_REQUEST['id']."'";
			$this->Query($update);
				if($this->Execute()) {	
$_SESSION['error'] = UPDATERECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_state();
		}
		
		}
		
		function state_delete(){
		
		$query="DELETE FROM state WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
			if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_state();
		}
		
		function state_status(){
		$query = "update state set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_state();
		}
		
		/******************************************* STATE END **********************************************************/
		
		
		
		/*************************************** CITY START **********************************************************/
		
		function show_city(){
			if($_REQUEST['search'])
			
			 {
		    $city_name=$_REQUEST['search'];
			
				$uquery ="select * from city WHERE city_name like '%".$city_name."%'";
			}else	{	
		 $uquery ="select * from city where 1";
			}
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_city.php"); 
		}
		
		function addnew_city() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  city WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_city(){
			$country_id=$_POST['country_id'];
			$state_id=$_POST['state_id'];
			$city_name=$_POST['city_name'];
			if(!$_REQUEST['id']){
		
		 $query="insert into city(city_name,country_id,state_id,status,datetime) value('".$city_name."','".$country_id."','".$state_id."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
		header("location:index.php?control=regional_hierarchy&task=show_city");
		}
		else
		{
			$update="update city set city_name='".$city_name."',country_id='".$country_id."',state_id='".$state_id."' where id='".$_REQUEST['id']."'";
			$this->Query($update);
				if($this->Execute()) {	
$_SESSION['error'] = UPDATERECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_city();
		
		}
		
		}
		
		function city_status(){
		$query = "update city set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_city();
		}
		
		function city_delete(){
		
		$query="DELETE FROM city WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
			if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_city();
		}
		
		/***************************************** CITY END **********************************************************/
		
		
		
		
		/*************************************** AREA START **********************************************************/
		
		function show_area(){	
		 $uquery ="select * from area where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_area.php"); 
		}
		
		function addnew_area() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  area WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_area(){
			$area_name=$_POST['area_name'];     $amount = $_POST['amount'];
			$country_id=$_POST['country_id'];
			$state_id=$_POST['state_id'];
			$city_id=$_POST['city_id'];
			if(!$_REQUEST['id']){
		
		 $query="insert into area(area_name,city_id,country_id,state_id,status,amount,datetime) value('".$area_name."','".$city_id."','".$country_id."','".$state_id."','1','".$amount."','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
        
		header("location:index.php?control=regional_hierarchy&task=show_area");
		}
		else
		{
			$update="update area set area_name='".$area_name."', amount='".$amount."', city_id='".$city_id."',country_id='".$country_id."', state_id='".$state_id."' where id='".$_REQUEST['id']."'";
			$this->Query($update);
			$this->Execute();
					if($this->Execute()) {	
$_SESSION['error'] = UPDATERECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_area();
		
		}
		
		}
		
		function area_status(){
		$query = "update area set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_area();
		}
		
		function area_delete(){
		
		$query="DELETE FROM area WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
				if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_area();
		}
		
		/****************************************** AREA END **********************************************************/
	

		
			/******************************************Start Society  **********************************************************/
		function show_society(){	
		 $uquery ="select * from society where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		 require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function addnew_society() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  society WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_society(){
			$society_name=$_POST['society_name'];
			$area_id=$_POST['area_id'];
			$country_id=$_POST['country_id'];
			$state_id=$_POST['state_id'];
			$city_id=$_POST['city_id'];
			$owner_type=$_POST['owner_type'];
			if(!$_REQUEST['id']){
		
		 $query="insert into society(society_name,area_id,city_id,country_id,state_id,owner_type,status,datetime) value('".$society_name."','".$area_id."','".$city_id."','".$country_id."','".$state_id."','".$owner_type."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
        
		header("location:index.php?control=regional_hierarchy&task=show_society");
		}
		else
		{
				$update="update society set society_name='".$society_name."',area_id='".$area_id."',city_id='".$city_id."',country_id='".$country_id."',state_id='".$state_id."',owner_type='".$owner_type."' where id='".$_REQUEST['id']."'";
				$this->Query($update);
				$this->Execute();
				
				if($this->Execute()) {	
				$_SESSION['error'] = UPDATERECORD;	
				$_SESSION['errorclass'] = ERRORCLASS;
				}
				header("location:index.php?control=regional_hierarchy&task=show_society");
				
				}
		
		}
		
		function society_status(){
		$query = "update society set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_society();
		}
		
		function society_delete(){
		
		$query="DELETE FROM society WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
				if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_society();
		}
		
		
	
		/****************************************** Start Scheme END **********************************************************/	
		
		function show_scheme(){	
		 $uquery ="select * from scheme where 1"; 
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		 require_once("views/".$this->name."/show_scheme.php"); 
		}
		
		function addnew_scheme() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  scheme WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_scheme(){
			$scheme_type = $_POST['scheme_type'];			$registration_fee = $_POST['registration_fee'];
			$emi         = $_POST['emi'];         			         $rental  = $_POST['rental'];
			
			if(!$_REQUEST['id']){
		
		 $query="insert into scheme(scheme_type,registration_fee,emi,rental,status,datetime) value('".$scheme_type."','".$registration_fee."','".$emi."','".$rental."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
        
		header("location:index.php?control=regional_hierarchy&task=show_scheme");
		}
		else
		{
				$update="update scheme set scheme_type='".$scheme_type."', registration_fee='".$registration_fee."', emi='".$emi."', rental='".$rental."' where id='".$_REQUEST['id']."'";
				$this->Query($update);
				$this->Execute();
				
				if($this->Execute()) {	
				$_SESSION['error'] = UPDATERECORD;	
				$_SESSION['errorclass'] = ERRORCLASS;
				}
				header("location:index.php?control=regional_hierarchy&task=show_scheme");
				
				}
		
		}
		
		function scheme_status(){
		$query = "update scheme set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_scheme();
		}
		
		function scheme_delete(){
		
		$query="DELETE FROM scheme WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
				if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_scheme();
		}
		
		
		/******************************************End Scheme  **********************************************************/		
		
		
		
		
		/****************************************** Start New Scheme  **********************************************************/	
		
		function show_new_connection_scheme(){	
		 $uquery ="select * from new_connection_scheme where 1 order by id DESC"; 
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		 require_once("views/".$this->name."/show_new_connection_scheme.php"); 
		}
		
		function addnew_new_connection_scheme() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  new_connection_scheme WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_new_connection_scheme(){
			$society_id = $_POST['society_id'];			$area_id = $_POST['area_id'];
			$city_id    = $_POST['city_id'];       $rental  = $_POST['rental'];
			
			$city = mysql_fetch_array(mysql_query("select * from city where id='".$city_id."'"));
			
			
			$boxes = $_POST['chkbox'];
			
/*$scheme_id = array();
$p = array_key_exists('scheme_id',$_POST) ? $_POST['scheme_id'] : array();
for($v = 0; $v <= $boxes; $v++){
    if(array_key_exists($v,$p)){
        $scheme_id[$v] = trim(stripslashes($p[$v]));
    }else{
        $scheme_id[$v] = 'No';
    }
}

print_r($scheme_id);
exit;*/
			
			
			if(!$_REQUEST['id']){
				
		    $scheme_id = array();
			$p = array_key_exists('scheme_id',$_POST) ? $_POST['scheme_id'] : array();
			for($v = 0; $v <= $boxes; $v++){
				if(array_key_exists($v,$p)){
					$scheme_id[$v] = trim(stripslashes($p[$v]));
					 $query="insert into new_connection_scheme(scheme_id,society_id,area_id,city_id,country_id,state_id,status,datetime) value('".$scheme_id[$v]."', '".$society_id."', '".$area_id."', '".$city_id."', '".$city['country_id']."', '".$city['state_id']."', '1', '".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
		$this->Execute();
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
					
				}else{
					$scheme_id[$v] = 'No';
				}
			}
		
		

        
		header("location:index.php?control=regional_hierarchy&task=show_new_connection_scheme");
		}
		else
		{
			 $scheme_id = array();
			$p = array_key_exists('scheme_id',$_POST) ? $_POST['scheme_id'] : array();
			for($v = 0; $v <= $boxes; $v++){
				if(array_key_exists($v,$p)){
					$scheme_id[$v] = trim(stripslashes($p[$v]));
			
			
				$update="update new_connection_scheme set scheme_id='".$scheme_id[$v]."', society_id='".$society_id."', area_id='".$area_id."', city_id='".$city_id."', country_id='".$city['country_id']."', state_id='".$city['state_id']."' where id='".$_REQUEST['id']."'";
				$this->Query($update);
				$this->Execute();
			/*	if(!$this->Execute()) {	
				 $query= mysql_query("insert into new_connection_scheme(scheme_id, society_id, area_id, city_id, country_id, state_id, status, datetime) value('".$scheme_id[$v]."', '".$society_id."', '".$area_id."', '".$city_id."', '".$city['country_id']."', '".$city['state_id']."', '1', '".date("Y-m-d H:i:s")."')");	
		    }*/
				
				
					}else{
					$scheme_id[$v] = 'No';
				}
			}
		
				
			
				$_SESSION['error'] = UPDATERECORD;	
				$_SESSION['errorclass'] = ERRORCLASS;
				
				
				
				
				header("location:index.php?control=regional_hierarchy&task=show_new_connection_scheme");
				
				}
		
		}
		
		function new_connection_scheme_status(){
		$query = "update new_connection_scheme set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_new_connection_scheme();
		}
		
		function new_connection_scheme_delete(){
		
		$query="DELETE FROM new_connection_scheme WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
				if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_new_connection_scheme();
		}
		
		
		/******************************************End New Scheme  **********************************************************/	
		
		
		
		/***************************************** POST START **********************************************************/
		
		function show_post(){	
		 $uquery ="select * from post where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_post.php"); 
		}
		
		function addnew_post() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  post WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_post(){
			$post_name=$_POST['post_name'];
			if(!$_REQUEST['id']){
		
		$query="insert into post(post_name,status,datetime) value('".$post_name."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}

		header("location:index.php?control=regional_hierarchy&task=show_post");
		}
		else
		{
			$update="update post set post_name='".$post_name."' where id='".$_REQUEST['id']."'";
			$this->Query($update);
					if($this->Execute()) {	
$_SESSION['error'] = UPDATERECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_post();
			
		}

		
		}
		
		function post_delete(){
		
		$query="DELETE FROM post WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		if($this->Execute()) {	
          $_SESSION['error'] = UPDATERECORD;	
         $_SESSION['errorclass'] = ERRORCLASS;
			}
			$this->show_post();
		}
		
		/***************************************************** POST END **********************************************************/
	
		

		
		function show_excel(){	
			 
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
	
	}

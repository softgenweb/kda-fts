<?php
/*************************************************************************
php easy :: pagination scripts set - Version Three
==========================================================================
Author:      php easy code, www.phpeasycode.com
Web Site:    http://www.phpeasycode.com
Contact:     webmaster@phpeasycode.com
*************************************************************************/
function paginate_three($reload, $page, $tpages, $adjacents,$maxpage=NULL) {
	//echo $page;
	/*$prevlabel = "<<";
	$nextlabel = ">>";*/
	
	$out = "<div class=\"dataTables_paginate paging_simple_numbers\" id=\"example1-1_paginate\"><ul class=\"pagination\"><li class=\"paginate_button next \" id=\"example1-1_next\">\n";
	
	// previous
	if($page==1) {
		$out.= "<a  class=' gradient'>Previous</a>\n";
		// $out.= "<a  class=' gradient'>" . $prevlabel . "</a>\n";
	}
	elseif($page==2) {
		$out.= "<a  class=' gradient' onclick=paging('1') >Previous</a>\n";
		// $out.= "<a  class=' gradient' onclick=paging('1') >" . $prevlabel . "</a>\n";

		//$out.= "<a onclick='paging(".$reload.")' href=\"" . $reload . "\">" . $prevlabel . "</a>\n";
	}
	else {
		//$out.= "<a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a>\n";
		// $out.= "<a  class=' gradient' onclick=paging('". ($page-1) ."')  >" . $prevlabel . "</a></li>\n";
		$out.= "<a  class=' gradient' onclick=paging('". ($page-1) ."')  >Previous</a></li>\n";
	}
	
	// first
	if($page>($adjacents+1)) {
		$out.= "\n";
		// $out.= "<a  class=' ' onclick=paging('') >1</a>\n";
		//$out.= "<a href=\"" . $reload . "\">1</a>\n";
	}
	
	// interval
	if($page>($adjacents+2)) {
		//$out.= "...\n";
		$out .= '';//"...\n";
	}
	
	// pages
	//echo $page;
	$pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
	$pmax = $maxpage?$maxpage:(($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages);
	for($i=$pmin; $i<=$pmax; $i++) {
		if($i==$page) {
			$out.= "<li class=\"paginate_button active\"><a href=\"#\" aria-controls=\"example1-1\" data-dt-idx=". $i ." tabindex=\"0\">" . $i . "</a></li>\n";
		}
		elseif($i==1) {
			$out.= "<li class=\"paginate_button\"><a   class=' gradient' onclick=paging('" . $i . "')>" . $i . "</a></li>\n";
		}
		else {
			if(($i>$page-4) and ($i<$page+4) and ($i!=$pmax)) {
				$out.= "<li class=\"paginate_button\"><a  class=' gradient' onclick=paging('".$i."')>" . $i . "</a></li>\n";
			}
			if(($i==$pmax)) {
				$out.= "<li class=\"paginate_button\"><a class=' gradient' onclick=paging('".$i."')>" . $i . "</a></li>\n";
			}
		}
	}
	
	// interval
	if($page <($tpages-$adjacents-1)) {
		//$out.= "...";
		//$out .= '';//"...\n";
	}
	
	// last
	if($page<($tpages-$adjacents)) {
		//$out .= "<a onclick=paging('". $tpages."')></a>\n";
	}
	
	// next
	//echo $maxpage; 
	if($page<$maxpage) {
		$out .= "<li next class=\"paginate_button\"><a class=' gradient' onclick=paging('".($page+1)."')>Next</a></li>\n";
	}
	else {
		$out .= "<li class=\"paginate_button next \" id=\"example1-1_next\"><a  class=' gradient'>Next</a></li>\n";
	}
	
	$out .= "</ul></div>";
	
	return $out;
}
?>
<?php
   ini_set("display_errors","Off");
   include('configuration.php');
   $id = $_REQUEST['id'];
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>FTS</title>
      <link rel="icon" href="./images/favicon.png" type="image/png" sizes="16x16">
      <!-- Bootstrap -->
      <!-- <script src="styles/js/pace.js"></script> -->
      <link href="styles/css/bootstrap.css" rel="stylesheet">
      <link href="styles/css/theme.css" rel="stylesheet">
      <link href="styles/css/font-awesome.css" rel="stylesheet">
      <link href="styles/css/animate.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:700,400|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
      <link href="styles/css/theme-loading-bar.css" rel="stylesheet" />
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
         @media (min-width: 320px) and (max-width: 767px) {
         .navbar-brand.top-navbar-brand.cmlogo {
         position:relative;
         top:9px !important;
         }

         }
   
      </style>
      <script src="biomatric/jquery-1.8.2.js"></script>
      <script src="biomatric/mfs100-9.0.2.6.js"></script>
   </head>
   <body>
      <?php 
         $response = array();
         
         		//$sql="SELECT * FROM user ORDER BY id";
         		 $sql    = "SELECT * FROM users where base64iostemp!='' and id='".$id."' ORDER BY id";
         		  $result = mysql_query($sql);
         		 $rowcount = mysql_num_rows($result);
         		 
         		$rowl = 1;
         
         		$fingerdate = "";
         		$fingerdateid = "";
         		$i = 1;
         		 
         		while($row=mysql_fetch_array($result)){
         
         			$fingerdate .= $fingerdate != "" ? ",".$row['base64iostemp'] : $row['base64iostemp'];
         			$fingerdateid .= $fingerdateid != "" ? ",".$row['id'] : $row['id'];
         
         			 
         			if($rowl == 10){
         				$tmp = array();
         				$tmp['id']=$fingerdateid;
         				$tmp['fdata']=$fingerdate;
         				array_push($response, $tmp);
         				$fingerdate = "";
         				$fingerdateid = "";
         				$rowl=1;
         			}else if($i == $rowcount){
         				$tmp = array();
         				$tmp['id']=$fingerdateid;
         				$tmp['fdata']=$fingerdate;
         				array_push($response, $tmp);
         				$fingerdate = "";
         				$fingerdateid = "";
         				$rowl=1;
         			}else{						
         				$rowl++;
         			}
         			$i++;
         
         			
         		} 
         		//print_r($idarr);
         
         		?>
      <div class="container" id="container">
         <header>
            <!-- Main comapny header -->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
               <div class="container">
                  <div class="navbar-header">
                     <a class="navbar-brand top-navbar-brand cmlogo" href="#">
                        <!--<img src="images/CMlogo.png" class="animated bounce"/>-->
                        <!--<img src="../wp-content/uploads/2016/11/ggllogo.png"/>-->
                     </a>
                  </div>
                  <!--	<ul class="nav navbar-nav navbar-right bigger-130">
                     <li><a href="#"><i class="fa fa-google-plus-square" style="color:#DE4E3C"></i></a></li>
                     <li><a href="#"><i class="fa fa-facebook-square" style="color:#3C599F"></i></a></li>
                     <li><a href="#"><i class="fa fa-twitter-square" style="color:#5EA9DD"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-square" style="color:#D73532"></i></i></a></li>
                     </ul>-->
               </div>
            </nav>
         </header>
         <section id="form" class="animated fadeInDown">
            <div class="container">
               <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                  <div class="panel white-alpha-90" >
                     <div class="panel-heading">
                        <div class="panel-title text-center"><img src="images/logo.png" width="150"/></div>
                     </div>
                     <?php 
                        if($_REQUEST['error']==1){ ?>
                     <div style="color:#F00; text-align:center; font-size:14px; font-weight:bold;" >*Invalid Thumb !!</div>
                     <?php }
                        else  if($_REQUEST['error']==2){ ?>
                     <div style="color:#F00; text-align:center; font-size:14px; font-weight:bold;" >*You are not a authorised user.</div>
                     <?php }?>                       
                     <div class="panel-body" >
                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                        <form id="loginform" class="form-horizontal" role="form" action="authen_scan.php" method="post">

                           <div class="input-group col-xs-12 text-center" id="thumb" style="display:none">
                              <h3 style="margin-top: -20px;">Please place your finger on scanner</h3><img id="default_img" width="145px" height="188px" alt="&nbsp;"  src="images/thumb.gif"/>
                              <img id="imgFinger_verify" width="145px" height="188px" alt="&nbsp;" name="imgFinger" />
                              <input type="hidden" name="hd_imgFinger_verify" id="hd_imgFinger_verify">
                              <input type="hidden" name="id" id="id" value="">
                           </div>
                           <div class="input-group col-xs-12 text-center login-action">
                              <div class="checkbox">
                                 <label>
                                    <!-- <span class="forget"><a href="forgotpassword.php">Forgot password?   </a></span>&nbsp;
                                       <span id="btn-login"><a href="#" class="btn btn-warning">Login  </a></span>-->
                                    <span id="btn-login">
                                    <input type="submit" name="btn_save_capture_data" class="btn btn-warning"  value="Save"  id="btn_save_capture_data"  style="display:none;">
                                    <a href="javascript:;" class="btn btn-warning" onclick="verify_thumb()">Verify</a>
                                    </span> 
                                 </label>
                              </div>
                           </div>
                           <div style="margin-top:10px" class="form-group">
                              <div class="col-sm-12 controls">
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <footer>
            <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
               <div class="container text-center">
                  <div class="footer-content">
                     <center><span> &copy; 2018 | FTS</span></center>
                  </div>
               </div>
               <!-- /.container-fluid -->
            </nav>
         </footer>
      </div>
      <script language="javascript" type="text/javascript">
         var jqueryarray = <?php echo json_encode($response); ?>;
         
         
         
         
                var quality = 60; //(1 to 100) (recommanded minimum 55)
                var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )
         
                $('#sec_get_info').hide();
                $('#sec_capture_data').hide();
                function GetInfo() {
                	$('#txt_serial_no').val(''); 
                	$('#txt_certificate').val(''); 
                	$('#txt_make').val(''); 
                	$('#txt_model').val(''); 
                	$('#txt_width').val(''); 
                	$('#txt_height').val(''); 
                	$('#txt_localip').val(''); 
                	$('#txt_localmac').val(''); 
                	$('#txt_publicip').val(''); 
                	$('#txt_systemid').val(''); 
         
                	$('#hd_serial_no').val(''); 
                	$('#hd_certificate').val(''); 
                	$('#hd_make').val(''); 
                	$('#hd_model').val(''); 
                	$('#hd_width').val(''); 
                	$('#hd_height').val(''); 
                	$('#hd_localip').val(''); 
                	$('#hd_localmac').val(''); 
                	$('#hd_publicip').val(''); 
                	$('#hd_systemid').val(''); 
         
         
         
                	$('#sec_get_info').show();
                	$('#sec_capture_data').hide();
         
                	var key = "";
         
                	var res;
                	if (key.length == 0) {
                		res = GetMFS100Info();
                	}
                	else {
                		res = GetMFS100KeyInfo(key);
                	}
         
                	if (res.httpStaus) {
         
                        //document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;
         
                        if (res.data.ErrorCode == "0") {                 
         
         
                        	$('#txt_serial_no').val(res.data.DeviceInfo.SerialNo); 
                        	$('#txt_certificate').val(res.data.DeviceInfo.Certificate); 
                        	$('#txt_make').val(res.data.DeviceInfo.Make); 
                        	$('#txt_model').val(res.data.DeviceInfo.Model); 
                        	$('#txt_width').val(res.data.DeviceInfo.Width); 
                        	$('#txt_height').val(res.data.DeviceInfo.Height); 
                        	$('#txt_localip').val(res.data.DeviceInfo.LocalIP); 
                        	$('#txt_localmac').val(res.data.DeviceInfo.LocalMac); 
                        	$('#txt_publicip').val(res.data.DeviceInfo.PublicIP); 
                        	$('#txt_systemid').val(res.data.DeviceInfo.SystemID); 
         
                        	$('#hd_serial_no').val(res.data.DeviceInfo.SerialNo); 
                        	$('#hd_certificate').val(res.data.DeviceInfo.Certificate); 
                        	$('#hd_make').val(res.data.DeviceInfo.Make); 
                        	$('#hd_model').val(res.data.DeviceInfo.Model); 
                        	$('#hd_width').val(res.data.DeviceInfo.Width); 
                        	$('#hd_height').val(res.data.DeviceInfo.Height); 
                        	$('#hd_localip').val(res.data.DeviceInfo.LocalIP); 
                        	$('#hd_localmac').val(res.data.DeviceInfo.LocalMac); 
                        	$('#hd_publicip').val(res.data.DeviceInfo.PublicIP); 
                        	$('#hd_systemid').val(res.data.DeviceInfo.SystemID); 
         
                        	$('#btn_save_info').click();
                        }
                    }
                    else {
                    	alert(res.err);
                    }
                    return false;
                }
         
                function Capture() {
                	try {
         
                		$('#sec_get_info').hide();
                		$('#sec_capture_data').show();
         
         
                		$('#txt_quality').val('');
                		$('#txt_base64iostemp').val('');
                		$('#txt_base64ansitemp').val('');
                		$('#txt_base64iosimg').val('');
                		$('#txt_base64rawdata').val('');
                		$('#txt_base64wsqimgdata').val(''); 
         
                		$('#hd_quality').val('');
                		$('#hd_base64iostemp').val('');
                		$('#hd_base64ansitemp').val('');
                		$('#hd_base64iosimg').val('');
                		$('#hd_base64rawdata').val('');
                		$('#hd_base64wsqimgdata').val(''); 
         
         
         
                		var res = CaptureFinger(quality, timeout);
                		if (res.httpStaus) {
         
                           // document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;
         
                           if (res.data.ErrorCode == "0") {
                           	document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;
         
                           	$('#hd_imgFinger').val("data:image/bmp;base64," + res.data.BitmapData);
         
                           	var imageinfo = "Quality: " + res.data.Quality + " Nfiq: " + res.data.Nfiq + " W(in): " + res.data.InWidth + " H(in): " + res.data.InHeight + " area(in): " + res.data.InArea + " Resolution: " + res.data.Resolution + " GrayScale: " + res.data.GrayScale + " Bpp: " + res.data.Bpp + " WSQCompressRatio: " + res.data.WSQCompressRatio + " WSQInfo: " + res.data.WSQInfo;
         
         
         
                           	$('#txt_quality').val(imageinfo);
                           	$('#txt_base64iostemp').val(res.data.IsoTemplate);
                           	$('#txt_base64ansitemp').val(res.data.AnsiTemplate);
                           	$('#txt_base64iosimg').val(res.data.IsoImage);
                           	$('#txt_base64rawdata').val(res.data.RawData);
                           	$('#txt_base64wsqimgdata').val(res.data.WsqImage); 
         
                           	$('#hd_quality').val(imageinfo);
                           	$('#hd_base64iostemp').val(res.data.IsoTemplate);
                           	$('#hd_base64ansitemp').val(res.data.AnsiTemplate);
                           	$('#hd_base64iosimg').val(res.data.IsoImage);
                           	$('#hd_base64rawdata').val(res.data.RawData);
                           	$('#hd_base64wsqimgdata').val(res.data.WsqImage); 
         
         
                         	//$('#btn_save_capture_data').click();
         
         
         
                           }
                       }
                       else {
                       	alert(res.err);
                       }
                   }
                   catch (e) {
                   	alert(e);
                   }
                   return false;
               }
            
            
            
               function Match() {
               	try {
               		var logs = 0;
               		$.each(jqueryarray, function () {
         
               			var isotemplate = this.fdata;
         //alert(isotemplate);
               			var res = MatchFinger(quality, timeout, isotemplate);
         if(res.data.Status==true){
               			if (res.httpStaus) {
               				if (res.data.Status) {
               					alert("Finger matched");
               					alert(this.id);
               					logs = 1;
               					return false;
               				}
               				// else {
               				// 	if (res.data.ErrorCode != "0") {
               				// 		alert(res.data.ErrorDescription);
               				// 	}
               				// 	else {
               				// 		alert("Finger not matched");
               				// 	}
               				// }
               			}
               			else {
               				alert(res.err);
               			}
         }
               		});
               		if(logs == 0){
               			alert("Finger not matched");
               		}
               	}
               	catch (e) {
               		alert(e);
               	}
         
               	return false;
         
               }
         
         
         /*
         
         function verify(){
                	try{   
         
         			var res = CaptureFinger(quality, timeout);
         			if (res.httpStaus) {
         				if (res.data.ErrorCode == "0") {
         					var probTemplate= res.data.IsoTemplate; 
         					var logs = 0;
         					$.each(jqueryarray, function () {
         
         						var galleryTemplate= this.fdata;
         
         						var res = VerifyFinger(probTemplate, galleryTemplate); 
         						 
         
         						if (res.httpStaus) { 
         							if (res.data.Status) { 
         								alert("Finger matched & Verify");
         		       					alert(this.id);
         		       					logs = 1;
         								return false;
         
         							}  
         						} else {   
         							alert(res.err); 
         						}
         					});
         					if(logs == 0){
         		       			alert("Finger not matched");
         		       		}
         
         				}
         			}
         			else {
         				alert(res.err);
         			} 
         
         		} catch(e){
         			alert(e);
         		}
         		return false;
         	}*/
         
         
         
         
         
         // For Verify Code
           function verify_thumb(){
			   $('#thumb').show();
			   setTimeout(verify, 1000);
			   
		   }
			   
         function verify(){
               	try{   
         	
         	        $('#sec_get_info').hide();
                		$('#sec_capture_data_verify').show();
         
         
        /*       		var res = CaptureFinger(quality, timeout);
         		document.getElementById('default_img').style.display='none';
         			document.getElementById('imgFinger_verify').src = "data:image/bmp;base64," + res.data.BitmapData;
                     	$('#hd_imgFinger_verify').val("data:image/bmp;base64," + res.data.BitmapData);*/
                       
       var res = CaptureFinger(quality, timeout);
       if(res.data.BitmapData == undefined || res.data.BitmapData == null){
          alert('No finger print found, Please Try Again !!!');
       }else{
          document.getElementById('default_img').style.display='none';
		   $('#imgFinger_verify').show();
         // document.getElementById('imgFinger_verify').style.display = 'block';
          document.getElementById('imgFinger_verify').src = "data:image/bmp;base64," + res.data.BitmapData;
          $('#hd_imgFinger_verify').val("data:image/bmp;base64," + res.data.BitmapData); 
       }       			
         		
               		if (res.httpStaus) {		
               			if (res.data.ErrorCode == "0") {				    	
         
         				
               				var probTemplate= res.data.IsoTemplate; 
               				var logs = 0; 
         					$.each(jqueryarray, function () {
         
         						var galleryTemplate= this.fdata;
         						var fingeridcom = this.id;
         
         						var res = VerifyFinger(probTemplate, galleryTemplate); 
         
         
         						if (res.httpStaus) { 
         							if (res.data.Status) { 
         
         								var arrfig = galleryTemplate.split(',');
         								var arrfigid = fingeridcom.split(',');
         									console.log(arrfig);
         								for(var inneri=0; inneri < 10; inneri++ ){
         									var res = VerifyFinger(probTemplate, arrfig[inneri]); 
         
         									if (res.httpStaus) { 
         										if (res.data.Status) {
         											//alert("Finger matched");
         											//alert(arrfigid[inneri]);
         											document.getElementById('id').value = arrfigid[inneri];
         											logs = 1;	
         								$('#btn_save_capture_data').click(); 
         							// document.getElementById("loginform").submit(); 
         											return false;
         										}
         									}
         								} 
         								//logs = 1;
         								//return false;
         								
         							
         
         							} 	
         							
         						
         						} else {   
         							alert(res.err); 
         						}
         					});
         					if(logs == 0){
         						alert("Finger not matched");
         					}
         				}
         			}
         			else {
         				alert(res.err);
         			} 
         	    
         
         		} catch(e){
         			alert(e);
         		}
         		return false;
         	}
         
         
           
      </script>
      <script src="styles/js/jquery.min.js"></script>
      <script src="styles/js/bootstrap.min.js"></script>
      <script src="styles/js/jquery.backstretch.min.js"></script>
      <script>
         Pace.on('hide', function(){
           $("#container").fadeIn('1000'); 
         
           $.backstretch([
         		"styles/images/bg11111.jpg",
         		"styles/images/bg22111.jpg"
         	], {duration: 5000, fade: 1000});
         });
         
         
         
      </script>
   </body>
</html>


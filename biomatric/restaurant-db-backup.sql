

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_ip` varchar(200) NOT NULL,
  `activity` varchar(220) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

INSERT INTO activity_log VALUES("1","192.168.1.34","Login by admin","1","2018-12-22 07:26:30","1");
INSERT INTO activity_log VALUES("2","192.168.1.34","Update Company Details by admin","1","2018-12-22 12:11:42","1");
INSERT INTO activity_log VALUES("3","192.168.1.34","Update Payment Mode (CASH) by admin","1","2018-12-22 12:12:51","1");
INSERT INTO activity_log VALUES("4","192.168.1.34","Add Payment Mode (PAYTM) by admin","1","2018-12-22 12:13:25","1");
INSERT INTO activity_log VALUES("5","192.168.1.34","Update Tax (0%) by admin","1","2018-12-22 12:14:00","1");
INSERT INTO activity_log VALUES("6","192.168.1.34","Add Tax (123%) by admin","1","2018-12-22 12:20:39","1");
INSERT INTO activity_log VALUES("7","192.168.1.34","Update Tax (18%) by admin","1","2018-12-22 12:20:57","1");
INSERT INTO activity_log VALUES("8","192.168.1.34","Add Discount (5%) by admin","1","2018-12-22 12:30:19","1");
INSERT INTO activity_log VALUES("9","192.168.1.34","Update Discount (Other Amount%) by admin","1","2018-12-22 12:30:54","1");
INSERT INTO activity_log VALUES("10","192.168.1.34","Update detail of Staff (Gaurav Kumar Singh/73763860056)  by admin","1","2018-12-22 13:20:30","1");
INSERT INTO activity_log VALUES("11","192.168.1.34","Add Item (NIMBU PANI) of Price 50/- With Discount 0/- by admin","1","2018-12-22 13:23:50","1");
INSERT INTO activity_log VALUES("12","192.168.1.34","Update Item (GFHGFH) of Price 100/- With Discount 1/- by admin","1","2018-12-22 13:24:52","1");
INSERT INTO activity_log VALUES("13","192.168.1.34","Add New Bill (BILL1819003) of total amount:2425.65","1","2018-12-22 13:25:54","1");
INSERT INTO activity_log VALUES("14","192.168.1.34","Login by admin","1","2018-12-22 13:37:01","1");
INSERT INTO activity_log VALUES("15","192.168.1.34","Update Bill (BILL1819003) of total amount:2688.12","1","2018-12-22 15:02:03","1");
INSERT INTO activity_log VALUES("16","192.168.1.34","Update Bill (BILL1819003) of total amount:2409.08","1","2018-12-22 15:02:38","1");
INSERT INTO activity_log VALUES("17","192.168.1.34","Update Bill (BILL1819003) of total amount:3365.53","1","2018-12-22 15:03:11","1");
INSERT INTO activity_log VALUES("18","192.168.1.34","Update Bill (BILL1819003) of total amount:2688.75","1","2018-12-22 15:11:05","1");
INSERT INTO activity_log VALUES("19","192.168.1.34","Login by admin","1","2018-12-25 10:36:30","1");
INSERT INTO activity_log VALUES("20","192.168.1.34","Change Status of payment mode (2)  by admin","1","2018-12-25 11:50:27","1");
INSERT INTO activity_log VALUES("21","192.168.1.34","Change Status of Tax (2) by admin","1","2018-12-25 11:53:26","1");
INSERT INTO activity_log VALUES("22","192.168.1.34","Change Status of Tax (2) by admin","1","2018-12-25 11:53:39","1");
INSERT INTO activity_log VALUES("23","192.168.1.34","Change status of Discount (5) by admin","1","2018-12-25 11:54:09","1");
INSERT INTO activity_log VALUES("24","192.168.1.34","Change status of Discount (5) by admin","1","2018-12-25 11:54:20","1");
INSERT INTO activity_log VALUES("25","192.168.1.34","Login by admin","1","2018-12-25 17:23:40","1");
INSERT INTO activity_log VALUES("26","192.168.1.33","Login by admin","1","2018-12-26 10:44:11","1");
INSERT INTO activity_log VALUES("27","192.168.1.35","Login by admin","1","2018-12-27 11:13:52","1");
INSERT INTO activity_log VALUES("28","192.168.1.35","Login by admin","1","2018-12-27 12:00:02","1");
INSERT INTO activity_log VALUES("29","192.168.1.35","Add Item (SDFDSFSDF) of Price 12/- With Discount 1/- by admin","1","2018-12-27 12:00:19","1");
INSERT INTO activity_log VALUES("30","192.168.1.35","Add Item (TEST) of Price 123/- With Discount 15/- by admin","1","2018-12-27 12:14:27","1");
INSERT INTO activity_log VALUES("31","192.168.1.35","Add Payment Mode (GIFT CARD) by admin","1","2018-12-27 13:34:39","1");
INSERT INTO activity_log VALUES("32","192.168.1.35","Add Payment Mode (COUPON) by admin","1","2018-12-27 13:35:11","1");
INSERT INTO activity_log VALUES("33","192.168.1.35","Add Payment Mode (ZOMATO) by admin","1","2018-12-27 13:35:26","1");
INSERT INTO activity_log VALUES("34","192.168.1.35","Add Payment Mode (FOODPANDA) by admin","1","2018-12-27 13:35:46","1");
INSERT INTO activity_log VALUES("35","192.168.1.35","Add Payment Mode (GOOGLE PAY) by admin","1","2018-12-27 13:36:09","1");
INSERT INTO activity_log VALUES("36","192.168.1.35","Add Payment Mode (PHONEPE) by admin","1","2018-12-27 13:36:24","1");
INSERT INTO activity_log VALUES("37","192.168.1.35","Add Payment Mode (BHIM UPI) by admin","1","2018-12-27 13:36:43","1");
INSERT INTO activity_log VALUES("38","192.168.1.35","Add Payment Mode (COD) by admin","1","2018-12-27 13:37:07","1");
INSERT INTO activity_log VALUES("39","192.168.1.35","Add Payment Mode (SWIGGY) by admin","1","2018-12-27 14:48:33","1");
INSERT INTO activity_log VALUES("40","192.168.1.35","Update Bill (BILL1819002) of total amount:1848.25","1","2018-12-27 16:29:06","1");
INSERT INTO activity_log VALUES("41","192.168.1.35","Add New Bill (BILL1819004) of total amount:2044.87","1","2018-12-27 18:08:55","1");
INSERT INTO activity_log VALUES("42","192.168.1.35","Update Bill (BILL1819004) of total amount:2044.87","1","2018-12-27 18:10:47","1");
INSERT INTO activity_log VALUES("43","192.168.1.35","Update Bill (BILL1819004) of total amount:2046.22","1","2018-12-27 18:16:12","1");
INSERT INTO activity_log VALUES("44","192.168.1.35","Update Bill (BILL1819004) of total amount:2046.22","1","2018-12-27 18:22:25","1");
INSERT INTO activity_log VALUES("45","192.168.1.35","Update Bill (BILL1819004) of total amount:2046.22","1","2018-12-27 18:26:06","1");
INSERT INTO activity_log VALUES("46","192.168.1.35","Update Bill (BILL1819004) of total amount:2046.22","1","2018-12-27 18:26:39","1");
INSERT INTO activity_log VALUES("47","192.168.1.35","Update Bill (BILL1819004) of total amount:2046.22","1","2018-12-27 18:27:07","1");
INSERT INTO activity_log VALUES("48","::1","Login by admin","1","2018-12-28 11:59:22","1");
INSERT INTO activity_log VALUES("49","192.168.1.35","Login by admin","1","2018-12-28 12:02:15","1");
INSERT INTO activity_log VALUES("50","192.168.1.33","Login by admin","1","2018-12-29 11:03:12","1");
INSERT INTO activity_log VALUES("51","192.168.1.33","Login by admin","1","2019-01-02 12:16:28","1");
INSERT INTO activity_log VALUES("52","192.168.1.33","Add New Bill (BILL1819005) of total amount:1275.00","1","2019-01-02 16:05:59","1");
INSERT INTO activity_log VALUES("53","192.168.1.33","Add New Bill (BILL1819005) of total amount:2125.00","1","2019-01-02 16:08:09","1");
INSERT INTO activity_log VALUES("54","192.168.1.33","Add New Bill (BILL1819006) of total amount:3887.84","1","2019-01-02 16:10:41","1");
INSERT INTO activity_log VALUES("55","192.168.1.33","Add New Bill (BILL1819007) of total amount:920.08","1","2019-01-02 16:11:29","1");
INSERT INTO activity_log VALUES("56","192.168.1.33","Login by admin","1","2019-01-02 17:15:00","1");
INSERT INTO activity_log VALUES("57","192.168.1.33","Add New Bill (KSFW1819008) of total amount:93.66","1","2019-01-02 17:50:52","1");
INSERT INTO activity_log VALUES("58","192.168.1.33","Login by admin","1","2019-01-02 18:32:57","1");
INSERT INTO activity_log VALUES("59","192.168.1.35","Login by admin","1","2019-01-03 10:30:42","1");
INSERT INTO activity_log VALUES("60","192.168.1.35","Login by admin","1","2019-01-03 11:14:06","1");
INSERT INTO activity_log VALUES("61","192.168.1.35","Login by admin","1","2019-01-03 12:11:58","1");
INSERT INTO activity_log VALUES("62","192.168.1.35","Login by admin","1","2019-01-03 13:11:36","1");
INSERT INTO activity_log VALUES("63","192.168.1.34","Login by admin","1","2019-01-04 10:21:41","1");
INSERT INTO activity_log VALUES("64","192.168.1.34","Login by admin","1","2019-01-04 13:46:10","1");
INSERT INTO activity_log VALUES("65","192.168.1.34","Add New Bill (KSFW1819009) of total amount:85.00","1","2019-01-04 14:00:11","1");
INSERT INTO activity_log VALUES("66","192.168.1.34","Add New Bill (KSFW18190010) of total amount:188.00","1","2019-01-04 14:04:29","1");
INSERT INTO activity_log VALUES("67","192.168.1.34","Login by admin","1","2019-01-04 14:05:44","1");
INSERT INTO activity_log VALUES("68","192.168.1.34","Add New Bill (KSFW18190011) of total amount:80.00","1","2019-01-04 15:02:36","1");
INSERT INTO activity_log VALUES("69","192.168.1.34","Add New Bill (KSFW18190012) of total amount:85.00","1","2019-01-04 15:03:53","1");
INSERT INTO activity_log VALUES("70","192.168.1.34","Add New Bill (KSFW18190013) of total amount:94.00","1","2019-01-04 15:08:15","1");
INSERT INTO activity_log VALUES("71","192.168.1.34","Login by admin","1","2019-01-04 15:15:12","1");
INSERT INTO activity_log VALUES("72","192.168.1.34","Add New Bill (KSFW18190014) of total amount:188.00","1","2019-01-04 15:39:16","1");
INSERT INTO activity_log VALUES("73","192.168.1.34","Login by admin","1","2019-01-07 15:37:09","1");
INSERT INTO activity_log VALUES("74","192.168.1.41","Login by admin","1","2019-01-07 16:14:09","1");
INSERT INTO activity_log VALUES("75","192.168.1.34","Login by admin","1","2019-01-07 16:23:11","1");
INSERT INTO activity_log VALUES("76","192.168.1.41","Login by admin","1","2019-01-07 16:42:56","1");
INSERT INTO activity_log VALUES("77","192.168.1.41","Add New Bill (KSFW18190015) of total amount:176.72","1","2019-01-07 17:11:04","1");
INSERT INTO activity_log VALUES("78","192.168.1.41","Add New Bill (KSFW18190016) of total amount:188.00","1","2019-01-07 17:11:47","1");
INSERT INTO activity_log VALUES("79","192.168.1.41","Add New Bill (KSFW18190017) of total amount:188.00","1","2019-01-07 17:13:27","1");
INSERT INTO activity_log VALUES("80","192.168.1.41","Add New Bill (KSFW18190018) of total amount:188.00","1","2019-01-07 17:14:06","1");
INSERT INTO activity_log VALUES("81","192.168.1.41","Add New Bill (KSFW18190019) of total amount:188.00","1","2019-01-07 17:14:53","1");
INSERT INTO activity_log VALUES("82","192.168.1.41","Add New Bill (KSFW18190020) of total amount:188.00","1","2019-01-07 17:15:32","1");
INSERT INTO activity_log VALUES("83","192.168.1.41","Add New Bill (KSFW18190021) of total amount:188.00","1","2019-01-07 17:18:53","1");
INSERT INTO activity_log VALUES("84","192.168.1.41","Add New Bill (KSFW18190022) of total amount:85.00","1","2019-01-07 17:29:26","1");
INSERT INTO activity_log VALUES("85","192.168.1.41","Login by admin","1","2019-01-09 11:30:36","1");
INSERT INTO activity_log VALUES("86","192.168.1.43","Login by admin","1","2019-01-09 11:53:56","1");
INSERT INTO activity_log VALUES("87","192.168.1.43","Login by admin","1","2019-01-09 12:01:33","1");
INSERT INTO activity_log VALUES("88","192.168.1.43","Login by admin","1","2019-01-09 12:02:51","1");
INSERT INTO activity_log VALUES("89","192.168.1.35","Login by admin","1","2019-01-19 11:23:17","1");
INSERT INTO activity_log VALUES("90","::1","Login by admin","1","2019-01-19 13:44:45","1");
INSERT INTO activity_log VALUES("91","192.168.1.34","Login by admin","1","2019-01-21 10:34:01","1");
INSERT INTO activity_log VALUES("92","192.168.1.34","Login by admin","1","2019-01-21 12:41:13","1");
INSERT INTO activity_log VALUES("93","192.168.1.34","Update Item Category (Test) by admin","1","2019-01-21 13:18:13","1");
INSERT INTO activity_log VALUES("94","192.168.1.34","Update Item Category (Starter) by admin","1","2019-01-21 13:27:18","1");
INSERT INTO activity_log VALUES("95","192.168.1.34","Add Item Category (Combo) by admin","1","2019-01-21 13:27:30","1");
INSERT INTO activity_log VALUES("96","192.168.1.34","Add Item Category (Normal) by admin","1","2019-01-21 13:27:41","1");
INSERT INTO activity_log VALUES("97","192.168.1.34","Change Status of Product Category (2) by admin","1","2019-01-21 13:28:23","1");
INSERT INTO activity_log VALUES("98","192.168.1.34","Change Status of Product Category (2) by admin","1","2019-01-21 13:29:05","1");
INSERT INTO activity_log VALUES("99","192.168.1.34","Change Status of Product Category (3) by admin","1","2019-01-21 13:29:11","1");
INSERT INTO activity_log VALUES("100","192.168.1.34","Update Item (BRIYANI) of Price 1000/- With Discount 150/- by admin","1","2019-01-21 13:36:16","1");





CREATE TABLE `bill_discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `percent` varchar(100) NOT NULL,
  `remark` varchar(225) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='bill discounts';

INSERT INTO bill_discount VALUES("1","0","","2018-12-19 11:13:01","1");
INSERT INTO bill_discount VALUES("2","6","","2018-12-19 11:13:42","1");
INSERT INTO bill_discount VALUES("3","12","","2018-12-19 11:13:53","1");
INSERT INTO bill_discount VALUES("4","15","","2018-12-19 11:13:53","1");
INSERT INTO bill_discount VALUES("5","Other Amount","only for other amount","2018-12-19 11:14:11","1");
INSERT INTO bill_discount VALUES("6","5","","2018-12-22 12:30:19","1");





CREATE TABLE `bill_fare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_no` varchar(220) NOT NULL,
  `token_no` int(11) NOT NULL,
  `customer_name` varchar(220) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `total_item` int(11) NOT NULL COMMENT 'number of total product/particuler',
  `total_qty` varchar(220) NOT NULL,
  `tax_percent` varchar(100) NOT NULL,
  `total_tax` varchar(200) NOT NULL,
  `disc_percent` varchar(100) NOT NULL,
  `total_discount` varchar(220) NOT NULL,
  `total_amount` varchar(220) NOT NULL,
  `grand_total` varchar(220) NOT NULL,
  `financial_year` varchar(220) NOT NULL,
  `payment_mode` int(11) NOT NULL,
  `card_number` bigint(100) NOT NULL COMMENT 'card last 4 number',
  `date_created` datetime NOT NULL,
  `date_modify` varchar(220) NOT NULL,
  `emp_id` bigint(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

INSERT INTO bill_fare VALUES("1","BILL1819001","1","Gaurav Singh","7376386005","05-08-1995","4","43","6","299.28","6","317.24","4988.00","4970.04","181900","3","0","2018-12-22 11:03:37","2018-12-22 11:35:30","1","1");
INSERT INTO bill_fare VALUES("2","BILL1819002","2","Uday Veer Singh","8299431080","22-12-1985","3","18","6","118.56","12","246.31","1976.00","1848.25","181900","3","0","2018-12-22 11:07:54","2018-12-27 16:29:06","1","1");
INSERT INTO bill_fare VALUES("3","BILL1819003","3","Ritesh Singh","9898989989","06-06-1992","4","17","12","306.60","6","172.85","2555.00","2688.75","181900","4","0","2018-12-22 13:25:54","2018-12-22 15:11:05","1","1");
INSERT INTO bill_fare VALUES("4","BILL1819004","1","Test Bill ","8989899665","25-06-1996","3","18","12","248.64","12","274.42","2072.00","2046.22","181900","2","0","2018-12-27 18:08:55","2018-12-27 18:27:07","1","1");
INSERT INTO bill_fare VALUES("6","BILL1819006","1","Gaurav","9879879879","56-46-5465","1","22","0","0.00","6","248.16","4136.00","3887.84","181900","2","0","2019-01-01 16:10:41","2019-01-01 16:10:41","1","1");
INSERT INTO bill_fare VALUES("7","BILL1819007","2","Sdfdsjhjh","8698989898","65-46-5456","2","9","6","52.08","0","0.00","868.00","920.08","181900","10","0","2019-01-01 16:11:29","2019-01-01 16:11:29","1","1");
INSERT INTO bill_fare VALUES("8","KSFW1819008","1","Gsijdfjk","9885968986","25-25-2525","1","1","6","5.64","6","5.98","94.00","93.66","181900","10","0","2019-01-02 17:50:52","2019-01-02 17:50:52","1","1");
INSERT INTO bill_fare VALUES("9","KSFW1819009","1","Tset","9899898899","56-56-5656","1","1","0","0.00","0","0.00","85.00","85.00","181900","10","0","2019-01-04 14:00:11","2019-01-04 14:00:11","1","1");
INSERT INTO bill_fare VALUES("10","KSFW18190010","2","Gaurav Singh","7376386005","05-08-1995","1","1","0","0.00","0","0.00","188.00","188.00","181900","2","0","2019-01-04 14:04:29","2019-01-04 14:04:29","1","1");
INSERT INTO bill_fare VALUES("11","KSFW18190011","3","Gaurav Singh","7376386005","05-08-1995","1","1","0","0.00","0","0.00","80.00","80.00","181900","3","0","2019-01-04 15:02:36","2019-01-04 15:02:36","1","1");
INSERT INTO bill_fare VALUES("12","KSFW18190012","4","Gaurav Singh","7376386005","05-08-1995","1","1","0","0.00","0","0.00","85.00","85.00","181900","11","0","2019-01-04 15:03:53","2019-01-04 15:03:53","1","1");
INSERT INTO bill_fare VALUES("13","KSFW18190013","5","Gaurav Singh","7376386005","05-08-1995","1","1","0","0.00","0","0.00","94.00","94.00","181900","9","0","2019-01-04 15:08:15","2019-01-04 15:08:15","1","1");
INSERT INTO bill_fare VALUES("14","KSFW18190014","6","Uday Veer Singh","8299431080","22-12-1985","1","1","0","0.00","0","0.00","188.00","188.00","181900","1","0","2019-01-04 15:39:16","2019-01-04 15:39:16","1","1");
INSERT INTO bill_fare VALUES("15","KSFW18190015","1","ABC","9865686588","11-11-1111","1","1","0","0.00","6","11.28","188.00","176.72","181900","2","0","2019-01-07 17:11:04","2019-01-07 17:11:04","1","1");
INSERT INTO bill_fare VALUES("16","KSFW18190016","2","ABV","9888888888","11-11-1111","1","1","0","0.00","0","0.00","188.00","188.00","181900","2","0","2019-01-07 17:11:47","2019-01-07 17:11:47","1","1");
INSERT INTO bill_fare VALUES("17","KSFW18190017","3","AAA","9888888888","11-11-1111","1","1","0","0.00","0","0.00","188.00","188.00","181900","2","0","2019-01-07 17:13:27","2019-01-07 17:13:27","1","1");
INSERT INTO bill_fare VALUES("18","KSFW18190018","4","ABV","9888888888","11-11-1111","1","1","0","0.00","0","0.00","188.00","188.00","181900","2","0","2019-01-07 17:14:06","2019-01-07 17:14:06","1","1");
INSERT INTO bill_fare VALUES("19","KSFW18190019","5","ABV","9888888888","11-11-1111","1","1","0","0.00","0","0.00","188.00","188.00","181900","2","0","2019-01-07 17:14:53","2019-01-07 17:14:53","1","1");
INSERT INTO bill_fare VALUES("20","KSFW18190020","6","ABV","9888888888","11-11-1111","1","1","0","0.00","0","0.00","188.00","188.00","181900","2","0","2019-01-07 17:15:32","2019-01-07 17:15:32","1","1");
INSERT INTO bill_fare VALUES("21","KSFW18190021","7","ABV","9888888888","11-11-1111","1","1","0","0.00","0","0.00","188.00","188.00","181900","2","0","2019-01-07 17:18:53","2019-01-07 17:18:53","1","1");
INSERT INTO bill_fare VALUES("22","KSFW18190022","8","ABV","9888888888","11-11-1111","1","1","0","0.00","0","0.00","85.00","85.00","181900","2","0","2019-01-07 17:29:26","2019-01-07 17:29:26","1","1");





CREATE TABLE `bill_items` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT,
  `bill_id` bigint(100) NOT NULL,
  `bill_no` varchar(220) NOT NULL,
  `cust_mobile` varchar(100) NOT NULL,
  `product_id` int(100) NOT NULL,
  `product_name` varchar(225) NOT NULL,
  `qty` int(220) NOT NULL,
  `rate` varchar(100) NOT NULL,
  `discount` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `remark` varchar(220) NOT NULL,
  `financial_year` varchar(100) NOT NULL,
  `bill_date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

INSERT INTO bill_items VALUES("8","1","BILL1819001","7376386005","1","PIZZA","10","200.00","12.00","1880.00","","181900","2018-12-22 11:03:37","2018-12-22 11:35:30","1");
INSERT INTO bill_items VALUES("9","1","BILL1819001","7376386005","4","CHAUMIN","12","100.00","6.00","1128.00","","181900","2018-12-22 11:03:37","2018-12-22 11:35:30","1");
INSERT INTO bill_items VALUES("10","1","BILL1819001","7376386005","6","PASTA","6","150.00","20.00","780.00","","181900","2018-12-22 11:03:37","2018-12-22 11:35:30","1");
INSERT INTO bill_items VALUES("11","1","BILL1819001","7376386005","3","COFFEE","15","80.00","0.00","1200.00","","181900","2018-12-22 11:03:37","2018-12-22 11:35:30","1");
INSERT INTO bill_items VALUES("23","3","BILL1819003","9898989989","3","COFFEE","10","80.00","0.00","800.00","","181900","2018-12-22 13:25:54","2018-12-22 15:11:05","1");
INSERT INTO bill_items VALUES("24","3","BILL1819003","9898989989","5","BRIYANI","1","1000.00","150.00","850.00","","181900","2018-12-22 13:25:54","2018-12-22 15:11:05","1");
INSERT INTO bill_items VALUES("25","3","BILL1819003","9898989989","6","PASTA","5","150.00","20.00","650.00","","181900","2018-12-22 13:25:54","2018-12-22 15:11:05","1");
INSERT INTO bill_items VALUES("26","3","BILL1819003","9898989989","7","CAKE","1","300.00","45.00","255.00","","181900","2018-12-22 13:25:54","2018-12-22 15:11:05","1");
INSERT INTO bill_items VALUES("27","2","BILL1819002","8299431080","1","PIZZA","2","200.00","12.00","376.00","","181900","2018-12-22 11:07:54","2018-12-27 16:29:06","1");
INSERT INTO bill_items VALUES("28","2","BILL1819002","8299431080","5","BRIYANI","1","1000.00","150.00","850.00","","181900","2018-12-22 11:07:54","2018-12-27 16:29:06","1");
INSERT INTO bill_items VALUES("29","2","BILL1819002","8299431080","10","NIMBU PANI","15","50.00","0.00","750.00","","181900","2018-12-22 11:07:54","2018-12-27 16:29:06","1");
INSERT INTO bill_items VALUES("30","4","BILL1819004","8989899665","1","PIZZA","5","200.00","12.00","940.00","","181900","2018-12-27 18:08:55","2018-12-27 18:27:07","1");
INSERT INTO bill_items VALUES("31","4","BILL1819004","8989899665","2","BURGER","10","100.00","15.00","850.00","","181900","2018-12-27 18:08:55","2018-12-27 18:27:07","1");
INSERT INTO bill_items VALUES("32","4","BILL1819004","8989899665","4","CHAUMIN","3","100.00","6.00","282.00","","181900","2018-12-27 18:08:55","2018-12-27 18:27:07","1");
INSERT INTO bill_items VALUES("33","5","BILL1819005","9898987984","2","BURGER","25","100.00","15.00","2125.00","","181900","2019-01-02 16:08:09","2019-01-02 16:08:09","1");
INSERT INTO bill_items VALUES("34","6","BILL1819006","9879879879","1","PIZZA","22","200.00","12.00","4136.00","","181900","2019-01-02 16:10:41","2019-01-02 16:10:41","1");
INSERT INTO bill_items VALUES("35","7","BILL1819007","8698989898","1","PIZZA","1","200.00","12.00","188.00","","181900","2019-01-02 16:11:29","2019-01-02 16:11:29","1");
INSERT INTO bill_items VALUES("36","7","BILL1819007","8698989898","2","BURGER","8","100.00","15.00","680.00","","181900","2019-01-02 16:11:29","2019-01-02 16:11:29","1");
INSERT INTO bill_items VALUES("37","8","KSFW1819008","9885968986","4","CHAUMIN","1","100.00","6.00","94.00","","181900","2019-01-02 17:50:52","2019-01-02 17:50:52","1");
INSERT INTO bill_items VALUES("38","9","KSFW1819009","9899898899","2","BURGER","1","100.00","15.00","85.00","","181900","2019-01-04 14:00:11","2019-01-04 14:00:11","1");
INSERT INTO bill_items VALUES("39","10","KSFW18190010","7376386005","1","PIZZA","1","200.00","12.00","188.00","","181900","2019-01-04 14:04:29","2019-01-04 14:04:29","1");
INSERT INTO bill_items VALUES("40","11","KSFW18190011","7376386005","3","COFFEE","1","80.00","0.00","80.00","","181900","2019-01-04 15:02:36","2019-01-04 15:02:36","1");
INSERT INTO bill_items VALUES("41","12","KSFW18190012","7376386005","2","BURGER","1","100.00","15.00","85.00","","181900","2019-01-04 15:03:53","2019-01-04 15:03:53","1");
INSERT INTO bill_items VALUES("42","13","KSFW18190013","7376386005","4","CHAUMIN","1","100.00","6.00","94.00","","181900","2019-01-04 15:08:15","2019-01-04 15:08:15","1");
INSERT INTO bill_items VALUES("43","14","KSFW18190014","8299431080","1","PIZZA","1","200.00","12.00","188.00","","181900","2019-01-04 15:39:16","2019-01-04 15:39:16","1");
INSERT INTO bill_items VALUES("44","15","KSFW18190015","9865686588","1","PIZZA","1","200.00","12.00","188.00","","181900","2019-01-07 17:11:04","2019-01-07 17:11:04","1");
INSERT INTO bill_items VALUES("45","16","KSFW18190016","9888888888","1","PIZZA","1","200.00","12.00","188.00","","181900","2019-01-07 17:11:47","2019-01-07 17:11:47","1");
INSERT INTO bill_items VALUES("46","17","KSFW18190017","9888888888","1","PIZZA","1","200.00","12.00","188.00","","181900","2019-01-07 17:13:27","2019-01-07 17:13:27","1");
INSERT INTO bill_items VALUES("47","18","KSFW18190018","9888888888","1","PIZZA","1","200.00","12.00","188.00","","181900","2019-01-07 17:14:06","2019-01-07 17:14:06","1");
INSERT INTO bill_items VALUES("48","19","KSFW18190019","9888888888","1","PIZZA","1","200.00","12.00","188.00","","181900","2019-01-07 17:14:53","2019-01-07 17:14:53","1");
INSERT INTO bill_items VALUES("49","20","KSFW18190020","9888888888","1","PIZZA","1","200.00","12.00","188.00","","181900","2019-01-07 17:15:32","2019-01-07 17:15:32","1");
INSERT INTO bill_items VALUES("50","21","KSFW18190021","9888888888","1","PIZZA","1","200.00","12.00","188.00","","181900","2019-01-07 17:18:53","2019-01-07 17:18:53","1");
INSERT INTO bill_items VALUES("51","22","KSFW18190022","9888888888","2","BURGER","1","100.00","15.00","85.00","","181900","2019-01-07 17:29:26","2019-01-07 17:29:26","1");





CREATE TABLE `bill_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax` varchar(100) NOT NULL,
  `percent` varchar(100) NOT NULL DEFAULT 'other',
  `remark` varchar(200) NOT NULL,
  `created_by` bigint(100) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO bill_tax VALUES("1","GST","0","testing","1","2018-12-15 19:03:36","1");
INSERT INTO bill_tax VALUES("2","GST","6","testing","1","2018-12-15 19:03:36","1");
INSERT INTO bill_tax VALUES("3","GST","12","testing","1","2018-12-15 19:03:36","1");
INSERT INTO bill_tax VALUES("4","OTHER AMOUNT","Other Amount","","1","2018-12-15 19:07:07","1");
INSERT INTO bill_tax VALUES("5","GST","18","","1","2018-12-22 12:20:39","1");





CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(220) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `gst_no` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `image` varchar(220) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modify` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO company VALUES("1","Kavyashree Foodworks","gaurav.s@softgentechnologies.com","0544623434","8299431080","LDA Colony Near Power House Chauraha , Lucknow","37ADAPM1724A2Z5","QWERT1234A","1/1logo.gif","2018-12-15 16:12:53","2018-12-22 12:11:42","1");





CREATE TABLE `confic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `confic_type_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `control` varchar(200) DEFAULT 'text',
  PRIMARY KEY (`id`,`confic_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

INSERT INTO confic VALUES("1","1","paging","100","Page should be #5, 10, 20, 50, 100, 1000","select");
INSERT INTO confic VALUES("3","1","default_mail","narai1987@gmail.com","Default mail to communicate with user","text");
INSERT INTO confic VALUES("4","1","max_count","100","","");
INSERT INTO confic VALUES("5","2","language","1","","");
INSERT INTO confic VALUES("6","1","service_tax","8","","");
INSERT INTO confic VALUES("10","1","join_point","100","","");
INSERT INTO confic VALUES("11","1","rating_point","10","","");
INSERT INTO confic VALUES("12","1","get_point_on_purchase_product_per_1000_USD","100","","");
INSERT INTO confic VALUES("13","1","login_point","5","","");
INSERT INTO confic VALUES("14","1","price_per_100_point","2","","");
INSERT INTO confic VALUES("15","1","admin_gift_point","50","","");
INSERT INTO confic VALUES("16","1","minimum_used_point","500","","");
INSERT INTO confic VALUES("17","1","point_per_beverage","5","","");
INSERT INTO confic VALUES("18","1","point_per_eqipment","5","","");
INSERT INTO confic VALUES("19","1","point_per_food","5","","");
INSERT INTO confic VALUES("20","1","point_per_cabin","10","","");
INSERT INTO confic VALUES("21","1","trip_low_price_range","5000","","text");
INSERT INTO confic VALUES("22","1","trip_high_price_range","25000","","text");





CREATE TABLE `customer_details` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(100) NOT NULL,
  `name` varchar(220) NOT NULL,
  `mobile` varchar(220) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL COMMENT 'date_of_birth/ anniversery',
  `address` text NOT NULL,
  `date_created` datetime NOT NULL,
  `emp_id` bigint(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `hrm_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_name` varchar(220) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO hrm_post VALUES("1","ADMIN","2018-12-17 11:17:41","1");
INSERT INTO hrm_post VALUES("2","MANAGER","2018-12-17 11:46:51","1");
INSERT INTO hrm_post VALUES("3","EMPLOYEE","2018-12-17 11:47:06","1");





CREATE TABLE `payment_mode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mode` varchar(200) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO payment_mode VALUES("1","DEBIT CARD","2018-12-15 16:38:30","1");
INSERT INTO payment_mode VALUES("2","CASH","2018-12-15 18:43:51","1");
INSERT INTO payment_mode VALUES("3","CREDIT CARD","2018-12-15 18:44:17","1");
INSERT INTO payment_mode VALUES("4","PAYTM","2018-12-22 12:13:25","1");
INSERT INTO payment_mode VALUES("5","GIFT CARD","2018-12-27 13:34:39","1");
INSERT INTO payment_mode VALUES("6","ZOMATO","2018-12-27 13:35:26","1");
INSERT INTO payment_mode VALUES("7","FOODPANDA","2018-12-27 13:35:46","1");
INSERT INTO payment_mode VALUES("8","GOOGLE PAY","2018-12-27 13:36:09","1");
INSERT INTO payment_mode VALUES("9","PHONEPE","2018-12-27 13:36:24","1");
INSERT INTO payment_mode VALUES("10","BHIM UPI","2018-12-27 13:36:43","1");
INSERT INTO payment_mode VALUES("11","COD","2018-12-27 13:37:07","1");
INSERT INTO payment_mode VALUES("12","SWIGGY","2018-12-27 14:48:33","1");





CREATE TABLE `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(220) NOT NULL,
  `remark` varchar(225) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO product_category VALUES("1","Starter","","2019-01-21 13:07:03","1");
INSERT INTO product_category VALUES("2","Combo","","2019-01-21 13:27:30","1");
INSERT INTO product_category VALUES("3","Normal","","2019-01-21 13:27:41","1");





CREATE TABLE `product_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(220) NOT NULL,
  `category_id` int(11) NOT NULL,
  `qty` varchar(100) NOT NULL DEFAULT '1',
  `price` varchar(100) NOT NULL,
  `discount_type` varchar(255) NOT NULL COMMENT 'Other Means manual Amount number means %',
  `discount` varchar(100) NOT NULL,
  `remark` varchar(220) NOT NULL,
  `date_created` datetime NOT NULL,
  `created_by` bigint(100) NOT NULL,
  `date_modify` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO product_list VALUES("1","PIZZA","0","1","200","6","12","pizza offer","2018-12-15 00:00:00","1","dfgdf","1");
INSERT INTO product_list VALUES("2","BURGER","0","1","100","15","15","","2018-12-15 18:15:02","1","2018-12-15 18:15:02","1");
INSERT INTO product_list VALUES("3","COFFEE","0","1","80","0","0","Coffee Nescafe","2018-12-19 14:40:43","1","2018-12-19 14:40:43","1");
INSERT INTO product_list VALUES("4","CHAUMIN","0","1","100","6","6","","2018-12-19 14:51:39","1","2018-12-19 14:51:39","1");
INSERT INTO product_list VALUES("5","BRIYANI","3","1","1000","15","150","","2018-12-19 15:12:20","1","2018-12-19 15:12:20","1");
INSERT INTO product_list VALUES("6","PASTA","0","1","150","Other Amount","20","pasta le lo","2018-12-19 15:20:07","1","2018-12-19 15:20:07","1");
INSERT INTO product_list VALUES("7","CAKE","0","1","300","15","45","Cake kha lo","2018-12-19 15:20:43","1","2018-12-19 15:20:43","1");
INSERT INTO product_list VALUES("8","GFHGFH","0","1","100","Other Amount","1","","2018-12-21 14:12:18","1","2018-12-21 14:12:18","1");
INSERT INTO product_list VALUES("9","FDGFD","0","1","100","Other Amount","100","","2018-12-21 14:14:27","1","2018-12-21 14:14:27","1");
INSERT INTO product_list VALUES("10","NIMBU PANI","0","1","50","0","0","","2018-12-22 13:23:50","1","2018-12-22 13:23:50","1");
INSERT INTO product_list VALUES("11","SDFDSFSDF","0","1","12","6","1","","2018-12-27 12:00:19","1","2018-12-27 12:00:19","1");
INSERT INTO product_list VALUES("12","TEST","0","1","123","12","15","dfgfd","2018-12-27 12:14:27","1","2018-12-27 12:14:27","1");





CREATE TABLE `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `tmp_type` int(11) DEFAULT NULL,
  `default_temp` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO templates VALUES("1","jet_tmp","1","0","1");
INSERT INTO templates VALUES("2","api","0","0","1");
INSERT INTO templates VALUES("7","kartiano_web","1","1","1");
INSERT INTO templates VALUES("8","restaurant","0","1","1");





CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `state_id` varchar(255) DEFAULT NULL,
  `city_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(255) DEFAULT NULL,
  `post` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `reporting` varchar(255) DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `utype` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO users VALUES("1","admin","admin","Admin","9411950511","admin@gmail.com","","1","1","1","","1/1imagePenguins.jpg","01/02/2017","Administrator","1");
INSERT INTO users VALUES("2","EMP001","admin","Nagesh rai","8382936646","M.D","4","2","1","2","1","","","Employee","1");
INSERT INTO users VALUES("3","EMP004","123456","Gaurav Kumar Singh","73763860056","gaurav.s@softgentechnologies.com","","","","3","","","2018-12-17","Employee","1");




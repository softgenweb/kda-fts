<?php
include("connection.php");
$response = array();
?>

<!DOCTYPE html>
<html>
<head>
	<title>SGT Finger Print Demo</title>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script src="jquery-1.8.2.js"></script>
	<script src="mfs100-9.0.2.6.js"></script>
</head>
<body>
	<div class="page-header">
		<h1 style="color: #87CB16;">SGT Finger Print Demo</h1>
	</div>
	<div class="container">
		
		<div class="row">
			<div class="col-md-3">
			</div>

			<div class="col-md-1">
				<a href="javascript:;" class="btn btn-success" onclick="GetInfo()">Get Info</a>
			</div>
			<div class="col-md-1">
				<a href="javascript:;" class="btn btn-success" onclick="Capture()">Capture</a>
			</div>
			<div class="col-md-2">
				<a href="javascript:;" class="btn btn-success" onclick="Match()">Capture & Match</a>
			</div>
			<div class="col-md-1">

			</div>

			<div class="col-md-1">

			</div>

			<div class="col-md-3">
				<?php 
				//$sql = "SELECT id,base64iostemp FROM mantra_capture ORDER BY id";
			// $sql = "SELECT id,base64iostemp FROM mantra_capture where hd_base64iostemp='".$_REQUEST['hd_base64iostemp']."' ORDER BY id";
			$sql = "SELECT id,base64iostemp FROM mantra_capture where id='42' ORDER BY id";
				$result = mysql_query($sql);

				while($row=mysql_fetch_array($result)){
					$tmp = array();
					$tmp['id']=$row['id'];
					$tmp['fdata']=$row['base64iostemp'];
					array_push($response, $tmp);
				} 

				?>
			</div>

		</div>
		<div class="clearfix"></div>

		<br>
		<div class="row">
			<?php
			if(isset($_SESSION["saved_status"])){
				if($_SESSION["saved_status"] == "0"){ ?>
					<h3>Success</h3>
				<?php }else if($_SESSION["saved_status"] == "1"){ ?>
					<h3>Error</h3>
				<?php }
				unset($_SESSION["saved_status"]);
			}

			?>
		</div>
		<br>
		<div id="sec_get_info">
			<form method="POST" action="process.php">
				<div class="row">
					<h3 style="color: #87CB16;">Get Info</h3>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-2">
						<span>Serial No</span>
					</div>
					<div class="col-md-2">
						<input type="text" value="" id="txt_serial_no" class="form-control" disabled="" />
						<input type="hidden" name="hd_serial_no" id="hd_serial_no">

					</div>
					<div class="col-md-2">
						<span>Certification</span>
					</div>
					<div class="col-md-2">
						<input type="text" value="" id="txt_certificate" class="form-control" disabled=""/>
						<input type="hidden" name="hd_certificate" id="hd_certificate">
					</div>
					<div class="col-md-2">
						<span>Make</span>
					</div>
					<div class="col-md-2">
						<input type="text" value="" id="txt_make" class="form-control" disabled=""/>
						<input type="hidden" name="hd_make" id="hd_make">

					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-md-2">
						<span>Model</span>
					</div>
					<div class="col-md-2">
						<input type="text" value="" id="txt_model" class="form-control" disabled=""/>
						<input type="hidden" name="hd_model" id="hd_model">
					</div>
					<div class="col-md-2">
						<span>Width</span>
					</div>
					<div class="col-md-2">
						<input type="text" value="" id="txt_width" class="form-control" disabled=""/>
						<input type="hidden" name="hd_width" id="hd_width">
					</div>
					<div class="col-md-2">
						<span>Height</span>
					</div>
					<div class="col-md-2">
						<input type="text" value="" id="txt_height" class="form-control" disabled=""/>
						<input type="hidden" name="hd_height" id="hd_height">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-2">
						<span>Local IP	</span>
					</div>
					<div class="col-md-2">
						<input type="text" value="" id="txt_localip" class="form-control" disabled=""/>
						<input type="hidden" name="hd_localip" id="hd_localip">
					</div>
					<div class="col-md-2">
						<span>Local MAC:	</span>
					</div>
					<div class="col-md-2">
						<input type="text" value="" id="txt_localmac" class="form-control" disabled=""/>
						<input type="hidden" name="hd_localmac" id="hd_localmac">
					</div>

					<div class="col-md-2">
						<span>Public IP	</span>
					</div>
					<div class="col-md-2">
						<input type="text" value="" id="txt_publicip" class="form-control" disabled=""/>
						<input type="hidden" name="hd_publicip" id="hd_publicip">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-2">
						<span>System ID	</span>
					</div>
					<div class="col-md-2">
						<input type="text" value="" id="txt_systemid" class="form-control" disabled=""/>
						<input type="hidden" name="hd_systemid" id="hd_systemid">
					</div>
				</div>	
				<div style="display: none;">
					<input type="submit" name="btn_save_info" value="Save"  id="btn_save_info">
				</div>	 
				<hr>
				<br> 
			</form>
		</div>
		<div id="sec_capture_data">
			<form method="POST" action="process.php" enctype="multipart/form-data">
				<div class="row">
					<h3 style="color: #87CB16;">Capture Data</h3>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-3">
						<span>Image</span>
					</div>
					<div class="col-md-9">
						<img id="imgFinger" width="145px" height="188px" alt="Finger Image" name="imgFinger" />
						<input type="hidden" name="hd_imgFinger" id="hd_imgFinger">

					</div>
				</div>
				<br> 

				<div class="row">
					<div class="col-md-3">
						<span>Quality</span>
					</div>
					<div class="col-md-9">
						<input type="text" value="" id="txt_quality" class="form-control" disabled="" />
						<input type="hidden" name="hd_quality" id="hd_quality">
					</div>
				</div>
				<br> 


				<div class="row">


					<div class="col-md-3">
						<span>Base64Encoded ISO Template</span>
					</div>
					<div class="col-md-9">
						<input type="text" value="" id="txt_base64iostemp" class="form-control" disabled=""/>
						<input type="hidden" name="hd_base64iostemp" id="hd_base64iostemp">
					</div>
				</div>
				<br> 
				<div class="row">

					<div class="col-md-3">
						<span>Base64Encoded ANSI Template	</span>
					</div>
					<div class="col-md-9">
						<input type="text" value="" id="txt_base64ansitemp" class="form-control" disabled=""/>
						<input type="hidden" name="hd_base64ansitemp" id="hd_base64ansitemp">

					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-md-3">
						<span>Base64Encoded ISO Image	</span>
					</div>
					<div class="col-md-9">
						<input type="text" value="" id="txt_base64iosimg" class="form-control" disabled=""/>
						<input type="hidden" name="hd_base64iosimg" id="hd_base64iosimg">
					</div>
				</div>
				<br> 
				<div class="row">
					<div class="col-md-3">
						<span>Base64Encoded Raw Data	</span>
					</div>
					<div class="col-md-9">
						<input type="text" value="" id="txt_base64rawdata" class="form-control" disabled=""/>
						<input type="hidden" name="hd_base64rawdata" id="hd_base64rawdata">
					</div>
				</div>
				<br> 
				<div class="row">

					<div class="col-md-3">
						<span>Base64Encoded Wsq Image Data	</span>
					</div>
					<div class="col-md-9">
						<input type="text" value="" id="txt_base64wsqimgdata" class="form-control" disabled=""/>
						<input type="hidden" name="hd_base64wsqimgdata" id="hd_base64wsqimgdata">
					</div>


				</div>
			<!--<br> 
			 <div class="row">
				<div class="col-md-3">
					<span>Encrypted Base64Encoded Pid/Rbd		</span>
				</div>
				<div class="col-md-9">
					<input type="text" value="" id="txt_encrybase64pid" class="form-control" disabled=""/>
				</div>
			</div>
			<br> 
			<div class="row">
				<div class="col-md-3">
					<span>Encrypted Base64Encoded Session Key	</span>
				</div>
				<div class="col-md-9">
					<input type="text" value="" id="txt_encrybase64sesskey" class="form-control" disabled=""/>
				</div>
			</div>
			<br> 
			<div class="row">
				<div class="col-md-3">
					<span>Encrypted Base64Encoded Hmac	</span>
				</div>
				<div class="col-md-9">
					<input type="text" value="" id="txt_encrybase64hmac" class="form-control" disabled=""/>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3">
					<span>Ci</span>
				</div>
				<div class="col-md-9">
					<input type="text" value="" id="txt_ci" class="form-control" disabled=""/>
				</div>
			</div>	
			<br> 
			<div class="row">
				<div class="col-md-3">
					<span>Pid/Rbd Ts	</span>
				</div>
				<div class="col-md-9">
					<input type="text" value="" id="txt_pid" class="form-control" disabled=""/>
				</div>
			</div>	 -->

			<div style="display: block;">
				<input type="submit" name="btn_save_capture_data" value="Save"  id="btn_save_capture_data">
			</div>	
		</form>
	</div>
	<hr>
</div>
<script language="javascript" type="text/javascript">

	var jqueryarray = <?php echo json_encode($response); ?>;

	


        var quality = 60; //(1 to 100) (recommanded minimum 55)
        var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

        $('#sec_get_info').hide();
        $('#sec_capture_data').hide();
        function GetInfo() {
        	$('#txt_serial_no').val(''); 
        	$('#txt_certificate').val(''); 
        	$('#txt_make').val(''); 
        	$('#txt_model').val(''); 
        	$('#txt_width').val(''); 
        	$('#txt_height').val(''); 
        	$('#txt_localip').val(''); 
        	$('#txt_localmac').val(''); 
        	$('#txt_publicip').val(''); 
        	$('#txt_systemid').val(''); 

        	$('#hd_serial_no').val(''); 
        	$('#hd_certificate').val(''); 
        	$('#hd_make').val(''); 
        	$('#hd_model').val(''); 
        	$('#hd_width').val(''); 
        	$('#hd_height').val(''); 
        	$('#hd_localip').val(''); 
        	$('#hd_localmac').val(''); 
        	$('#hd_publicip').val(''); 
        	$('#hd_systemid').val(''); 



        	$('#sec_get_info').show();
        	$('#sec_capture_data').hide();

        	var key = "";

        	var res;
        	if (key.length == 0) {
        		res = GetMFS100Info();
        	}
        	else {
        		res = GetMFS100KeyInfo(key);
        	}

        	if (res.httpStaus) {

                //document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;

                if (res.data.ErrorCode == "0") {                 


                	$('#txt_serial_no').val(res.data.DeviceInfo.SerialNo); 
                	$('#txt_certificate').val(res.data.DeviceInfo.Certificate); 
                	$('#txt_make').val(res.data.DeviceInfo.Make); 
                	$('#txt_model').val(res.data.DeviceInfo.Model); 
                	$('#txt_width').val(res.data.DeviceInfo.Width); 
                	$('#txt_height').val(res.data.DeviceInfo.Height); 
                	$('#txt_localip').val(res.data.DeviceInfo.LocalIP); 
                	$('#txt_localmac').val(res.data.DeviceInfo.LocalMac); 
                	$('#txt_publicip').val(res.data.DeviceInfo.PublicIP); 
                	$('#txt_systemid').val(res.data.DeviceInfo.SystemID); 

                	$('#hd_serial_no').val(res.data.DeviceInfo.SerialNo); 
                	$('#hd_certificate').val(res.data.DeviceInfo.Certificate); 
                	$('#hd_make').val(res.data.DeviceInfo.Make); 
                	$('#hd_model').val(res.data.DeviceInfo.Model); 
                	$('#hd_width').val(res.data.DeviceInfo.Width); 
                	$('#hd_height').val(res.data.DeviceInfo.Height); 
                	$('#hd_localip').val(res.data.DeviceInfo.LocalIP); 
                	$('#hd_localmac').val(res.data.DeviceInfo.LocalMac); 
                	$('#hd_publicip').val(res.data.DeviceInfo.PublicIP); 
                	$('#hd_systemid').val(res.data.DeviceInfo.SystemID); 

                	$('#btn_save_info').click();
                }
            }
            else {
            	alert(res.err);
            }
            return false;
        }

        function Capture() {
        	try {

        		$('#sec_get_info').hide();
        		$('#sec_capture_data').show();


        		$('#txt_quality').val('');
        		$('#txt_base64iostemp').val('');
        		$('#txt_base64ansitemp').val('');
        		$('#txt_base64iosimg').val('');
        		$('#txt_base64rawdata').val('');
        		$('#txt_base64wsqimgdata').val(''); 

        		$('#hd_quality').val('');
        		$('#hd_base64iostemp').val('');
        		$('#hd_base64ansitemp').val('');
        		$('#hd_base64iosimg').val('');
        		$('#hd_base64rawdata').val('');
        		$('#hd_base64wsqimgdata').val(''); 



        		var res = CaptureFinger(quality, timeout);
        		if (res.httpStaus) {

                   // document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;

                   if (res.data.ErrorCode == "0") {
                   	document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;

                   	$('#hd_imgFinger').val("data:image/bmp;base64," + res.data.BitmapData);

                   	var imageinfo = "Quality: " + res.data.Quality + " Nfiq: " + res.data.Nfiq + " W(in): " + res.data.InWidth + " H(in): " + res.data.InHeight + " area(in): " + res.data.InArea + " Resolution: " + res.data.Resolution + " GrayScale: " + res.data.GrayScale + " Bpp: " + res.data.Bpp + " WSQCompressRatio: " + res.data.WSQCompressRatio + " WSQInfo: " + res.data.WSQInfo;



                   	$('#txt_quality').val(imageinfo);
                   	$('#txt_base64iostemp').val(res.data.IsoTemplate);
                   	$('#txt_base64ansitemp').val(res.data.AnsiTemplate);
                   	$('#txt_base64iosimg').val(res.data.IsoImage);
                   	$('#txt_base64rawdata').val(res.data.RawData);
                   	$('#txt_base64wsqimgdata').val(res.data.WsqImage); 

                   	$('#hd_quality').val(imageinfo);
                   	$('#hd_base64iostemp').val(res.data.IsoTemplate);
                   	$('#hd_base64ansitemp').val(res.data.AnsiTemplate);
                   	$('#hd_base64iosimg').val(res.data.IsoImage);
                   	$('#hd_base64rawdata').val(res.data.RawData);
                   	$('#hd_base64wsqimgdata').val(res.data.WsqImage); 


                  // 	$('#btn_save_capture_data').click();



                   }
               }
               else {
               	alert(res.err);
               }
           }
           catch (e) {
           	alert(e);
           }
           return false;
       }
     
	   function Match() {
       	try {
       		var logs = 0;
       		$.each(jqueryarray, function () {
alert(this.id);
       			var isotemplate = this.fdata;

       			var res = MatchFinger(quality, timeout, isotemplate);

       			if (res.httpStaus) {
       				if (res.data.Status) {
       					alert("Finger matched");
       					alert(this.id);
       					logs = 1;
       					return false;
       				}
       				// else {
       				// 	if (res.data.ErrorCode != "0") {
       				// 		alert(res.data.ErrorDescription);
       				// 	}
       				// 	else {
       				// 		alert("Finger not matched");
       				// 	}
       				// }
       			}
       			else {
       				alert(res.err);
       			}
       		});
       		if(logs == 0){
       			alert("Finger not matched");
       		}
       	}
       	catch (e) {
       		alert(e);
       	}

       	return false;

       }






   </script>
</body>
</html>